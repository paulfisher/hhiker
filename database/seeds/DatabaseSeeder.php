<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        require_once 'VkGeoSeeder.php';
        Model::unguard();

        $this->call(VkGeoSeeder::class);

        Model::reguard();
    }
}
