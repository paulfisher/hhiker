<?php

use App\Models\City;
use App\Models\Country;
use App\Models\Region;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class VkGeoSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $countries = $this->loadCountries();
    }

    private function loadCountries() {
        $url = "http://api.vk.com/method/database.getCountries?need_all=1&count=1099";
        $countriesData = json_decode(file_get_contents($url), true);
        $countries = $countriesData['response'];
        foreach ($countries as $countryData) {
            $this->saveCountry($countryData);
        }
    }

    private function saveCountry($countryData) {
        $country = Country::where('vk_id', $countryData['cid'])->first();
        if (!$country) {
            $country = Country::create([
                'vk_id' => $countryData['cid'],
                'name' => $countryData['title']
            ]);
            echo "saving country " . $countryData['title'] . "\n";
        }

        $this->loadRegions($country);
        $this->loadCities($country);
    }

    private function loadRegions($country) {
        $offset = 0;
        $count = 1000;
        do {
            $loadedRegions = $this->loadRegionsEx($country, $offset, $count);
            $offset += $count;
        } while($loadedRegions > 0);
    }

    private function loadRegionsEx($country, $offset, $count) {
        $url = "http://api.vk.com/method/database.getRegions?country_id=" . $country->id . "&need_all=1&count=" . $count . "&offset=" . $offset;
        $regionsData = json_decode(file_get_contents($url), true);
        $regions = [];
        if (isset($regionsData['response'])) {
            $regions = $regionsData['response'];
            foreach ($regions as $regionData) {
                $this->saveRegion($country, $regionData);
            }
        }
        return count($regions);
    }

    private function saveRegion($country, $regionData) {
        $region = Region::where('vk_id', $regionData['region_id'])->first();
        if (!$region) {
            $region = Region::create([
                'country_id' => $country->id,
                'vk_id' => $regionData['region_id'],
                'name' => $regionData['title'],
            ]);
            echo "saving region " . $regionData['title'] . "\n";
        }
    }

    private function loadCities($country) {
        $offset = 0;
        $count = 1000;
        do {
            $loadedCities = $this->loadCitiesEx($country, $offset, $count);
            $offset += $count;
        } while($loadedCities > 0);
    }

    private function loadCitiesEx($country, $offset, $count) {
        $url = "http://api.vk.com/method/database.getCities?country_id=" . $country->id . "&need_all=1&count=" . $count . "&offset=" . $offset;
        $citiesData = json_decode(file_get_contents($url), true);
        $cities = [];
        if (isset($citiesData['response'])) {
            $cities = $citiesData['response'];
            foreach ($cities as $cityData) {
                $this->saveCity($country, $cityData);
            }
        }
        return count($cities);
    }

    private function saveCity($country, $cityData) {
        $city = City::where('vk_id', $cityData['cid'])->first();
        if (!$city) {
            $city = City::create([
                'country_id' => $country->id,
                'vk_id' => $cityData['cid'],
                'name' => $cityData['title'],
                'region' => isset($cityData['region']) ? $cityData['region'] : '',
                'area' => isset($cityData['area']) ? $cityData['area'] : '',
                'important' => isset($cityData['important']) ? $cityData['important'] : false,
            ]);
            echo "saving city " . $cityData['title'] . "\n";
        }
    }


}
