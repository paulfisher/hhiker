<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterUserRenameSalePostcard extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('is_sale_postcard');
            $table->string('yandex_money_account')->nullable();
        });

        Schema::table('blog', function (Blueprint $table) {
            $table->boolean('is_accept_funding')->default(false);
            $table->integer('postcard_min_pledge')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}
