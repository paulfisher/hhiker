<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DeleteTravelBlog extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::drop('travel_blog');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() { }
}
