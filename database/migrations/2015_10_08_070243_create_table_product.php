<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableProduct extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->boolean('is_sale_postcard')->default(false);
        });

        Schema::create('order', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('blog_id')->unsigned();
            $table->integer('seller_user_id')->unsigned();
            $table->integer('buyer_user_id')->unsigned()->nullable();

            $table->string('buyer_country')->nullable();
            $table->string('buyer_region')->nullable();
            $table->string('buyer_city')->nullable();
            $table->string('buyer_street')->nullable();
            $table->string('buyer_building')->nullable();
            $table->string('buyer_room')->nullable();
            $table->string('buyer_zip_code')->nullable();
            $table->string('buyer_email')->nullable();
            $table->string('buyer_mobile_phone_number')->nullable();
            $table->text('buyer_message')->nullable();
            $table->text('seller_message')->nullable();

            $table->tinyInteger('status');
            $table->tinyInteger('payment_status');
            $table->integer('rub_value');

            $table->timestamp('payed_at')->nullable();
            $table->timestamp('sent_at')->nullable();
            $table->timestamp('come_at')->nullable();

            $table->timestamps();

            $table->foreign('seller_user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

            $table->foreign('buyer_user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

            $table->foreign('blog_id')
                ->references('id')
                ->on('blog')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('order');
    }
}

