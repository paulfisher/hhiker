<?php

use App\Models\Article;
use App\Models\Video;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropChannelPosts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Article::where('blog_channel_id', '>', 0)->delete();

        Schema::table('article', function (Blueprint $table) {
            $table->dropForeign('article_blog_channel_id_foreign');
            $table->dropColumn([
                'channel_post_data',
                'channel_post_key',
                'blog_channel_id',
                'channel_post_likes',
                'channel_post_comments',
                'channel_post_reposts',
                'channel_post_weight',
                'channel_type',
                'hidden',
                'moderated',
                'geo_lat',
                'geo_long',
                'geo_name',
            ]);
        });

        Schema::table('blog_channel', function (Blueprint $table) {
            $table->dropColumn([
                'avg_likes',
                'avg_comments',
                'avg_reposts',
                'avg_weight']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
