<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGeo extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('country', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->index();
            $table->integer('vk_id')->index();
        });

        Schema::create('region', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('country_id')->unsigned();
            $table->integer('vk_id')->index();
            $table->string('name')->index();

            $table->foreign('country_id')
                ->references('id')
                ->on('country')
                ->onDelete('cascade');
        });

        Schema::create('city', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('country_id')->unsigned();
            $table->integer('vk_id')->index();
            $table->string('name')->index();
            $table->string('region');
            $table->string('area');
            $table->boolean('important');

            $table->foreign('country_id')
                ->references('id')
                ->on('country')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('country');
        Schema::drop('region');
        Schema::drop('city');
    }
}
