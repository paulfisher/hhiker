<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterChannelAddStatistics extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('blog_channel', function (Blueprint $table) {
            $table->integer('subscribers_count')->nullable();
            $table->decimal('avg_likes')->nullable();
            $table->decimal('avg_comments')->nullable();
            $table->decimal('avg_reposts')->nullable();
            $table->decimal('avg_weight')->nullable();
        });
        Schema::table('article', function (Blueprint $table) {
            $table->integer('channel_post_likes')->nullable();
            $table->integer('channel_post_comments')->nullable();
            $table->integer('channel_post_reposts')->nullable();
            $table->integer('channel_post_weight')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('blog_channel', function (Blueprint $table) {
            //
        });
    }
}
