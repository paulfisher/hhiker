<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterArticle extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('article', function (Blueprint $table) {

            $table->string('channel_post_key')->nullable();
            $table->text('channel_post_data')->nullable();
            $table->string('geo_lat')->nullable();
            $table->string('geo_long')->nullable();
            $table->string('geo_name')->nullable();

            $table->integer('blog_id')->unsigned()->nullable();
            $table->foreign('blog_id')
                ->references('id')
                ->on('blog')
                ->onDelete('cascade');

            $table->integer('blog_channel_id')->unsigned()->nullable();
            $table->foreign('blog_channel_id')
                ->references('id')
                ->on('blog_channel')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('article', function (Blueprint $table) {
            //
        });
    }
}
