<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChannel extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('blog', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('img')->nullable();
            $table->text('name');
            $table->text('about');
            $table->timestamps();

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
        });

        Schema::create('channel', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('type');
            $table->timestamps();
        });

        Schema::create('blog_channel', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('blog_id')->unsigned();
            $table->integer('channel_id')->unsigned();
            $table->text('url');
            $table->string('channel_blog_key');
            $table->timestamps();

            $table->foreign('blog_id')
                ->references('id')
                ->on('blog')
                ->onDelete('cascade');

            $table->foreign('channel_id')
                ->references('id')
                ->on('channel')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('blog_channel');
        Schema::drop('blog');
        Schema::drop('channel');
    }
}
