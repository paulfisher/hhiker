<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('image', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('article_id')->unsigned();

            $table->string('key')->unique()->index();

            $table->string('photo_xs');
            $table->string('photo_sm');
            $table->string('photo_md');
            $table->string('photo_lg')->nullable();
            $table->string('photo_xl')->nullable();
            $table->string('photo_xxl')->nullable();//photo_2560
            $table->timestamps();

            $table->foreign('article_id')
                ->references('id')
                ->on('article')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('image');
    }
}
