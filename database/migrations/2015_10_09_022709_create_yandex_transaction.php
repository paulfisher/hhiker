<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateYandexTransaction extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('yandex_transaction', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('order_id')->unsigned()->unique();

            //string Для переводов из кошелька — p2p-incoming. Для переводов с произвольной карты — cardincoming.
            $table->string('notification_type');

            //string Идентификатор операции в истории счета получателя.
            $table->string('operation_id');

            //Сумма, которая зачислена на счет получателя.
            $table->decimal('amount');

            //Сумма, которая списана со счета отправителя.
            $table->decimal('withdraw_amount')->nullable();

            //string Код валюты — всегда 643 (рубль РФ согласно ISO 4217).
            $table->string('currency');

            //datetime Дата и время совершения перевода.
            $table->string('datetime');

            //string Для переводов из кошелька — номер счетаотправителя. Для переводов с произвольной карты —параметр содержит пустую строку.
            $table->string('sender');

            //boolean Для переводов из кошелька — перевод защищен кодом протекции. Для переводов с произвольной карты — всегда false.
            $table->boolean('codepro');

            //string Метка платежа. Если ее нет, параметр содержит пустую строку.
            $table->string('label');

            //string SHA-1 hash параметров уведомления.
            $table->string('sha1_hash');

            $table->boolean('test_notification')->nullable();

            $table->boolean('unaccepted')->nullable();

            $table->timestamps();

            $table->foreign('order_id')
                ->references('id')
                ->on('order')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('yandex_transaction');
    }
}
