<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVideo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('video', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('article_id')->unsigned()->nullable();

            $table->string('key')->unique()->index();
            $table->integer('duration_secs')->nullable();
            $table->string('name')->nullable();
            $table->text('description')->nullable();
            $table->string('photo_sm')->nullable();
            $table->string('photo_md')->nullable();
            $table->string('photo_lg')->nullable();
            $table->timestamps();

            $table->foreign('article_id')
                ->references('id')
                ->on('article')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('video');
    }
}
