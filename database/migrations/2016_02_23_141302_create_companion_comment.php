<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanionComment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hunt_companion_comment', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('hunt_companion_id')->unsigned();

            $table->integer('hunt_companion_comment_id')->unsigned()->nullable();

            $table->text('body');
            $table->integer('status');
            $table->integer('type');

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

            $table->foreign('hunt_companion_id')
                ->references('id')
                ->on('hunt_companion')
                ->onDelete('cascade');

            $table->foreign('hunt_companion_comment_id')
                ->references('id')
                ->on('hunt_companion_comment')
                ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('hunt_companion_comment');
    }
}
