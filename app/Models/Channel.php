<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Channel extends Model
{
    protected $table = 'channel';

    protected $fillable = [
        'type',
        'name',
    ];

    public function blogs() {
        return $this->hasMany('App\Models\BlogChannel')->withTimestamps();
    }
}
