<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;


/**
 * Class Blog
 * @package App
 *
 * @property integer $id
 * @property string $about
 * @property string $img
 * @property integer $user_id
 * @property string $name
 * @property float $rating
 * @property boolean $is_accept_funding
 *
 * @property User user
 * @property BlogChannel[] blogChannels
 */
class Blog extends Model
{
    protected $table = 'blog';

    protected $fillable = [
        'about',
        'img',
        'user_id',
        'name',
        'rating',//avg - из channels avg_weight
        'is_accept_funding',
        'postcard_min_pledge',
    ];

    public function user() {
        return $this->belongsTo('App\Models\User');
    }

    public function checkIsMine() {
        if (!$this->isMine()) {
            abort(404);
        }
        return $this;
    }

    public function isMine() {
        return Auth::check() && Auth::user()->id == $this->user_id;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function blogChannels() {
        return $this->hasMany('App\Models\BlogChannel');
    }

    public function channels() {
        return $this->belongsToMany('App\Models\Channel');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function articles() {
        return $this->hasMany('App\Models\Article');
    }

    public function orders() {
        return $this->hasMany('App\Models\Orderd');
    }

    public function popular($limit = 12) {
        return $this->articles()
            ->orderBy('channel_post_weight', 'desc')
            ->published()
            ->limit($limit)
            ->get()
            ->shuffle();
    }

    public function subscribers() {
        return $this->blogChannels()->sum('subscribers_count');
    }
}
