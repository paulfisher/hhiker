<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class BlogChannel extends Model
{
    protected $table = 'blog_channel';

    protected $fillable = [
        'url',
        'channel_blog_key',
        'channel_id',
        'blog_id',
        'hashtag',
        'subscribers_count',
        'access_data',
    ];

    public function blog() {
        return $this->belongsTo('App\Models\Blog');
    }

    public function channel() {
        return $this->belongsTo('App\Models\Channel');
    }
}
