<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{

    protected $table = 'tag';

    protected $fillable = [
        'name',
        'is_popular',
    ];

    public function articles() {
        return $this->belongsToMany('App\Models\Article');
    }

    public function questions() {
        return $this->belongsToMany('App\Models\Question');
    }

    public function companions() {
        return $this->belongsToMany('App\Models\HuntCompanion');
    }
}
