<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Article extends Model {

    protected $table = 'article';

    protected $fillable = [
        'title',
        'short',
        'slug',
        'body',
        'published_at',
        'excerpt',
        'user_id',
        'main_img',
        'blog_id',
    ];

    protected $dates = ['published_at'];

    public function setPublishedAtAttribute($date) {
        $this->attributes['published_at'] = Carbon::parse($date);
    }

    public function getPublishedAtAttribute($date) {
        return Carbon::parse($date)->format('Y-m-d');
    }

    public function scopePublished($query) {
        $query->where('published_at', '<=', Carbon::now());
    }

    public function scopeUnpublished($query) {
        $query->where('published_at', '>=', Carbon::now());
    }

    public function user() {
        return $this->belongsTo('App\Models\User');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function tags() {
        return $this->belongsToMany('App\Models\Tag')->withTimestamps();
    }

    public function getTagListAttribute() {
        return $this->tags->lists('id')->toArray();
    }

    public function slugOrId() {
        return (!empty($this->slug)) ? $this->slug : $this->id;
    }

    public function blog() {
        return $this->belongsTo('App\Models\Blog');
    }

    public function images() {
        return $this->hasMany('App\Models\Image');
    }

    public function videos() {
        return $this->hasMany('App\Models\Video');
    }

    public function mainImage() {
        if ($this->main_img) {
            return $this->main_img;
        }
        if ($this->images()->exists()) {
            return $this->images()->first()->photo_md;
        }
        if ($this->videos()->exists()) {
            return $this->videos()->first()->photo_sm;
        }
        return false;
    }

    private $_prev;
    public function previous() {
        if ($this->_prev === false) {
            return null;
        }
        if (!$this->_prev) {
            $this->_prev = Article::where('blog_id', $this->blog_id)
                ->where('published_at', '<', $this->published_at)
                ->orderBy('published_at', 'desc')
                ->limit(1)
                ->first();
            if (!$this->_prev) {
                $this->_prev = false;
            }
        }
        return $this->_prev;
    }

    private $_next;
    public function next() {
        if ($this->_next === false) {
            return null;
        }
        if (!$this->_next) {
            $this->_next = Article::where('blog_id', $this->blog_id)
                ->where('published_at', '>', $this->published_at)
                ->orderBy('published_at', 'asc')
                ->limit(1)
                ->first();
            if (!$this->_next) {
                $this->_next = false;
            }
        }
        return $this->_next;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function comments() {
        return $this->hasMany('App\Models\ArticleComment');
    }
}