<?php

namespace App\Models;

use ReflectionClass;

abstract class Enum {
    static function getKeys() {
        return array_keys((new ReflectionClass(get_called_class()))->getConstants());
    }
}