<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Video extends Model {

    protected $table = 'video';

    protected $fillable = [
        'article_id',
        'user_id',
        'photo_sm',
        'photo_md',
        'photo_lg',
        'key',
        'duration_secs',
        'name',
        'description',
        'source_type',
        'url',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function article() {
        return $this->belongsTo('App\Models\Article');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user() {
        return $this->belongsTo('App\Models\User');
    }
}