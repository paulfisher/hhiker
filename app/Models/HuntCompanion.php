<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class HuntCompanion extends Model
{
    protected $table = 'hunt_companion';

    protected $fillable = [
        'about_me',
        'about_trip',
        'about_companion',
        'when',
        'contact',
        'user_id'
    ];

    public function user() {
        return $this->belongsTo('App\Models\User');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function tags() {
        return $this->belongsToMany('App\Models\Tag')->withTimestamps();
    }

    public function getTagListAttribute() {
        return $this->tags->lists('id')->toArray();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function comments() {
        return $this->hasMany('App\Models\HuntCompanionComment');
    }

    public function checkIsMine() {
        if (!$this->isMine()) {
            abort(404);
        }
        return $this;
    }

    public function isMine() {
        return Auth::check() && Auth::user()->id == $this->user_id;
    }
}
