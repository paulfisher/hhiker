<?php

namespace App\Models;

abstract class ImageSourceType extends Enum {

    const VK = 1;
    const INSTAGRAM = 2;
    const TWITTER = 3;
    const FACEBOOK = 4;

}