<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Order extends Model
{
    const POSTCARD_PRICE = 199;
    const YA_MONEY_ACC = '410013606367952';

    protected $table = 'order';

    protected $fillable = [
        'blog_id',
        'seller_user_id',
        'buyer_user_id',
        'buyer_country',
        'buyer_region',
        'buyer_city',
        'buyer_street',
        'buyer_building',
        'buyer_room',
        'buyer_zip_code',
        'buyer_mobile_phone_number',
        'buyer_email',
        'status',
        'payment_status',
        'rub_value',
        'buyer_message',
        'seller_message',
        'payed_at',
        'sent_at',
        'come_at',
    ];

    protected $dates = ['payed_at', 'sent_at', 'come_at'];

    public static function createDefault() {
        return new Order(/*[
            'buyer_country' => 'Russia',
            'buyer_region' => 'Марий Эл',
            'buyer_city' => 'Йошкар-Ола',
            'buyer_street' => 'Карла Маркса',
            'buyer_building' => '108',
            'buyer_room' => '41',
            'buyer_zip_code' => '424000',
            'buyer_mobile_phone_number' => '89600967896',
            'buyer_email' => 'pavel.rybako123@gmail.com',
            'buyer_message' => 'Спасибо вам, ребятушки',
        ]*/);
    }

    public function seller() {
        return $this->belongsTo('App\Models\User', 'seller_user_id');
    }

    public function buyer() {
        return $this->belongsTo('App\Models\User', 'buyer_user_id');
    }

    public function blog() {
        return $this->belongsTo('App\Models\Blog');
    }

    public function checkForMe() {
        if ($this->forMe()) {
            return $this;
        }
        abort(404);
        return false;
    }

    public function checkIsMineOrForMe() {
        if ($this->isMine() || $this->forMe()) {
            return $this;
        }
        abort(404);
        return false;
    }

    public function checkIsMine() {
        if (!$this->isMine()) {
            abort(404);
        }
        return $this;
    }

    public function isMine() {
        return Auth::check() && Auth::user()->id == $this->buyer_user_id;
    }

    public function forMe() {
        return Auth::check() && Auth::user()->id == $this->seller_user_id;
    }

    public function yandexTransaction() {
        return $this->hasOne('App\Models\YandexTransaction');
    }

    public function checkStatus($status) {
        if ($this->status != $status) {
            abort(404);
        }
        return $this;
    }

    public function checkPaymentStatus($paymentStatus) {
        if ($this->payment_status != $paymentStatus) {
            abort(404);
        }
        return $this;
    }

    public function address() {
        return implode(' ', [
            $this->buyer_country,
            $this->buyer_region,
            $this->buyer_city,
            $this->buyer_street,
            $this->buyer_building,
            $this->buyer_room,
        ]);
    }

    public function paymentStatusName() {
        switch ($this->payment_status) {
            case PaymentStatus::NOT_PAYED: return 'Не оплачено';
            case PaymentStatus::SUCCESS_PAYED: return 'Успешно оплачено';
            case PaymentStatus::PROCESSING: return 'В обработке';
            case PaymentStatus::CANCELLED: return 'Отмена';
            case PaymentStatus::FAIL: return 'Ошибка';
        }
        return '-';
    }

    public function statusName() {
        switch ($this->status) {
            case OrderStatus::CREATED: return 'Новый';
            case OrderStatus::CONFIRMED: return 'Подтвержден';
            case OrderStatus::SENT: return 'Отправлен';
            case OrderStatus::SUCCESS_COME: return 'Пришел';
            case OrderStatus::FAILED: return 'Ошибка';
        }
        return '-';
    }
}
