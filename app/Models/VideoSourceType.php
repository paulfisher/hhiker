<?php

namespace App\Models;

abstract class VideoSourceType extends Enum {

    const YOUTUBE = 1;
    const VK = 2;
    const VIMEO = 3;

}