<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

/**
 * Class User
 * @package App
 *
 * @property string $name
 * @property string $email
 * @property string $password
 * @property string $img
 * @property boolean $is_receive_subscription
 * @property boolean is_active
 */
class User extends Model implements AuthenticatableContract, CanResetPasswordContract
{
    use Authenticatable, CanResetPassword;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'img',
        'yandex_money_account',
        'is_receive_subscription',
        'is_active',
        'activation_hash',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    public function articles() {
        return $this->hasMany('App\Models\Article');
    }

    public function companions() {
        return $this->hasMany('App\Models\HuntCompanion');
    }

    public function couches() {
        return $this->hasMany('App\Models\Couch');
    }

    public function blogs() {
        return $this->hasMany('App\Models\Blog');
    }

    public function saleOrders() {
        return $this->hasMany('App\Models\Order', 'seller_user_id');
    }

    public function buyOrders() {
        return $this->hasMany('App\Models\Order', 'buyer_user_id');
    }

    public function vkAccount() {
        return $this->hasOne('App\Models\VkAccount');
    }

    public function questions() {
        return $this->hasMany('App\Models\Question');
    }

    public function videos() {
        return $this->hasMany('App\Models\Video');
    }
}