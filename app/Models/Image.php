<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Image extends Model {

    protected $table = 'image';

    protected $fillable = [
        'article_id',
        'photo_xs',
        'photo_sm',
        'photo_md',
        'photo_lg',
        'photo_xl',
        'photo_xxl',
        'key',
        'source_type',
    ];

    public function article() {
        return $this->belongsTo('App\Models\Article');
    }
}