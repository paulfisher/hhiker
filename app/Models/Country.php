<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Country extends Model
{
    protected $table = 'country';

    public $timestamps = false;

    protected $fillable = [
        'name',
        'vk_id',
    ];

    public function cities() {
        return $this->hasMany('App\Models\City');
    }
}
