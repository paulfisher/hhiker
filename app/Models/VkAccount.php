<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VkAccount extends Model
{
    protected $table = 'vk_account';

    protected $fillable = [
        'uid',
        'first_name',
        'last_name',
        'photo_400_orig',
        'photo_max_orig',
        'email',
        'about',
        'user_id'
    ];

    public function user() {
        return $this->belongsTo('App\Models\User');
    }

    public function fullName() {
        return $this->first_name . " " . $this->last_name;
    }
}
