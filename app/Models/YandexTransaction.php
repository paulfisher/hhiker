<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class YandexTransaction extends Model
{
    protected $table = 'yandex_transaction';

    protected $fillable = [
        'order_id',
        'notification_type',
        'operation_id',
        'amount',
        'withdraw_amount',
        'currency',
        'datetime',
        'sender',
        'codepro',
        'label',
        'sha1_hash',
        'unaccepted',
        'test_notification',
    ];

    public function order() {
        return $this->belongsTo('App\Models\Order');
    }

}
