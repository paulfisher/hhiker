<?php

namespace App\Models;

use App\Http\Requests\BlogReadCriteriaRequest;

class BlogReadCriteria {

    const VIEW_MODE_TABLE = 'view_table';
    const VIEW_MODE_LIST = 'view_list';
    const VIEW_MODE_STRIP = 'view_strip';

    public $onlyPopular;

    public $viewMode;

    public $sortBy;
    public $order;

    public function __construct() {
        $this->onlyPopular = false;

        $this->viewMode = self::VIEW_MODE_TABLE;

        $this->sortBy = 'published_at';
        $this->order = 'desc';
    }

    public function update(BlogReadCriteriaRequest $request) {
        if ($request->query->has('onlyPopular')) $this->onlyPopular = $request->onlyPopular;
        if ($request->query->has('viewMode')) $this->viewMode = $request->viewMode;
        if ($request->query->has('sortBy')) $this->sortBy = $request->sortBy;
        if ($request->query->has('order')) $this->order = $request->order;

        if (!$this->viewMode) {
            $this->viewMode = self::VIEW_MODE_TABLE;
        }
    }
}