<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property HuntCompanionComment parentComment
 * @property User user
 */
class HuntCompanionComment extends Model
{
    protected $table = 'hunt_companion_comment';

    protected $fillable = [
        'body',
        'status',
        'user_id',
        'hunt_companion_id',
        'hunt_companion_comment_id',
    ];

    public $timestamps = true;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user() {
        return $this->belongsTo('App\Models\User');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function comments() {
        return $this->hasMany('App\Models\HuntCompanionComment');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function parentComment() {
        return $this->belongsTo('App\Models\HuntCompanionComment', 'hunt_companion_comment_id');
    }
}
