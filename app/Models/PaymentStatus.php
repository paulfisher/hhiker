<?php

namespace App\Models;

abstract class PaymentStatus extends Enum {

    const NOT_PAYED = 1;
    const PROCESSING = 2;
    const FAIL = 3;
    const CANCELLED = 4;
    const SUCCESS_PAYED = 10;
}