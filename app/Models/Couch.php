<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Couch extends Model {

    protected $table = 'couch';

    protected $fillable = [
        'user_id',
        'about_couch',
        'contact',
        'city_id',
        'couchsurf_account',
    ];

    public function getCityNameAttribute() {
        return $this->city ? $this->city->name : '';
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function city() {
        return $this->belongsTo('App\Models\City');
    }

    public function user() {
        return $this->belongsTo('App\Models\User');
    }

    public function isMine() {
        return Auth::check() && Auth::user()->id == $this->user_id;
    }

    public function checkIsMine() {
        if (!$this->isMine()) {
            abort(404);
        }
        return $this;
    }


}
