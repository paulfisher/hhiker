<?php

namespace App\Models;

use Carbon\Carbon;
use Google_Service_YouTube_ResourceId;
use Google_Service_YouTube_SearchResultSnippet;
use Google_Service_YouTube_ThumbnailDetails;

class VideoInfo {

    public $key;
    public $title;
    public $description;
    public $publishedAt;

    public $data;

    public $thumbSmall = '';
    public $thumbMedium = '';
    public $thumbLarge = '';

    public $likes;
    public $comments;
    public $reposts;

    /**
     * @param Google_Service_YouTube_ResourceId $resourceId
     * @param Google_Service_YouTube_SearchResultSnippet $snippet
     * @return VideoInfo
     */
    public static function fromYoutubeSearchResultSnippet(Google_Service_YouTube_ResourceId $resourceId, Google_Service_YouTube_SearchResultSnippet $snippet) {
        $videoInfo = new VideoInfo();
        $videoInfo->key = $resourceId->getVideoId();
        $videoInfo->title = $snippet->getTitle();
        $videoInfo->description = $snippet->getDescription();
        $videoInfo->publishedAt = Carbon::createFromTimestamp(date("U",strtotime($snippet->getPublishedAt())));


        /** @var Google_Service_YouTube_ThumbnailDetails $snippetThumbs */
        $snippetThumbs = $snippet->getThumbnails();
        if ($snippetThumbs != null) {
            $videoInfo->thumbSmall = $snippetThumbs->getMedium() ? $snippetThumbs->getMedium()->url : '';
            $videoInfo->thumbMedium = $snippetThumbs->getHigh() ? $snippetThumbs->getHigh()->url : '';
            $videoInfo->thumbLarge = $snippetThumbs->getMaxres() ? $snippetThumbs->getMaxres()->url : '';
        }

        $videoInfo->data = json_encode([
            'Google_Service_YouTube_ResourceId' => $resourceId,
            'Google_Service_YouTube_SearchResultSnippet' => $snippet
        ]);

        return $videoInfo;
    }
}