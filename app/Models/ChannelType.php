<?php

namespace App\Models;

abstract class ChannelType extends Enum {

    const VK_GROUP = 1;
    const VK_PROFILE = 2;
    const INSTAGRAM = 3;
    const YOUTUBE = 4;

}