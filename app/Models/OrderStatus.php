<?php

namespace App\Models;

abstract class OrderStatus extends Enum {

    const CREATED = 1;
    const CONFIRMED = 2;
    const SENT = 3;
    const CANCELLED = 4;
    const FAILED = 5;
    const SUCCESS_COME = 10;
}