<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

/**
 * @property User user
 */
class Question extends Model
{
    protected $table = 'question';

    protected $fillable = [
        'title',
        'body',
        'status',
        'user_id',
    ];

    public $timestamps = true;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user() {
        return $this->belongsTo('App\Models\User');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function comments() {
        return $this->hasMany('App\Models\QuestionComment');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function tags() {
        return $this->belongsToMany('App\Models\Tag')->withTimestamps();
    }

    public function getTagListAttribute() {
        return $this->tags->lists('id')->toArray();
    }

    public function checkIsMine() {
        if (!$this->isMine()) {
            abort(404);
        }
        return $this;
    }

    public function isMine() {
        return Auth::check() && Auth::user()->id == $this->user_id;
    }
}
