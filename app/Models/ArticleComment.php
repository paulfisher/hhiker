<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property ArticleComment|null parentComment
 */
class ArticleComment extends Model
{
    protected $table = 'article_comment';

    protected $fillable = [
        'body',
        'status',
        'user_id',
        'article_id',
        'article_comment_id',
    ];

    public $timestamps = true;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user() {
        return $this->belongsTo('App\Models\User');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function comments() {
        return $this->hasMany('App\Models\ArticleComment');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function parentComment() {
        return $this->belongsTo('App\Models\ArticleComment', 'article_comment_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function article() {
        return $this->belongsTo('App\Models\Article');
    }
}
