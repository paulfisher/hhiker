<?php

namespace App\Services\Channels;

use App\Models\Article;
use App\Models\BlogChannel;
use App\Services\StringHelper;
use App\Models\Video;
use App\Models\VideoInfo;
use App\Models\VideoSourceType;

use Google_Client;

use Google_Service_YouTube;
use Google_Service_YouTube_Channel;
use Google_Service_YouTube_ChannelStatistics;


use Google_Service_YouTube_SearchResult;


use Google_Service_YouTube_VideoStatistics;

class YoutubeChannelImportService {

    private $apiKey = "AIzaSyBHXarhqcQzyfTIWoWnUHX8w6mtnEDCbJM";
    private $applicationName = 'HHiker.ru';

    /**
     * @var Google_Service_YouTube
     */
    private $_youtubeClient;

    public function import(BlogChannel $blogChannel) {
        echo "importing " . $blogChannel->url . "\n";

        $channelIds = explode(' ', $blogChannel->channel_blog_key);
        $subscribers = 0;
        foreach ($channelIds as $channelId) {
            $statistics = $this->getYoutubeClient()->channels->listChannels('statistics', [
                'id' => $channelId
            ]);
            /** @var Google_Service_YouTube_Channel $statisticsItem */
            $statisticsItem = $statistics->getItems()[0];

            /** @var Google_Service_YouTube_ChannelStatistics $statisticsInfo */
            $statisticsInfo = $statisticsItem->getStatistics();
            $subscribers += $statisticsInfo->getSubscriberCount();
        }
        $blogChannel->update(['subscribers_count' => $subscribers]);
        foreach ($channelIds as $channelId) {
            $this->loadChannelVideos($channelId, $blogChannel);
        }
    }

    private function loadChannelVideos($channelId, BlogChannel $blogChannel) {
        $pageToken = null;
        do {
            $searchResults = $this->search($channelId, $pageToken);
            foreach ($searchResults->getItems() as $searchResultItem) {
                /** @var Google_Service_YouTube_SearchResult $searchResultItem */

                $videoInfo = VideoInfo::fromYoutubeSearchResultSnippet($searchResultItem->getId(), $searchResultItem->getSnippet());
                $this->populateStats($videoInfo);
                $this->saveArticle($blogChannel, $videoInfo);
            }
            $pageToken = $searchResults->getNextPageToken();
        } while ($pageToken);
    }

    /**
     * @param $channelId
     * @param null $pageToken
     * @return \Google_Service_YouTube_SearchListResponse
     */
    private function search($channelId, $pageToken = null) {
        return $this->getYoutubeClient()->search->listSearch('id,snippet', [
            'channelId' => $channelId,
            'pageToken' => $pageToken,
            'type' => 'video',
        ]);
    }

    private function saveArticle(BlogChannel $blogChannel, VideoInfo $videoInfo) {
        if ($blogChannel->hashtag) {
            if (strpos($videoInfo->description, '#' . $blogChannel->hashtag) === false) {
                return;
            }
        }

        $article = Article::where('channel_post_key', $videoInfo->key)
            ->where('blog_channel_id', $blogChannel->id)
            ->first();

        if (!$article) {
            $title = $blogChannel->blog->name . " #" . $videoInfo->title;
            $article = Article::create([
                'title' => $title,
                'short' => mb_substr($videoInfo->description, 0, 70),
                'slug' => $blogChannel->id . "_" . $videoInfo->key . "_" . StringHelper::slugIt($title),
                'body' => $videoInfo->description,
                'user_id' => $blogChannel->blog->user_id,
                'blog_channel_id' => $blogChannel->id,
                'channel_post_key' => $videoInfo->key,
                'published_at' => $videoInfo->publishedAt,
                'blog_id' => $blogChannel->blog_id,
                'channel_type' => $blogChannel->channel->type,
            ]);
            //echo "new article" . $article->slug . "\n";
        } else {
            //echo "article exists " . $article->slug . "\n";
        }

        $article->update([
            'published_at' => $videoInfo->publishedAt,
            'channel_post_weight' => $this->calculatePostWeigt($videoInfo),
            'channel_post_likes' => $videoInfo->likes,
            'channel_post_comments' => $videoInfo->comments,
            'channel_post_reposts' => $videoInfo->reposts,
            'body' => $videoInfo->description,
            'channel_post_data' => $videoInfo->data,
            'geo_lat' => '',
            'geo_long' => '',
            'geo_name' => '',
            'blog_id' => $blogChannel->blog_id,
            'channel_type' => $blogChannel->channel->type,
        ]);
        $this->saveVideo($article, $videoInfo);
    }

    private function saveVideo(Article $article, VideoInfo $videoInfo) {
        if (!Video::where('key', $videoInfo->key)->where('source_type', VideoSourceType::YOUTUBE)->exists()) {
            $article->videos()->create([
                'key' => $videoInfo->key,
                'source_type' => VideoSourceType::YOUTUBE,
                'duration_secs' => 0,
                'name' => $videoInfo->title,
                'description' => $videoInfo->description,
                'photo_sm' => $videoInfo->thumbSmall,
                'photo_md' => $videoInfo->thumbMedium,
                'photo_lg' => $videoInfo->thumbLarge,
                'url' => 'https://www.youtube.com/watch?v=' . $videoInfo->key
            ]);
        }
    }

    /**
     * @return Google_Service_YouTube
     */
    private function getYoutubeClient() {
        if (!$this->_youtubeClient) {
            $client = new Google_Client();
            $client->setApplicationName($this->applicationName);
            $client->setDeveloperKey($this->apiKey);
            $this->_youtubeClient = new Google_Service_YouTube($client);
        }
        return $this->_youtubeClient;
    }

    public function testService() {
        var_dump($this->getChannelId("https://www.youtube.com/user/inna2868?sub_confirmation=1"));
        var_dump($this->getChannelId("https://www.youtube.com/channel/UCWJ2lWNubArHWmf3FIHbfcQ"));
        var_dump($this->getChannelId("https://www.youtube.com/channel/UCmFqRwdntCmVL114XQW-V-Q"));
    }

    /**
     * @param $url
     * @return array|bool|mixed
     */
    public function getChannelId($url) {
        $urlInfo = parse_url($url);
        if (!isset($urlInfo['path'])) {
            return false;
        }
        $path = $urlInfo['path'];
        if (strpos($path, '/user/') !== false) {
            $userId = str_replace('/user/', '', $path);
            return $this->getChannelIdForUser($userId);
        } else if (strpos($path, '/channel/') !== false) {
            $channelId = str_replace('/channel/', '', $path);
            return $channelId;
        } else {
            return false;
        }
    }

    public function getChannelIdForUser($userId) {
        $youtubeClient = $this->getYoutubeClient();
        /** @var \Google_Service_YouTube_ChannelListResponse $channelList */
        $channelList = $youtubeClient->channels->listChannels("id", [
            'forUsername' => $userId
        ]);
        $result = [];
        foreach ($channelList->getItems() as $channelItem) {
            /** @var Google_Service_YouTube_Channel $channelItem */
            $result[] = $channelItem->getId();
        }
        return $result;
    }

    private function populateStats(VideoInfo $videoInfo) {
        $stats = $this->getYoutubeClient()->videos->listVideos('id,statistics', [
            'id' => $videoInfo->key
        ]);
        /** @var Google_Service_YouTube_VideoStatistics $statistics */
        $statistics = $stats->getItems()[0]->getStatistics();
        $videoInfo->likes = $statistics->getLikeCount() + $statistics->getDislikeCount();//ДА, именно складываем. черный пиар - тоже пиар
        $videoInfo->comments = $statistics->getCommentCount();
        $videoInfo->reposts = $statistics->getFavoriteCount();
    }

    private function calculatePostWeigt(VideoInfo $videoInfo) {
        return $videoInfo->likes + 2 * $videoInfo->comments + 4 * $videoInfo->reposts;
    }
}