<?php

namespace App\Services\Channels;

use App\Models\Article;
use App\Models\Blog;
use App\Models\BlogChannel;
use App\Models\Channel;
use App\Models\Image;
use App\Models\ImageSourceType;
use App\Services\StringHelper;
use App\Models\Video;
use App\Models\VideoSourceType;
use Carbon\Carbon;

class VkGroup537ChannelImportService {

    private $groupGetUrl = 'https://api.vk.com/method/groups.getById?v=5.37&group_ids={groupName}';
    private $groupGetMembers = 'https://api.vk.com/method/groups.getMembers?v=5.37&group_id={groupId}';

    private $wallGetUrl = 'https://api.vk.com/method/wall.get?v=5.37&owner_id={ownerId}&count={count}&offset={offset}';

    private $videoGetUrl = 'https://api.vk.com/method/video.get?v=5.37&owner_id={ownerId}&videos={videos}&access_key={accessKey}';

    private $countPerRequest = 100;

    public function import(BlogChannel $blogChannel) {
        $max = 250;
        echo "importing " . $blogChannel->url . "\n";

        $subscribers = $this->getGroupSubscribers($blogChannel->channel_blog_key);
        $blogChannel->update(['subscribers_count' => $subscribers]);

        $offset = 0;
        $count = $this->countPerRequest;
        do {
            $response = $this->loadItems($blogChannel, $offset, $count);
            if (!$response) {
                echo "load items error for blogChannel " . $blogChannel->id . "\n";
                return null;
            }
            $this->saveItems($blogChannel, $response['items']);

            $total = $response['count'];
            echo "total items " . $total . "\n";

            $offset += $count;
        } while ($offset < $total || $offset < $max);
    }

    public function loadItems(BlogChannel $blogChannel, $offset, $count) {
        $url = strtr($this->wallGetUrl, [
            '{ownerId}' => $blogChannel->channel_blog_key,
            '{offset}' => $offset,
            '{count}' => $count
        ]);
        echo "api call " . $url . "\n";
        $json = json_decode(file_get_contents($url), true);

        if (!isset($json['response'])) {
            echo 'No response for blog channel ' . $blogChannel->id . "\n";
            return null;
        }
        return $json['response'];
    }

    public function saveItems(BlogChannel $blogChannel, $items) {
        foreach ($items as $item) {
            $channelPostKey = $item['id'];
            $article = Article::where('channel_post_key', $channelPostKey)
                ->where('blog_channel_id', $blogChannel->id)
                ->first();


            $text = $this->removeEmoji($item['text']);
            $text = $this->handleHashtags($text);
            $text = $this->handleLinks($text);
            if ($blogChannel->hashtag) {
                if (strpos($text, '#' . $blogChannel->hashtag) === false) {
                    continue;
                }
            }
            if (empty($text)) {
                //Пустые не нужны
                continue;
            }
            if (isset($item['copy_history']) && !empty($item['copy_history'])) {
                //Репосты незачем постить
                continue;
            }
            $likes = $item['likes']['count'];
            $reposts = $item['reposts']['count'];
            $comments = $item['comments']['count'];

            $geoLat = '';
            $geoLong = '';
            $geoName = '';
            if (isset($item['geo']) && isset($item['geo']['coordinates'])) {
                $geo = $item['geo'];
                list($geoLat, $geoLong) = explode(" ", $geo['coordinates']);
                if (isset($geo['place'])) {
                    $geoPlace = $geo['place'];
                    $geoName = $geoPlace['title'] . " " . $geoPlace['country'];
                }
            }
            if (!$article) {
                $title = $blogChannel->blog->name . " #" . $channelPostKey;
                $slug = $blogChannel->blog->id . "_" . $blogChannel->id . "_" . StringHelper::slugIt($title);
                $article = Article::create([
                    'title' => $title,
                    'short' => mb_substr(strip_tags($text), 0, 120),
                    'slug' => $slug,
                    'body' => $text,
                    'user_id' => $blogChannel->blog->user_id,
                    'blog_channel_id' => $blogChannel->id,
                    'channel_post_key' => $channelPostKey,
                    'published_at' => Carbon::now(),
                    'channel_type' => $blogChannel->channel->type,
                ]);
                //echo "new article" . $article->slug . "\n";
            } else {
                //echo "article exists " . $article->slug . "\n";
            }

            $article->timestamps = false;
            $article->update([
                'channel_post_weight' => $this->calculatePostWeigt($likes, $comments, $reposts),
                'channel_post_likes' => $likes,
                'channel_post_comments' => $comments,
                'channel_post_reposts' => $reposts,
                'body' => $text,
                'channel_post_data' => json_encode($item),
                'geo_lat' => $geoLat,
                'geo_long' => $geoLong,
                'geo_name' => $geoName,
                'blog_id' => $blogChannel->blog_id,
                'published_at' => Carbon::createFromTimestamp($item['date']),
                'channel_type' => $blogChannel->channel->type,
            ]);

            if (isset($item['attachments'])) {
                $this->syncAttachments($article, $item['attachments']);
            }
        }
    }

    private function removeEmoji($text) {

        $clean_text = "";

        // Match Emoticons
        $regexEmoticons = '/[\x{1F600}-\x{1F64F}]/u';
        $clean_text = preg_replace($regexEmoticons, '', $text);

        // Match Miscellaneous Symbols and Pictographs
        $regexSymbols = '/[\x{1F300}-\x{1F5FF}]/u';
        $clean_text = preg_replace($regexSymbols, '', $clean_text);

        // Match Transport And Map Symbols
        $regexTransport = '/[\x{1F680}-\x{1F6FF}]/u';
        $clean_text = preg_replace($regexTransport, '', $clean_text);

        // Match flags (iOS)
        $regexTransport = '/[\x{1F1E0}-\x{1F1FF}]/u';
        $clean_text = preg_replace($regexTransport, '', $clean_text);


        return $clean_text;
    }

    public function getGroupSubscribers($groupId) {
        $url = strtr($this->groupGetMembers, ['{groupId}' => str_replace('-', '', $groupId)]);
        $rawResponse = file_get_contents($url);
        $response = json_decode($rawResponse, true);
        if (isset($response['response'])) {
            return $response['response']['count'];
        }
        throw new \Exception($rawResponse);
    }

    public function getOwnerId($url) {
        $urlInfo = parse_url($url);
        $groupName = str_replace('/', '', $urlInfo['path']);
        if (strpos($groupName, 'public') !== false) {
            $groupId = str_replace('public', '', $groupName);
            if (is_numeric($groupId)) {
                return intval($groupId);
            }
        }

        $url = strtr($this->groupGetUrl, ['{groupName}' => $groupName]);
        $response = json_decode(file_get_contents($url), true);

        if (isset($response['error'])) {
            throw new \Exception($response['error']['error_msg']);
        }

        return $response['response'][0]['id'];
    }

    private function syncAttachments(Article $article, $attachments) {
        foreach ($attachments as $attach) {
            if ($attach['type'] === 'photo') {
                $this->savePhotoAttachment($article, $attach['photo']);
            } elseif ($attach['type'] == 'video') {
                $this->saveVideoAttachment($article, $attach['video']);
            }
        }
    }

    private function savePhotoAttachment(Article $article, $attach) {
        $key = $attach['id'];
        if (!Image::where('key', $key)->where('source_type', ImageSourceType::VK)->exists()) {
            $article->images()->create([
                'key' => $key,
                'source_type' => ImageSourceType::VK,
                'photo_xs' => $this->arrayGet($attach, 'photo_75'),
                'photo_sm' => $this->arrayGet($attach, 'photo_130'),
                'photo_md' => $this->arrayGet($attach, 'photo_604'),
                'photo_lg' => $this->arrayGet($attach, 'photo_807'),
                'photo_xl' => $this->arrayGet($attach, 'photo_1280'),
                'photo_xxl' => $this->arrayGet($attach, 'photo_2560'),
            ]);
        }
    }

    private function saveVideoAttachment(Article $article, $attach) {
        $key = $attach['id'];
        if (!Video::where('key', $key)->where('source_type', VideoSourceType::VK)->exists()) {
            $article->videos()->create([
                'key' => $key,
                'source_type' => VideoSourceType::VK,
                'duration_secs' => $this->arrayGet($attach, 'duration', 0),
                'name' => $this->arrayGet($attach, 'title'),
                'description' => $this->arrayGet($attach, 'description'),
                'photo_sm' => $this->arrayGet($attach, 'photo_130'),
                'photo_md' => $this->arrayGet($attach, 'photo_320'),
                'photo_lg' => $this->arrayGet($attach, 'photo_640'),
            ]);
        }
    }

    private function arrayGet($array, $key, $default = '') {
        return isset($array[$key]) ? $array[$key] : $default;
    }

    private function calculatePostWeigt($likes, $comments, $reposts) {
        return $likes + ($comments * 2) + ($reposts * 4);
    }

    private function handleLinks($text) {
        preg_match_all('/\[(club|id)(\d*?)\|(.*?)\]/', $text, $linkMatches);
        $replacePairs = [];
        if (count($linkMatches) == 4) {
            list($matches, $typeMatches, $idMatches, $nameMatches) = $linkMatches;
            for ($i = 0; $i < count($matches); $i++) {
                $href = 'https://vk.com/' . $typeMatches[$i] . $idMatches[$i];
                $name = $nameMatches[$i];
                $replacePairs[$matches[$i]] = '<a href="' . $href . '" target="_blank" class="vk_link">' . $name . '</a>';
            }
        }
        return strtr($text, $replacePairs);
    }

    private function handleHashtags($text) {
        preg_match_all('/#(.*?)(\s|$)/', $text, $hashtagsMatches);
        $replacePairs = [];
        if (isset($hashtagsMatches[0])) {
            foreach ($hashtagsMatches[0] as $hashtag) {
                $name = str_replace('#', '', $hashtag);
                $replacePairs[$hashtag] = '<a href="/search?hashtag=1&q=' . $name . '" class="hashtag">' . $hashtag . '</a>';
            }
        }
        return strtr($text, $replacePairs);
    }
}