<?php

namespace App\Services\Channels;

use App\Models\Article;
use App\Models\BlogChannel;
use App\Models\ChannelType;
use App\Models\Image;
use App\Models\ImageSourceType;
use App\Services\StringHelper;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use MetzWeb\Instagram\Instagram;

class InstagramImportService {

    const API_KEY = '77cf637f42ba422eab73873f23f5d5ef';
    const API_SECRET = '310851b998a24f7ca72e4f3ef9a85541';
    const MY_CB_VERIFY_TOKEN = 'BNc06eXwiq';

    public function import(BlogChannel $blogChannel, $token) {
        echo "importing " . $blogChannel->url . "\n";

        $baseUrl = url();
        $instagram = new Instagram(array(
            'apiKey'      => self::API_KEY,
            'apiSecret'   => self::API_SECRET,
            'apiCallback' => $baseUrl . '/instagram?blogChannelId=' . $blogChannel->id
        ));
        $instagram->setAccessToken($token);

        $instagramUser = $instagram->getUser();

        if (!$instagramUser) {
            echo "instagram user is null\n";
            return;
        }

        if (!isset($instagramUser->data)) {
            echo "instagram user data is not set\n";
            echo serialize($instagramUser) . "\n";
            return;
        }
        $me = $instagram->getUser()->data;

        $profilePicture = $me->profile_picture;
        /** @var User $user */
        $user = $blogChannel->blog->user;
        if (!$user->img && strpos($profilePicture, 'anonymous') === false) {
            $user->img = $profilePicture;
            $user->save();
        }

        $counts = $me->counts;
        $subscribers = $counts->followed_by;
        $mediaTotal = $counts->media;

        $blogChannel->channel_blog_key = $me->id;
        $blogChannel->subscribers_count = $subscribers;
        $blogChannel->save();

        $feed = $instagram->getUserMedia('self', $mediaTotal);

        foreach ($feed->data as $media) {
            $this->loadMedia($blogChannel, $instagram->getMedia($media->id)->data);
        }
        //скачать всю ленту и подписаться на новое
    }

    private function loadMedia(BlogChannel $blogChannel, $media) {
        if ($media->type == 'image') {
            $this->loadImage($blogChannel, $media);
        }
    }

    private function loadImage(BlogChannel $blogChannel, $media) {
        $link = $media->link;
        $info = parse_url($link);
        $mediaKey = strtr($info['path'], ['/p/' => '', '/' => '']);

        $title = !empty($media->caption) ? $media->caption->text : $blogChannel->blog->name . ' #' . $mediaKey;

        $article = Article::where('channel_post_key', $mediaKey)
            ->where('blog_channel_id', $blogChannel->id)
            ->first();

        if (!$article) {
            $shortTitle = mb_substr($title, 0, 80);
            $articleAttrs = [
                'title' => $shortTitle,
                'short' => mb_substr($title, 0, 120),
                'slug' => $blogChannel->id . "_" . $mediaKey . "_" . StringHelper::slugIt($shortTitle),
                'body' => $this->handleHashtags($title),
                'published_at' => Carbon::createFromTimestamp($media->created_time),
                'user_id' => $blogChannel->blog->user_id,
                'blog_channel_id' => $blogChannel->id,
                'channel_post_key' => $mediaKey,
                'channel_type' => ChannelType::INSTAGRAM,

            ];
            $article = Article::create($articleAttrs);
        }
        $likes = $media->likes->count;
        $reposts = count($media->users_in_photo);
        $comments = $media->comments->count;
        $article->update([
            'title' => mb_substr($title, 0, 80),
            'short' => mb_substr($title, 0, 120),
            'body' => $this->handleHashtags($title),
            'main_img' => $media->images->standard_resolution->url,
            'geo_lat' => '',
            'geo_long' => '',
            'geo_name' => '',
            'blog_id' => $blogChannel->blog_id,
            'channel_post_weight' => $this->calculatePostWeigt($likes, $comments, $reposts),
            'channel_post_likes' => $likes,
            'channel_post_reposts' => $reposts,
            'channel_post_comments' => $comments,
            'channel_post_data' => json_encode($media),
        ]);

        if (!Image::where('key', $mediaKey)->where('source_type', ImageSourceType::INSTAGRAM)->exists()) {
            $article->images()->create([
                'article_id' => '',
                'photo_xs' => $media->images->thumbnail->url,
                'photo_sm' => $media->images->low_resolution->url,
                'photo_md' => $media->images->standard_resolution->url,
                'photo_lg' => $media->images->standard_resolution->url,
                'photo_xl' => $media->images->standard_resolution->url,
                'photo_xxl' => $media->images->standard_resolution->url,
                'key' => $mediaKey,
                'source_type' => ImageSourceType::INSTAGRAM
            ]);
        }
    }

    private function calculatePostWeigt($likes, $comments, $reposts) {
        return $likes + ($comments * 2) + ($reposts * 4);
    }

    private function handleHashtags($text) {
        preg_match_all('/#(.*?)(\s|$)/', $text, $hashtagsMatches);
        $replacePairs = [];
        if (isset($hashtagsMatches[0])) {
            foreach ($hashtagsMatches[0] as $hashtag) {
                $name = str_replace('#', '', $hashtag);
                $replacePairs[$hashtag] = '<a href="/search?hashtag=1&q=' . $name . '" class="hashtag">' . $hashtag . '</a>';
            }
        }
        return strtr($text, $replacePairs);
    }
}