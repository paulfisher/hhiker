<?php

namespace App\Services\Channels;

use App\Models\Blog;
use App\Models\BlogChannel;
use App\Models\Channel;
use App\Models\ChannelType;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;

class ChannelImportService {

    public function update(BlogChannel $blogChannel) {
        switch ($blogChannel->channel->type) {
            case ChannelType::YOUTUBE:
                /** @var YoutubeChannelImportService $youtubeChannelImportService*/
                $youtubeChannelImportService = App::make('YoutubeChannelImportService');
                $youtubeChannelImportService->import($blogChannel);
                break;
            case ChannelType::VK_GROUP:
                /** @var VkGroup537ChannelImportService $vkGroup537ChannelImportService */
                $vkGroup537ChannelImportService = App::make('VkGroup537ChannelImportService');
                $vkGroup537ChannelImportService->import($blogChannel);
                break;
            case ChannelType::INSTAGRAM:
                if (!empty($blogChannel->access_data)) {
                    /** @var InstagramImportService $instagramImportService */
                    $instagramImportService = App::make('InstagramImportService');
                    $instagramImportService->import($blogChannel, $blogChannel->access_data);
                }
                break;
        }
        $this->updateRatings($blogChannel);
    }

    private function updateRatings(BlogChannel $blogChannel) {
        $blogChannel->update([
            'avg_likes' => DB::table('article')->where('blog_channel_id', $blogChannel->id)->avg('channel_post_likes'),
            'avg_comments' => DB::table('article')->where('blog_channel_id', $blogChannel->id)->avg('channel_post_comments'),
            'avg_reposts' => DB::table('article')->where('blog_channel_id', $blogChannel->id)->avg('channel_post_reposts'),
            'avg_weight' => DB::table('article')->where('blog_channel_id', $blogChannel->id)->avg('channel_post_weight'),
        ]);
    }
}