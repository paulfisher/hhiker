<?php

namespace App\Services;

class StringHelper {

    public static function translit($str) {
        static $converter = array(
            'а' => 'a', 'б' => 'b', 'в' => 'v','г' => 'g', 'д' => 'd', 'е' => 'e',
            'ё' => 'e', 'ж' => 'zh','з' => 'z','и' => 'i', 'й' => 'y', 'к' => 'k',
            'л' => 'l', 'м' => 'm', 'н' => 'n','о' => 'o', 'п' => 'p', 'р' => 'r',
            'с' => 's', 'т' => 't', 'у' => 'u','ф' => 'f', 'х' => 'h', 'ц' => 'c',
            'ч' => 'ch','ш' => 'sh','щ' => 'sch','ь' => '',  'ы' => 'y', 'ъ' => '',
            'э' => 'e', 'ю' => 'yu','я' => 'ya','А' => 'A', 'Б' => 'B', 'В' => 'V',
            'Г' => 'G', 'Д' => 'D', 'Е' => 'E','Ё' => 'E', 'Ж' => 'Zh','З' => 'Z',
            'И' => 'I', 'Й' => 'Y', 'К' => 'K','Л' => 'L', 'М' => 'M', 'Н' => 'N',
            'О' => 'O', 'П' => 'P', 'Р' => 'R','С' => 'S', 'Т' => 'T', 'У' => 'U',
            'Ф' => 'F', 'Х' => 'H', 'Ц' => 'C','Ч' => 'Ch','Ш' => 'Sh','Щ' => 'Sch',
            'Ь' => '',  'Ы' => 'Y', 'Ъ' => '','Э' => 'E', 'Ю' => 'Yu','Я' => 'Ya',
        );
        return strtr($str, $converter);
    }


    public static function slugIt($text) {
        $text = self::translit($text);

        // replace non letter or digits by -
        $text = preg_replace('~[^\\pL\d]+~u', '-', $text);

        // trim
        $text = trim($text, '-');

        // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT//IGNORE', $text);

        // lowercase
        $text = strtolower($text);

        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);

        if (empty($text)) {
            return null;
        }

        return $text;
    }

    public static function br2nl( $string, $separator = PHP_EOL ) {
        $separator = in_array($separator, array("\n", "\r", "\r\n", "\n\r", chr(30), chr(155), PHP_EOL)) ? $separator : PHP_EOL;  // Checks if provided $separator is valid.
        return preg_replace('/\<br(\s*)?\/?\>/i', $separator, $string);
    }

}