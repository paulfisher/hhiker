<?php

namespace App\Services;

use App\Models\VideoSourceType;

class VideoHelper {

    public static function getVideoSourceType($src) {
        if (!preg_match("@^https?://@", $src)) {
            $src = 'http://' . $src;
        }
        $urlInfo = parse_url($src);
        if (is_null($urlInfo)) return false;

        if (!isset($urlInfo['host'])) {
            return false;
        }

        $youtubeHosts = array('youtube.com','youtu.be','www.youtu.be', 'www.youtube.com');
        $vimeoHosts = array('vimeo.com', 'www.vimeo.com');

        $host = $urlInfo['host'];

        if (in_array($host, $youtubeHosts)) return VideoSourceType::YOUTUBE;
        if (in_array($host, $vimeoHosts)) return VideoSourceType::VIMEO;

        return false;
    }

    public static function getVideoId($src, $type) {
        switch ($type) {
            case VideoSourceType::YOUTUBE:
                preg_match("/^(?:http(?:s)?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:(?:watch)?\?(?:.*&)?v(?:i)?=|(?:embed|v|vi|user)\/))([^\?&\"'>]+)/", $src, $matches);
                return isset($matches[1])
                    ? $matches[1]
                    : null;
            case VideoSourceType::VIMEO:
                preg_match('~^https?://(?:www\.)?vimeo\.com/(?:clip:)?(\d+)~', $src, $match);
                return isset($match[1])
                    ? $match[1]
                    : null;
                break;
        }
        return null;
    }

    public static function test() {
        $src = "http://www.youtube.com/watch?v=tOxVXuMmPXI";
        $id = "tOxVXuMmPXI";
        assert(self::getVideoId($src, self::getVideoSourceType($src)) == $id);

        $src = "http://youtu.be/tOxVXuMmPXI";
        $id = "tOxVXuMmPXI";
        assert(self::getVideoId($src, self::getVideoSourceType($src)) == $id);

        $src = "https://www.youtube.com/watch?v=tOxVXuMmPXI";
        $id = "tOxVXuMmPXI";
        assert(self::getVideoId($src, self::getVideoSourceType($src)) == $id);

        $src = "https://youtu.be/tOxVXuMmPXI";
        $id = "tOxVXuMmPXI";
        assert(self::getVideoId($src, self::getVideoSourceType($src)) == $id);

        $src = "www.youtube.com/watch?v=tOxVXuMmPXI";
        $id = "tOxVXuMmPXI";
        assert(self::getVideoId($src, self::getVideoSourceType($src)) == $id);

        $src = "youtu.be/tOxVXuMmPXI";
        $id = "tOxVXuMmPXI";
        assert(self::getVideoId($src, self::getVideoSourceType($src)) == $id);

        $src = "http://vimeo.com/11518720";
        $id = "11518720";
        assert(self::getVideoId($src, self::getVideoSourceType($src)) == $id);

        $src = "https://vimeo.com/11518720";
        $id = "11518720";
        assert(self::getVideoId($src, self::getVideoSourceType($src)) == $id);

        $src = "http://www.youtube.com/watch?v=9hiYqDhrfSQ";
        $id = "9hiYqDhrfSQ";
        assert(self::getVideoId($src, self::getVideoSourceType($src)) == $id);
    }

}