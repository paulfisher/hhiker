<?php

namespace App\Repositories;

use Illuminate\Support\Facades\Session;

class VkontakteService {

    public $clientId = "5053760";
    public $clientSecret = "Ds4TUD3CILUsgErvx0yB";

    private $accessTokenUrl = "https://oauth.vk.com/access_token?client_id={clientId}&client_secret={clientSecret}&redirect_uri={redirectUri}&code={code}";
    private $accessToken;

    private $apiBaseUrl = "https://api.vk.com/method/{methodName}?access_token={accessToken}&{params}";
    const VK_ACCESS_DATA = 'vk_access_data';


    public function initAccessToken($code, $redirectUri) {
        if (Session::has(self::VK_ACCESS_DATA)) {
            //TODO: check expires in
            $accessTokenData = Session::get(self::VK_ACCESS_DATA);
        } else {
            $url = strtr($this->accessTokenUrl, [
                '{code}' => $code,
                '{clientId}' => $this->clientId,
                '{clientSecret}' => $this->clientSecret,
                '{redirectUri}' => $redirectUri
            ]);
            $accessTokenData = json_decode(file_get_contents($url), true);
            Session::put(self::VK_ACCESS_DATA, $accessTokenData);
        }

        $this->accessToken = isset($accessTokenData['access_token']) ? $accessTokenData['access_token'] : null;

        return $accessTokenData;
    }

    public function getUser($userId) {
        return $this->callApi("users.get", ["user_id=" . $userId, "fields=photo_400_orig,about,photo_max_orig"]);
    }

    private function callApi($methodName, $params) {
        $url = $this->apiBaseUrl;
        $url = strtr($url, [
            '{accessToken}' => $this->accessToken,
            '{methodName}' => $methodName,
            '{params}' => implode('&', $params)
        ]);
        return json_decode(file_get_contents($url), true);
    }

}