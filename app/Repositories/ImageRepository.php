<?php

namespace App\Repositories;

use Intervention\Image\Facades\Image;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class ImageRepository {

    public function storeImage(UploadedFile $image, $fitWidth = 1200) {

        $basePath = public_path();
        $imageDir = '/images';
        $baseName = substr(md5(time()), 0, 5) . '-' . pathinfo($image->getClientOriginalName(), PATHINFO_BASENAME);

        $originalFileName = $baseName . '.' . $image->getClientOriginalExtension();

        $image->move($basePath . $imageDir, $originalFileName);

        $originalPath = $imageDir. '/' . $originalFileName;

        /** @var \Intervention\Image\Image $img */
        $img = Image::make($basePath . $originalPath);

        // now you are able to resize the instance
        if ($img->getWidth() > $fitWidth) {
            $img->widen($fitWidth);
        }

        $resizedFileName = $baseName . '_[w_' . $fitWidth . '].' . $image->getClientOriginalExtension();
        $img->save($basePath . '/' . $imageDir  . '/' . $resizedFileName);

        return $imageDir. '/' . $resizedFileName;
    }

}