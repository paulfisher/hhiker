<?php

namespace App\Providers;

use App\Models\Article;
use App\Models\Blog;
use App\Models\BlogChannel;
use App\Models\BlogReadCriteria;
use App\Models\Couch;
use App\Http\Requests\ArticleRequest;
use App\Models\HuntCompanion;
use App\Models\Order;
use App\Models\Question;
use App\Models\Tag;
use App\Models\User;
use App\Models\Video;
use Illuminate\Http\Request;
use Illuminate\Routing\Router;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to the controller routes in your routes file.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function boot(Router $router)
    {
        //

        parent::boot($router);


        $router->bind('article', function($id) {
            if (is_numeric($id)) {
                $byId = Article::find($id);
                if ($byId) {
                    return $byId;
                }
            }
            return Article::where('slug', $id)->firstOrFail();
        });


        $router->bind('video', function($id) {
            return Video::findOrFail($id);
        });

        $router->bind('companion', function($id) {
            return HuntCompanion::findOrFail($id);
        });

        $router->bind('couch', function($id) {
            return Couch::findOrFail($id);
        });

        $router->bind('blog', function($id) {
            return Blog::findOrFail($id);
        });

        $router->bind('blogChannel', function($id) {
            return BlogChannel::findOrFail($id);
        });

        $router->bind('tag', function($id) {
            return Tag::findOrFail($id);
        });

        $router->bind('u', function($userId) {
            return User::findOrFail($userId);
        });

        $router->bind('orderId', function($orderId) {
            return Order::findOrFail($orderId);
        });

        $router->bind('question', function($questionId) {
            return Question::findOrFail($questionId);
        });
    }

    /**
     * Define the routes for the application.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function map(Router $router)
    {
        $router->group(['namespace' => $this->namespace], function ($router) {
            require app_path('Http/routes.php');
        });
    }
}
