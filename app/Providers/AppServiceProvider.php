<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register() {
        $this->app->bind('VkGroup537ChannelImportService', 'App\Services\Channels\VkGroup537ChannelImportService');
        $this->app->bind('YoutubeChannelImportService', 'App\Services\Channels\YoutubeChannelImportService');
        $this->app->bind('InstagramImportService', 'App\Services\Channels\InstagramImportService');
    }
}
