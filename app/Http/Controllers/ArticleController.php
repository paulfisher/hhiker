<?php

namespace App\Http\Controllers;

use App\Http\Requests\ArticleCommentRequest;
use App\Models\Article;
use App\Http\Requests\ArticleRequest;
use App\Models\ArticleComment;
use App\Repositories\ImageRepository;
use App\Services\StringHelper;
use App\Models\Tag;
use App\Models\User;
use Carbon\Carbon;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Filesystem\Filesystem;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use League\Flysystem\Directory;

class ArticleController extends Controller {

    const READ_ANON_UNTIL_AUTH = 5;

    public function __construct() {
        $this->middleware('auth', ['except' => ['index', 'show', 'saveInterests', 'readTag', 'read']]);
        $this->middleware('active', ['except' => ['index', 'show', 'saveInterests', 'readTag', 'read']]);
    }

    public function index() {
        $articles = Article::published()->limit(500)->get();

        return view('article.index', compact('articles'));
    }

    public function show(Article $article, \Illuminate\Http\Request $request) {
        $read = session('read');

        if (is_array($read) && count($read) > self::READ_ANON_UNTIL_AUTH && !Auth::check()) {
            session()->flush();
            return redirect()->guest('auth/login')->with('needAuthMessage', 'Я смотрю, ты уже погрузился в мир путешествий. Отвлеку на мгновение. Пожалуйста, зарегистрируйся. Займет 20 секунд, обещаю :)');
        }
        $isRandom = $request->rand;

        return view('article.show', compact('article', 'isRandom'));
    }

    public function create() {
        return view('article.create', compact('tags'));
    }

    public function store(ArticleRequest $request, ImageRepository $imageRepository) {
        $this->createArticle($request, $imageRepository);

        flash()->overlay('Your article has been created', 'Good job')->important();

        return redirect('article');
    }

    public function edit(Article $article) {
        return view('article.edit', compact('article'));
    }

    public function update(Article $article, ArticleRequest $request, ImageRepository $imageRepository) {
        $article->update($request->all());

        if (Input::hasFile('image') && Input::file('image')->isValid()) {
            $image = Input::file('image');
            $imageSrc = $imageRepository->storeImage($image, 600);
            $article->main_img = $imageSrc;
            $article->save();
        }

        $this->syncTags($article, $request->input('tag_list', []));

        return redirect('article');
    }

    private function syncTags(Article $article, array $tags) {
        $article->tags()->sync($tags);
    }

    private function createArticle(ArticleRequest $request, ImageRepository $imageRepository) {
        $article = Auth::user()
            ->articles()
            ->create($request->all());
        $article->slug = "u" .$article->user->id . "_" . StringHelper::slugIt($article->title);
        $article->save();

        if (Input::hasFile('image') && Input::file('image')->isValid()) {
            $image = Input::file('image');
            $imageSrc = $imageRepository->storeImage($image, 600);
            $article->main_img = $imageSrc;
            $article->save();
        }

        $this->syncTags($article, $request->input('tag_list', []));

        return $article;
    }

    public function saveInterests(\Illuminate\Http\Request $request) {
        if ($request->tags) {
            if (false && Auth::check()) {
                return "todo";
            } else {
                session(['interests' => explode(',', $request->tags)]);
            }
        }
        return redirect()->action('ArticleController@read');
    }

    public function read() {
        $read = session('read', []);
        $interests = session('interests', []);
        if (empty($interests)) {
            return redirect()->action('PageController@explore');
        }

        /** @var Article $article */
        $article = Article::moderated()
            ->orderByRaw("RAND()")
            ->whereHas('tags', function($query) use($interests) {
                $query->whereIn('id', $interests);
            })
            ->whereNotIn('id', $read)
            ->orderBy('channel_post_weight', 'desc')
            ->first();
        if (!$article) {
            session(['read' => []]);
            session(['interests' => []]);
            return redirect('/');
        }
        $read[] = $article->id;
        session(['read' => $read]);

        return redirect()->action('ArticleController@show', ['article' => $article->slugOrId()]);
    }

    public function readTag(Tag $tag) {
        return view('article.tag', compact('tag'));
    }

    public function postCreateComment(Article $article, ArticleCommentRequest $articleCommentRequest) {
        /** @var ArticleComment $comment */
        $comment = $article->comments()->create(
            collect($articleCommentRequest->all())
                ->filter(function($item) {
                    return !!$item;
                })
                ->merge(['user_id' => Auth::user()->id])
                ->all()
        );

        if ($comment->parentComment) {
            Mail::send('article.email.new_comment_answer', compact('comment', 'article'), function ($message) use ($article, $comment) {
                $message->from('noreply@hhiker.ru', 'HHiker');
                $message->to($comment->parentComment->user->email)->subject('Ответ на ваш комментарий на HHiker.ru');
            });
        } else {
            Mail::send('article.email.new_comment', compact('comment', 'article'), function ($message) use ($article) {
                $message->from('noreply@hhiker.ru', 'HHiker');
                $message->to($article->user->email)->subject('Комментарий на вашу запись на HHiker.ru');
            });
        }

        return redirect()->action('ArticleController@show', $article->id);
    }
}
