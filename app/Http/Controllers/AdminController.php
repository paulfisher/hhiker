<?php

namespace App\Http\Controllers;

use App\Http\Requests\TagRequest;
use App\Models\Article;
use App\Models\ArticleComment;
use App\Models\Couch;
use App\Models\HuntCompanion;
use App\Models\HuntCompanionComment;
use App\Models\Question;
use App\Models\QuestionComment;
use App\Models\Tag;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class AdminController extends Controller
{
    public function __construct() {
        $this->middleware('admin');
    }

    public function index() {
        return view('admin.index', [
            'articles' => Article::count(),
            'users' => User::count(),
            'questions' => Question::count(),
            'couches' => Couch::count(),
            'companions' => HuntCompanion::count(),
            'tags' => Tag::count(),
            'articleComments' => ArticleComment::count(),
            'questionComments' => QuestionComment::count(),
            'huntCompanionComments' => HuntCompanionComment::count(),
        ]);
    }

    public function notify() {
        return view('admin.notify', ['message' => "Тест <br/> нотификаций"]);
    }

    public function users() {
        $users = User::all();
        return view('admin.users.index', compact('users'));
    }

    public function tags() {
        $tags = Tag::latest()->limit(500)->get();
        return view('admin.tags.index', compact('tags'));
    }

    public function postCreateTag(TagRequest $tagRequest) {
        $tag = Tag::firstOrCreate(['name' => $tagRequest->name]);
        return redirect()->action('AdminController@tags');
    }

    public function moderateArticles() {
        $article = Article::where('moderated', false)->orderBy('channel_post_weight', 'desc')->first();
        if (!$article) {
            return view('admin.article.no_articles');
        }
        return redirect()->action('AdminController@moderate', $article->id);
    }

    public function moderate(Article $article) {
        $tags = Tag::latest()->limit(30)->get();

        return view('admin.article.moderate', compact('article', 'tags'));
    }

    public function moderated(Article $article) {
        $article->update(['moderated' => true]);
        return redirect()->action('AdminController@moderateArticles');
    }

    public function hideArticle(Article $article) {
        $article->update(['hidden' => true]);
        return redirect()->action('AdminController@moderate', $article->id);
    }

    public function showArticle(Article $article) {
        $article->update(['hidden' => false]);
        return redirect()->action('AdminController@moderate', $article->id);
    }

    public function addTagToArticle(Article $article, Request $request) {
        $idOrName = $request->tagIdOrName;
        $tag = Tag::where('name', $idOrName)->first();
        if (!$tag) {
            if (is_numeric($idOrName))
                $tag = Tag::find($idOrName);

            if (!$tag)
                $tag = Tag::create(['name' => $idOrName]);
        }
        if (!$tag) {
            abort(404);
        }
        if (!$article->tags()->lists('id')->contains($tag->id)) {
            $article->tags()->attach($tag->id);
        }
        return redirect()->action('AdminController@moderate', $article->id);
    }

    public function removeTagFromArticle(Article $article, Tag $tag) {
        $article->tags()->detach($tag->id);
        return redirect()->action('AdminController@moderate', $article->id);
    }

    public function subscribeUser(User $user) {
        $user->update(['is_receive_subscription' => true]);
        return redirect()->back();
    }

    public function unsubscribeUser(User $user) {
        $user->update(['is_receive_subscription' => false]);
        return redirect()->back();
    }

    public function activateUser(User $user) {
        $user->update(['is_active' => true]);
        return redirect()->back();
    }
}
