<?php

namespace App\Http\Controllers;

use App\Http\Requests\CouchRequest;
use App\Models\City;
use App\Models\Couch;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class CouchController extends Controller {

    public function __construct() {
        $this->middleware('auth', ['except' => ['index']]);
        $this->middleware('active', ['except' => ['index', 'show']]);
    }

    public function index() {
        $city = Input::get('city');
        if (!$city) {
            $couches = Couch::latest()->get();
        } else {
            $couches = Couch::where('city_id', $city)->latest()->get();
        }
        return view('couch.index', [
            'couches' => $couches,
            'selectedCity' => City::find($city),
            'cities' => $this->getCitiesWithCouches()
        ]);
    }

    public function create() {
        return view('couch.create');
    }

    public function store(CouchRequest $request) {
        Auth::user()
            ->couches()
            ->create($request->all());

        return redirect('couch');
    }

    public function show(Couch $couch) {
        return view('couch.show', compact('couch'));
    }

    public function edit(Couch $couch) {
        $couch->checkIsMine();
        return view('couch.edit', compact('couch'));
    }

    public function update(Couch $couch, CouchRequest $couchRequest) {
        $couch->checkIsMine()->update($couchRequest->all());

        return redirect('couch');
    }

    private function getCitiesWithCouches() {
        return DB::table('city')
            ->join('couch', 'city.id', '=', 'couch.city_id')
            ->select(['city.id', 'city.name'])
            ->distinct()
            ->get();
    }
}
