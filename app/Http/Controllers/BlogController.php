<?php

namespace App\Http\Controllers;

use App\Models\BlogReadCriteria;
use App\Http\Requests\BlogCrowdfundingRequest;
use App\Http\Requests\BlogReadCriteriaRequest;
use App\Http\Requests\BlogRequest;
use App\Models\Order;
use App\Models\OrderStatus;
use App\Repositories\ImageRepository;
use App\Models\Blog;
use App\Models\User;
use Exception;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;

class BlogController extends Controller {

    public function __construct() {
        $this->middleware('auth', ['except' => ['show', 'index', 'read', 'back']]);
    }

    public function index() {
        return view('blog.index', [
            'blogs' => Blog::latest()->get()
        ]);
    }

    public function create() {
        return view('blog.create');
    }

    public function store(BlogRequest $request, ImageRepository $imageRepository) {
        $blog = Auth::user()
            ->blogs()
            ->create($request->all());

        if (Input::hasFile('img') && Input::file('img')->isValid()) {
            $image = Input::file('img');
            $blog->img = $imageRepository->storeImage($image, 600);
            $blog->save();
        }

        try {
            Mail::raw('Новый блог: ' . $blog->name, function ($message) {
                $message->from('noreply@hhiker.ru', 'HHiker');
                $message->to('pavel.rybako@gmail.com')->subject('Новый блог на HHiker.ru');
            });
        } catch (Exception $e) {}

        return redirect()->action('BlogChannelController@create', $blog->id);
    }

    public function show(Blog $blog) {
        $articles = $blog->articles()->orderBy('published_at', 'desc')->get();
        return view('blog.show', compact('blog', 'articles'));
    }

    public function edit(Blog $blog) {
        $blog->checkIsMine();
        return view('blog.edit', compact('blog'));
    }

    public function update(Blog $blog, BlogRequest $blogRequest, ImageRepository $imageRepository) {
        $blog->checkIsMine()->update($blogRequest->all());

        if (Input::hasFile('img') && Input::file('img')->isValid()) {
            $image = Input::file('img');
            $blog->img = $imageRepository->storeImage($image, 600);
            $blog->save();
        }

        return redirect('blog');
    }

    public function read(Blog $blog, Requests\BlogReadCriteriaRequest $blogReadCriteriaRequest) {
        $blogReadCriteria = $this->loadCriteria($blogReadCriteriaRequest);
        $articles = $this->findArticlesByCriteria($blog, $blogReadCriteria);
        return view('blog.read', compact('blog', 'articles', 'blogReadCriteria'));
    }

    public function saveCrowdfunding(Blog $blog, BlogCrowdfundingRequest $blogCrowdfundingRequest) {
        flash()->overlay('Настройки краудфандинга сохранены.', 'Супер!')->important();

        $blog->update(['postcard_min_pledge' => $blogCrowdfundingRequest->postcard_min_pledge]);
        Auth::user()->update(['yandex_money_account' => $blogCrowdfundingRequest->yandex_money_account]);
        return redirect()->action('BlogController@crowdfunding', $blog->id);
    }

    public function crowdfunding(Blog $blog) {
        return view('blog.crowdfunding', compact('blog'));
    }

    public function toggleCrowdfunding(Blog $blog)
    {
        $blog->update(['is_accept_funding' => !$blog->is_accept_funding]);
        return redirect()->action('BlogController@crowdfunding', $blog->id);
    }

    public function back(Blog $blog) {
        return view('blog.back', compact('blog'));
    }

    public function orderCard()
    {
        /** @var User $me */
        $me = Auth::user();

        //В первую очередь проверяем, есть ли незаконченные заказы для этого блога
        /** @var Order $order */
        $order = $me->buyOrders()
            ->where('blog_id', $blog->id)
            ->where('status', OrderStatus::CREATED)
            ->latest()
            ->first();

        //если нет, дальше ищем просто заказы, чтобы повторно использовать данные в форме
        if (!$order) {
            $order = $me->buyOrders()->latest()->first();
            if ($order) {
                $order->id = 0;//обнуляем, чтобы не исправить существующий заказ
                $order->buyer_message = '';
            }
        }

        //если заказов вообще не было, создаем дефолтный
        if (!$order) {
            $order = Order::createDefault();
            $order->buyer_email = Auth::user()->email;
        }

        return view('orders.back_postcard', compact('blog', 'order'));
    }

    private function findArticlesByCriteria(Blog $blog, BlogReadCriteria $blogReadCriteria) {
        $articles = $blog->articles()->moderated();
        if ($blogReadCriteria->sortBy) {
            $articles->orderBy($blogReadCriteria->sortBy, $blogReadCriteria->order);
        }
        if ($blogReadCriteria->onlyPopular) {
            $articles->where('channel_post_weight', '>', $blog->rating);
        }
        $articles->limit(200);
        return $articles->get();
    }

    private function loadCriteria(BlogReadCriteriaRequest $blogReadCriteriaRequest) {
        /** @var BlogReadCriteria $blogReadCriteria */
        $blogReadCriteria = Session::get('blogReadCriteria', new BlogReadCriteria());
        $blogReadCriteria->update($blogReadCriteriaRequest);
        Session::put('blogReadCriteria', $blogReadCriteria);
        return $blogReadCriteria;
    }
}
