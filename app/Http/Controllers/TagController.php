<?php

namespace App\Http\Controllers;

use App\Models\Tag;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class TagController extends Controller
{
    public function show(Tag $tag) {
        $articles = $tag->articles;

        return view('articles.index', compact('articles'));
    }
}
