<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateOrderRequest;
use App\Models\Order;
use App\Models\OrderStatus;
use App\Models\PaymentStatus;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class OrderController extends Controller {

    public function create(CreateOrderRequest $createOrderRequest) {

        /** @var Order $order */

        if ($createOrderRequest->order_id > 0) {
            $order = Order::findOrFail($createOrderRequest->order_id);
            $order
                ->checkIsMine()
                ->checkPaymentStatus(PaymentStatus::NOT_PAYED)
                ->checkStatus(OrderStatus::CREATED);
            $order->update($createOrderRequest->all());
        } else {
            $order = Order::create(array_merge(
                $createOrderRequest->all(), [
                    'status' => OrderStatus::CREATED,
                    'payment_status' => PaymentStatus::NOT_PAYED,
                    'rub_value' => Order::POSTCARD_PRICE,
                ]
            ));
        }

        return redirect()->action('OrderController@confirm', $order->id);
    }

    public function confirm(Order $order) {
        $order
            ->checkIsMine()
            ->checkPaymentStatus(PaymentStatus::NOT_PAYED)
            ->checkStatus(OrderStatus::CREATED);

        return view('orders.confirm', compact('order'));
    }

    public function show(Order $order, Request $request) {
        $order->checkIsMineOrForMe();
        $isCb = $request->cb;
        $me = Auth::user();
        return view('orders.show', compact('order', 'me', 'isCb'));
    }

    public function index() {
        $me = Auth::user();
        return view('orders.index', compact('me'));
    }

    public function sent(Request $request) {
        /** @var Order $order */
        $order = Order::findOrFail($request->order_id)->checkForMe();
        $order->update([
            'seller_message' => $request->seller_message,
            'status' => OrderStatus::SENT,
            'sent_at' => Carbon::now()
        ]);

        Mail::send('emails.order_sent', compact('order'), function($message) use($order) {
            $message->from('noreply@hhiker.ru', 'HHiker');
            $message->to($order->buyer->email)->subject('Твоя открытка от ' . $order->seller->name . ' уже в пути!');
        });

        Mail::send('emails.order_sent', compact('order'), function($message) use($order) {
            $message->from('noreply@hhiker.ru', 'HHiker');
            $message->to('pavel.rybako@gmail.com')->subject('Твоя открытка от ' . $order->seller->name . ' уже в пути!');
        });

        return redirect()->action('OrderController@show', $order->id);
    }

    public function come(Order $order) {
        $order->checkIsMine();
        $order->update([
            'status' => OrderStatus::SUCCESS_COME,
            'come_at' => Carbon::now()
        ]);

        Mail::send('emails.order_come', compact('order'), function($message) use($order) {
            $message->from('noreply@hhiker.ru', 'HHiker');
            $message->to($order->seller->email)->subject('Открытка для ' . $order->buyer->name . ' дошла до адресата');
        });

        Mail::send('emails.order_come', compact('order'), function($message) use($order) {
            $message->from('noreply@hhiker.ru', 'HHiker');
            $message->to('pavel.rybako@gmail.com')->subject('Открытка для ' . $order->buyer->name . ' дошла до адресата');
        });

        return redirect()->action('OrderController@show', $order->id);
    }
}
