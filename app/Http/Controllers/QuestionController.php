<?php

namespace App\Http\Controllers;

use App\Http\Requests\CompanionRequest;
use App\Http\Requests\QuestionCommentRequest;
use App\Http\Requests\QuestionRequest;
use App\Models\Question;
use App\Models\QuestionComment;
use App\Models\Tag;
use Exception;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class QuestionController extends Controller {

    public function __construct() {
        $this->middleware('auth', ['except' => ['index', 'show']]);
        $this->middleware('active', ['except' => ['index', 'show']]);
    }

    public function index() {
        return view('question.index', [
            'questions' => Question::latest()->get()
        ]);
    }

    public function create() {
        return view('question.create');
    }

    public function postCreate(QuestionRequest $questionRequest) {
        $question = Auth::user()
            ->questions()
            ->create($questionRequest->all());

        $this->syncTags($question, $questionRequest->input('tag_list', []));


        try {
            Mail::raw('Новый вопрос: ' . $question->title . "\n" . $question->body, function ($message) {
                $message->from('noreply@hhiker.ru', 'HHiker');
                $message->to('pavel.rybako@gmail.com')->subject('Новый вопрос на HHiker.ru');
            });
        } catch (Exception $e) {}

        return redirect()->action('QuestionController@show', $question->id);
    }

    public function postEdit(Question $question, QuestionRequest $questionRequest) {
        $question->checkIsMine()->update($questionRequest->all());

        $this->syncTags($question, $questionRequest->input('tag_list', []));

        return redirect()->action('QuestionController@show', $question->id);
    }

    private function syncTags(Question $question, array $tags) {
        $question->tags()->sync($tags);
    }

    public function show(Question $question) {
        return view('question.show', compact('question'));
    }

    public function edit(Question $question) {
        return view('question.edit', compact('question'));
    }

    public function postCreateComment(Question $question, QuestionCommentRequest $questionCommentRequest) {
        /** @var QuestionComment $comment */
        $comment = $question->comments()->create(
            collect($questionCommentRequest->all())
                ->filter(function($item) {
                    return !!$item;
                })
                ->merge(['user_id' => Auth::user()->id])
                ->all()
        );

        if ($comment->parentComment) {
            Mail::send('question.email.new_comment_answer', compact('comment', 'question'), function ($message) use ($question, $comment) {
                $message->from('noreply@hhiker.ru', 'HHiker');
                $message->to($comment->parentComment->user->email)->subject('Ответ на ваш комментарий на HHiker.ru');
            });
        } else {
            Mail::send('question.email.new_comment', compact('comment', 'question'), function ($message) use ($question) {
                $message->from('noreply@hhiker.ru', 'HHiker');
                $message->to($question->user->email)->subject('Ответ на ваш вопрос на HHiker.ru');
            });
        }

        return redirect()->action('QuestionController@show', $question->id);
    }

    public function showTag(Tag $tag) {
        return view('question.tag.show', compact('tag'));
    }

    public function tags() {
        $tags = Tag::has('questions')->get();
        return view('question.tag.index', compact('tags'));
    }

/*
    public function store(CompanionRequest $request) {
        Auth::user()
            ->companions()
            ->create($request->all());

        return redirect('companions');
    }

    public function show(HuntCompanion $huntCompanion) {
        return view('companion.show', compact('huntCompanion'));
    }

    public function edit(HuntCompanion $huntCompanion) {
        $huntCompanion->checkIsMine();
        return view('companion.edit', compact('huntCompanion'));
    }

    public function update(HuntCompanion $huntCompanion, CompanionRequest $companionRequest) {
        $huntCompanion->checkIsMine()->update($companionRequest->all());

        return redirect('companions');
    }*/
}
