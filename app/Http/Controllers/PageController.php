<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Models\Blog;
use App\Models\BlogChannel;
use App\Models\ChannelType;
use App\Http\Requests\CreateOrderRequest;
use App\Models\HuntCompanion;
use App\Models\Image;
use App\Models\ImageSourceType;
use App\Models\Order;
use App\Models\OrderStatus;
use App\Models\PaymentStatus;
use App\Models\Question;
use App\Models\Video;
use App\Services\Channels\InstagramImportService;
use App\Services\StringHelper;
use App\Models\Tag;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use MetzWeb\Instagram\Instagram;

class PageController extends Controller {

    public function about() {
        return view('pages.about');
    }

    public function partners() {
        return view('pages.partners');
    }

    public function contact() {
        return view('pages.contact');
    }

    public function index() {
        $blogs = Blog::orderBy('rating', 'desc')->where('is_on_main', true)->limit(6)->get();
        $questions = Question::latest()->limit(10)->get();
        $companions = HuntCompanion::latest()->limit(10)->get();

        return view('pages.index', compact('blogs', 'questions', 'companions'));
    }

    public function search(Request $request) {
        $query = $request->get('q');
        if ($request->has('hashtag') && $request->get('hashtag') == '1') {
            $query = '#' . $query;
        }
        $blogs = [];
        $articles = [];
        if (mb_strlen($query) > 3) {
            $articles = Article::where('title', 'like', '%' . $query . '%')->orWhere('body', 'like', '%' . $query . '%')->get();
            $blogs = Blog::where('name', 'like', '%' . $query . '%')->orWhere('about', 'like', '%' . $query . '%')->get();
        }
        return view('pages.search', compact('articles', 'blogs', 'query'));
    }

    public function onAuth() {
        $redirectTo = '/';
        $sessionRedirectTo = session()->pull('redirectOnAuth');
        if ($sessionRedirectTo) {
            $redirectTo = $sessionRedirectTo;
        }
        return redirect()->to($redirectTo);
    }

    public function postcards() {
        return view('pages.postcards');
    }

    public function authInstagram(Request $request) {
        $baseUrl = url();
        $instagram = new Instagram(array(
            'apiKey'      => InstagramImportService::API_KEY,
            'apiSecret'   => InstagramImportService::API_SECRET,
            'apiCallback' => $baseUrl . '/instagram?blogChannelId=' . $request->blogChannelId
        ));

        return view('blog_channel.instagram_auth', compact('instagram'));
    }

    public function instagram(Request $request) {
        /*
         * http://hhiker.loc/authInstagram?blogChannelId=16
         */
        /** @var BlogChannel $blogChannel */
        $blogChannel = BlogChannel::findOrFail($request->blogChannelId);
        $blogChannel->blog->checkIsMine();

        $instagram = new Instagram(array(
            'apiKey'      => InstagramImportService::API_KEY,
            'apiSecret'   => InstagramImportService::API_SECRET,
            'apiCallback' => url() . '/instagram?blogChannelId=' . $blogChannel->id
        ));

        $accessTokenData = $instagram->getOAuthToken($request->code);

        $blogChannel->access_data = is_object($accessTokenData) ? $accessTokenData->access_token : $accessTokenData;
        $blogChannel->save();

        return redirect()->action('BlogChannelController@index', $blogChannel->blog_id);
    }

    public function callbackInstagramSubscribe(Request $request) {

        file_put_contents(public_path() . '/new_ins.log', json_encode($request->all()));

        ///сюда подписка приходит
        if ($request->has('hub_mode')) {
            $hubMode = $request->get('hub_mode');
            $hubChallenge = $request->get('hub_challenge');
            $hubVerifyToken = $request->get('hub_verify_token');
            if ($hubVerifyToken == InstagramImportService::MY_CB_VERIFY_TOKEN) {
                return $hubChallenge;
            } else {
                abort(404);
            }
        }

        //здесь логика обработки подписки

        return "ok";

        //curl -F 'client_id=77cf637f42ba422eab73873f23f5d5ef' -F 'client_secret=310851b998a24f7ca72e4f3ef9a85541' -F 'object=user' -F 'aspect=media' -F 'verify_token=BNc06eXwiq' -F 'callback_url=http://hhiker.ru/instagram' https://api.instagram.com/v1/subscriptions/
    }

    public function notifyAdmin(Request $request) {
        $notification = $request->message;
        if (!empty($notification)) {
            Mail::raw("Новое уведомление на hhiker.ru:\n" . StringHelper::br2nl($notification), function ($message) {
                $message->from('noreply@hhiker.ru', 'HHiker');
                $message->to('pavel.rybako@gmail.com')->subject("Системное сообщение на HHiker.ru");
            });
        } else {
            return "empty message";
        }
        return "ok";
    }

    public function testNotify(Request $request) {
        Mail::raw("Test на hhiker.ru", function ($message) {
            $message->from('noreply@hhiker.ru', 'HHiker');
            $message->to('pavel.rybako@gmail.com')->subject("Системное сообщение на HHiker.ru");
        });

        return "ok";
    }


    public function explore() {
        $tags = Tag::where('is_popular', true)->get();
        return view('pages.explore', compact('tags'));
    }

    public function errorInactiveUser() {
        return view('errors.inactive_user', ['user' => Auth::user()]);
    }
}
