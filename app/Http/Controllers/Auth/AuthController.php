<?php

namespace App\Http\Controllers\Auth;

use App\Repositories\VkontakteService;
use App\Models\VkAccount;
use App\Services\StringHelper;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Request;
use App\Models\User;
use Illuminate\Support\Facades\Input;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class AuthController extends Controller
{

    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers;

    protected $redirectTo = '/onAuth';

    public function __construct() {
        $this->middleware('guest', ['except' => ['getLogout', 'activateUser', 'getActivationMail']]);
    }

    public function authenticated() {
        return redirect()->to('onAuth');
    }

    /**
     * @param VkontakteService $vkService
     * @return \Illuminate\View\View
     */
    public function vkAuth(VkontakteService $vkService) {
        $code = Input::get('code');
        $redirectTo = Input::get('redirectOnAuth');
        if ($redirectTo) {
            session()->put('redirectOnAuth', $redirectTo);
        }

        try {
            $accessTokenData = $vkService->initAccessToken($code, Request::url());
        } catch (Exception $e) {
            return view('auth.vk_auth_failed');
        }

        $vkUserId = $accessTokenData['user_id'];

        /** @var VkAccount $vkAccount */
        $vkAccount = VkAccount::firstOrNew(['uid' => $vkUserId]);

        $me = $vkService->getUser($vkUserId);

        $vkAccount->fill($me['response'][0]);
        $user = $vkAccount->user;
        if (!$user) {
            if (!$vkAccount->email) {
                $email = isset($accessTokenData['email'])
                    ? $accessTokenData['email']
                    : (Input::has('email') ? Input::get('email') : null);
                if (!$email) {
                    return view('auth.vk_auth_email');
                }
                $vkAccount->email = $email;
            }

            $user = User::where('email', $vkAccount->email)->first();
            if (!$user) {

                /** @var User $user */
                $user = User::create([
                    'name' => $vkAccount->fullName(),
                    'email' => $vkAccount->email,
                    'password' => md5(time()),
                    'is_active' => true
                ]);

                try {
                    Mail::raw('Регистрация через vk: ' . $user->name . ' ' . $user->email, function ($message) {
                        $message->from('noreply@hhiker.ru', 'HHiker');
                        $message->to('pavel.rybako@gmail.com')->subject('Новый пользователь на HHiker.ru');
                    });
                } catch (Exception $e) {}

                $this->welcomeEmail($user);
            } else {
                if (!$user->is_active) {
                    $user->update(['is_active' => true]);
                    $this->welcomeEmail($user);
                }
            }
            $vkAccount->user_id = $user->id;
        }
        $vkAccount->save();
        if (!$user->img && $vkAccount->photo_400_orig) {
            $user->img = $vkAccount->photo_400_orig;
            $user->save();
        }

        Auth::login($user);

        return view('auth.vk_auth');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:4',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data) {

        /** @var User $user */
        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'is_active' => false,
            'activation_hash' => md5(time() . '-hhiker')
        ]);

        try {
            Mail::raw('Регистрация: ' . $user->name . ' ' . $user->email, function ($message) {
                $message->from('noreply@hhiker.ru', 'HHiker');
                $message->to('pavel.rybako@gmail.com')->subject('Новый пользователь на HHiker.ru');
            });
        } catch (Exception $e) {}

        $this->activationEmail($user);

        return $user;
    }

    public function activateUser($hash) {
        /** @var User $user */
        $user = User::where(['activation_hash' => $hash])->first();
        if (!$user) {
            abort(404);
        }
        if (!$user->is_active) {
            $user->update(['is_active' => true]);
            $this->welcomeEmail($user);
            return view('auth.activated', compact('user'));
        }
        return redirect()->to('/');
    }

    public function getActivationMail() {
        $user = Auth::user();
        $this->activationEmail($user);
        return view('auth.activation_email_sent', compact('user'));
    }

    private function activationEmail(User $user) {
        Mail::send('auth.email.activate', compact('user'), function ($message) use ($user) {
            $message->from('noreply@hhiker.ru', 'HHiker');
            $message->to($user->email)->subject('Добро пожаловать в сообщество HHiker.ru');
        });
    }

    private function welcomeEmail(User $user) {
        Mail::send('auth.email.welcome', compact('user'), function ($message) use ($user) {
            $message->from('noreply@hhiker.ru', 'HHiker');
            $message->to($user->email)->subject('Добро пожаловать в сообщество HHiker.ru');
        });
    }
}
