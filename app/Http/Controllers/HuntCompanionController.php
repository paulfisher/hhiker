<?php

namespace App\Http\Controllers;

use App\Http\Requests\CompanionRequest;
use App\Http\Requests\HuntCompanionCommentRequest;
use App\Models\HuntCompanion;
use App\Models\HuntCompanionComment;
use App\Models\Tag;
use Exception;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class HuntCompanionController extends Controller {

    public function __construct() {
        $this->middleware('auth', ['except' => ['index']]);
        $this->middleware('active', ['except' => ['index', 'show']]);
    }

    public function index() {
        return view('companion.index', [
            'companions' => HuntCompanion::latest()->get()
        ]);
    }

    public function create() {
        return view('companion.create');
    }

    public function store(CompanionRequest $request) {
        $huntCompanion = Auth::user()
            ->companions()
            ->create($request->all());

        $this->syncTags($huntCompanion, $request->input('tag_list', []));

        try {
            Mail::raw('Новый поиск попутчиков: ' . $huntCompanion->id . "\n" . $huntCompanion->about_trip, function ($message) {
                $message->from('noreply@hhiker.ru', 'HHiker');
                $message->to('pavel.rybako@gmail.com')->subject('Новый поиск попутчиков на HHiker.ru');
            });
        } catch (Exception $e) {}

        return redirect('companions');
    }

    public function show(HuntCompanion $huntCompanion) {
        return view('companion.show', compact('huntCompanion'));
    }

    public function edit(HuntCompanion $huntCompanion) {
        $huntCompanion->checkIsMine();
        return view('companion.edit', compact('huntCompanion'));
    }

    public function showTag(Tag $tag) {
        return view('companion.tag.show', compact('tag'));
    }

    public function tags() {
        $tags = Tag::has('companions')->get();
        return view('companion.tag.index', compact('tags'));
    }

    public function update(HuntCompanion $huntCompanion, CompanionRequest $companionRequest) {
        $huntCompanion->checkIsMine()->update($companionRequest->all());

        $this->syncTags($huntCompanion, $companionRequest->input('tag_list', []));

        return redirect('companions');
    }

    public function postCreateComment(HuntCompanion $companion, HuntCompanionCommentRequest $companionCommentRequest) {
        /** @var HuntCompanionComment $comment */
        $comment = $companion->comments()->create(
            collect($companionCommentRequest->all())
                ->filter(function($item) {
                    return !!$item;
                })
                ->merge(['user_id' => Auth::user()->id])
                ->all()
        );

        if ($comment->parentComment) {
            Mail::send('companion.email.new_comment_answer', compact('comment', 'companion'), function ($message) use ($companion, $comment) {
                $message->from('noreply@hhiker.ru', 'HHiker');
                $message->to($comment->parentComment->user->email)->subject('Ответ на ваш комментарий на HHiker.ru');
            });
        } else {
            Mail::send('companion.email.new_comment', compact('comment', 'companion'), function ($message) use ($companion) {
                $message->from('noreply@hhiker.ru', 'HHiker');
                $message->to($companion->user->email)->subject('Новый комментарий на ваш поиск попутчиков на HHiker.ru');
            });
        }

        return redirect()->action('HuntCompanionController@show', $companion->id);
    }

    private function syncTags(HuntCompanion $huntCompanion, array $tags) {
        $huntCompanion->tags()->sync($tags);
    }
}
