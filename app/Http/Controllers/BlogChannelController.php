<?php

namespace App\Http\Controllers;

use App\Models\BlogChannel;
use App\Models\Channel;
use App\Models\ChannelType;
use App\Http\Requests\BlogChannelRequest;
use App\Http\Requests\BlogRequest;
use App\Repositories\ImageRepository;
use App\Models\Blog;
use App\Services\Channels\VkGroup537ChannelImportService;
use App\Services\Channels\YoutubeChannelImportService;
use Exception;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use MetzWeb\Instagram\Instagram;

class BlogChannelController extends Controller {

    public function index(Blog $blog) {
        return view('blog_channel.index', compact('blog'));
    }

    public function create(Blog $blog) {
        return view('blog_channel.create', compact('blog'));
    }

    public function edit(BlogChannel $blogChannel) {
        $blog = $blogChannel->blog;
        return view('blog_channel.edit', compact('blog', 'blogChannel'));
    }

    public function store(Blog $blog, BlogChannelRequest $request) {
        $channelData = array_merge(
            $request->except('_token'),
            ['channel_blog_key' => $this->lookupKey($request)]);

        $blogChannel = $blog->blogChannels()->create($channelData);
        if ($blogChannel->channel->type == ChannelType::INSTAGRAM) {
            return redirect()->action('PageController@authInstagram', ['blogChannelId' => $blogChannel->id]);
        }

        return redirect()->action('BlogChannelController@index', [$blog->id]);
    }

    public function update(BlogChannel $blogChannel, BlogChannelRequest $request) {
        try {
            $channelData = array_merge(
                $request->all(),
                ['channel_blog_key' => $this->lookupKey($request)]);
        } catch(Exception $e) {
            return redirect()->back()->withErrors(['url' => 'not found']);
        }

        $blogChannel->update($channelData);
        if ($blogChannel->channel->type == ChannelType::INSTAGRAM) {
            return redirect()->action('PageController@authInstagram', ['blogChannelId' => $blogChannel->id]);
        }

        return redirect()->action('BlogChannelController@index', $blogChannel->blog->id);
    }

    private function lookupKey(BlogChannelRequest $request) {
        $channel = Channel::findOrFail($request->channel_id);
        switch ($channel->type) {
            case ChannelType::VK_GROUP:
                /** @var VkGroup537ChannelImportService $vkGroup537ChannelImportService */
                $vkGroup537ChannelImportService = App::make('VkGroup537ChannelImportService');
                $key = $vkGroup537ChannelImportService->getOwnerId($request->url);
                return '-' . $key;//Сообщество
            case ChannelType::YOUTUBE:
                /** @var YoutubeChannelImportService $youtubeChannelImportService*/
                $youtubeChannelImportService = App::make('YoutubeChannelImportService');
                $channelIds = $youtubeChannelImportService->getChannelId($request->url);
                return is_array($channelIds) ? implode(' ', $channelIds) : $channelIds;
            case ChannelType::INSTAGRAM:
                return $request->url;
        }
        return null;
    }
}