<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Models\OrderStatus;
use App\Models\PaymentStatus;
use App\Models\YandexTransaction;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Mockery\CountValidator\Exception;

class YandexController extends Controller {
    
    const secret = 'C2QJosd0HWiTyxHASmygcIpL';
    const hashTemplate = '{notification_type}&{operation_id}&{amount}&{currency}&{datetime}&{sender}&{codepro}&{notification_secret}&{label}';

    public function payResult(Request $request) {
        $order = Order::findOrFail($request->o)->checkIsMine();
        return view('orders.ya_result', compact('order'));
    }

    public function cb(Request $request) {

        file_put_contents(public_path() . '/ya.log', json_encode($request->all()));

        //{
        //  "notification_type":"p2p-incoming",
        //  "amount":"714.34",
        //  "datetime":"2015-10-09T03:14:21Z",
        //  "codepro":"false",
        //  "sender":"41001000040",
        //  "sha1_hash":"2f5b539ae61f033c9b09d6148b3cc780fd368f84",
        //  "test_notification":"true",
        //  "operation_label":"",
        //  "operation_id":"test-notification",
        //  "currency":"643",
        //  "label":""}

        $validHashRaw = strtr(self::hashTemplate, [
            '{notification_type}' => $request->notification_type,
            '{operation_id}' => $request->operation_id,
            '{amount}' => $request->amount,
            '{currency}' => $request->currency,
            '{datetime}' => $request->datetime,
            '{sender}' => $request->sender,
            '{codepro}' => $request->codepro,
            '{notification_secret}' => self::secret,
            '{label}' => $request->label,
        ]);
        $validHash = hash("sha1", $validHashRaw); //кодируем в SHA1
        if ($validHash != $request->sha1_hash) {
            throw new \HttpException(403);
        }

        /** @var Order $order */
        $order = Order::findOrFail($request->label);

        /** @var YandexTransaction $yandexTransaction */
        $yandexTransaction = YandexTransaction::create(array_merge(
            $request->all(),
            [
                'order_id' => $order->id,
                'unaccepted' => $request->unaccepted == "true"
            ]
        ));
        file_put_contents(public_path() . '/ya_trans.log', json_encode($yandexTransaction->getAttributes()));

        if ($yandexTransaction->unaccepted) {
            $order->update([
                'payment_status' => PaymentStatus::FAIL,
                'status' => OrderStatus::FAILED
            ]);
        } else {
            $order->update([
                'payed_at' => Carbon::now(),
                'payment_status' => PaymentStatus::SUCCESS_PAYED,
                'status' => OrderStatus::CONFIRMED
            ]);

            Mail::send('emails.new_order', compact('order'), function($message) use($order) {
                $message->from('noreply@hhiker.ru', 'HHiker');
                $message->to($order->seller->email)->subject('Заказ открытки для ' . $order->buyer->name);
            });

            Mail::send('emails.new_order', compact('order'), function($message) use($order) {
                $message->from('noreply@hhiker.ru', 'HHiker');
                $message->to('pavel.rybako@gmail.com')->subject('Заказ открытки для ' . $order->buyer->name);
            });
        }

        echo "ok";
    }
}
