<?php

namespace App\Http\Controllers;

use App\Repositories\ImageRepository;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Input;

class ImageController extends Controller {

    public function getUpload(Request $request) {
        $callbackId = $request->get('CKEditorFuncNum', 1);
        return view('image.upload', compact('callbackId'));
    }

    public function postUpload(Request $request, ImageRepository $imageRepository) {
        if (Input::file('image')->isValid()) {
            $imageSrc = $imageRepository->storeImage(Input::file('image'));

            $callbackId = $request->get('callbackId');

            return view('image.uploaded', compact('imageSrc', 'callbackId'));
        }

        return view('image.upload', ['callbackId' => 1]);
    }


}
