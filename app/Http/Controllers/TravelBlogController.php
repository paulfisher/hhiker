<?php

namespace App\Http\Controllers;

use App\Http\Requests\TravelBlogRequest;
use App\Repositories\ImageRepository;
use App\Models\TravelBlog;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

class TravelBlogController extends Controller {

    public function index() {
        return view('blogs.index', [
            'blogs' => TravelBlog::latest()->get()
        ]);
    }

    public function create() {
        return view('blogs.create');
    }

    public function store(TravelBlogRequest $request, ImageRepository $imageRepository) {
        $blog = Auth::user()
            ->travelBlogs()
            ->create($request->all());

        if (Input::hasFile('img') && Input::file('img')->isValid()) {
            $image = Input::file('img');
            $blog->img = $imageRepository->storeImage($image, 600);
            $blog->save();
        }

        return redirect('blogs');
    }

    public function show(TravelBlog $travelBlog) {
        return view('blogs.show', compact('travelBlog'));
    }

    public function edit(TravelBlog $travelBlog) {
        $travelBlog->checkIsMine();
        return view('blogs.edit', compact('travelBlog'));
    }

    public function update(TravelBlog $travelBlog, TravelBlogRequest $travelBlogRequest, ImageRepository $imageRepository) {
        $travelBlog->checkIsMine()->update($travelBlogRequest->all());

        if (Input::hasFile('img') && Input::file('img')->isValid()) {
            $image = Input::file('img');
            $travelBlog->img = $imageRepository->storeImage($image, 600);
            $travelBlog->save();
        }

        return redirect('blogs');
    }
}
