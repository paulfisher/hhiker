<?php

namespace App\Http\Controllers;

use App\Models\City;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Input;

class CityController extends Controller {

    const POPULAR_CITIES_CACHE_KEY = 'popular_cities';
    const CITY_CACHE_KEY = 'city_';

    public function search() {
        $requestName = Input::get('r');
        if (!$requestName) {
            return 400;
        }

        $name = mb_strtolower($requestName);
        $cached = Cache::get(self::CITY_CACHE_KEY . $name);
        if ($cached) {
            return $cached;
        }

        $cities = City::where('name', 'LIKE', $name . '%')->get(['id', 'name'])->toArray();
        $json = response()->json($cities);
        Cache::put(self::CITY_CACHE_KEY . $name, $json, Carbon::now()->addHours(2));

        return $json;
    }

    public function popular() {
        $popularCitiesJson = Cache::get(self::POPULAR_CITIES_CACHE_KEY);

        if (!$popularCitiesJson) {
            $cities = City::where('important', true)->get(['id', 'name'])->toArray();
            $popularCitiesJson = response()->json($cities);
            Cache::put(self::POPULAR_CITIES_CACHE_KEY, $popularCitiesJson, Carbon::now()->addDays(2));
        }

        return $popularCitiesJson;
    }
}
