<?php

namespace App\Http\Composers;

use App\Models\Article;
use Illuminate\Contracts\View\View;

class NavigationComposer {

//    public function __construct(ArticlesRepository ) {
//
//    }

    public function compose(View $view) {
        $view->with('latest', Article::latest()->published()->first());
        //$view->with('latest', Article::latest()->first());
    }

}