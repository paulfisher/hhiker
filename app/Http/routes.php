<?php

Route::get('/', 'PageController@index');
Route::get('read', 'ArticleController@read');
Route::get('saveInterests', 'ArticleController@saveInterests');

Route::get('explore', 'PageController@explore');
Route::get('onAuth', 'PageController@onAuth');
Route::get('error/inactive', 'PageController@errorInactiveUser');

Route::get('instagram', 'PageController@instagram');
Route::get('authInstagram', 'PageController@authInstagram');
Route::post('newInstagram', 'PageController@callbackInstagramSubscribe');
Route::get('newInstagram', 'PageController@callbackInstagramSubscribe');

Route::get('postcards', 'PageController@postcards');
Route::get('about', 'PageController@about');
Route::get('partners', 'PageController@partners');
Route::get('search', 'PageController@search');

Route::post('order/create', 'OrderController@create');
Route::post('order/sent', 'OrderController@sent');
Route::get('order/{orderId}/come', 'OrderController@come');
Route::get('order/{orderId}/confirm', 'OrderController@confirm');
Route::get('order/{orderId}/view', 'OrderController@show');
Route::get('orders', 'OrderController@index');

Route::post('ya/cb', 'YandexController@cb');
Route::get('ya/payResult', 'YandexController@payResult');

Route::post('article/{article}/createComment', 'ArticleController@postCreateComment');
Route::resource('article', 'ArticleController');
Route::get('read/{tag}', 'ArticleController@readTag');


Route::get('u/unsubscribe', 'UserController@unsubscribe');
Route::get('u/subscribe', 'UserController@subscribe');
Route::resource('u', 'UserController');


Route::get('tag/{tag}', 'TagController@show');

Route::controllers([
    'auth' => 'Auth\AuthController',
    'password' => 'Auth\PasswordController',
]);

Route::get('image/upload', 'ImageController@getUpload');
Route::post('image/upload', 'ImageController@postUpload');


Route::get('activate/mail', 'Auth\AuthController@getActivationMail');
Route::get('activate/{hash}', 'Auth\AuthController@activateUser');
Route::get('vkAuth', 'Auth\AuthController@vkAuth');
Route::post('vkAuth', 'Auth\AuthController@vkAuth');

Route::get('companions', 'HuntCompanionController@index');
Route::get('companions/tag/{tag}', 'HuntCompanionController@showTag');
Route::get('companions/tags', 'HuntCompanionController@tags');
Route::post('companion/{companion}/createComment', 'HuntCompanionController@postCreateComment');
Route::resource('companion', 'HuntCompanionController');

Route::resource('blog', 'BlogController');

Route::get('blog/{blog}/edit/crowdfunding', 'BlogController@crowdfunding');
Route::post('blog/{blog}/edit/crowdfunding', 'BlogController@saveCrowdfunding');
Route::get('blog/{blog}/edit/crowdfunding/toggle', 'BlogController@toggleCrowdfunding');

Route::get('blog/{blog}/read', 'BlogController@read');
Route::get('blog/{blog}/back', 'BlogController@back');


Route::get('blog/{blog}/channel/add', 'BlogChannelController@create');
Route::post('blog/{blog}/channel/add', 'BlogChannelController@store');
Route::get('blog/{blog}/channel/index', 'BlogChannelController@index');
Route::get('blogChannel/{blogChannel}/edit', 'BlogChannelController@edit');
Route::patch('blogChannel/{blogChannel}/update', 'BlogChannelController@update');


Route::resource('couch', 'CouchController');

Route::get('/city/search', 'CityController@search');
Route::get('/city/popular', 'CityController@popular');

Route::get('/admin', 'AdminController@index');
Route::get('/admin/users', 'AdminController@users');
Route::get('/admin/articles/moderate', 'AdminController@moderateArticles');
Route::get('/admin/articles/{article}/hide', 'AdminController@hideArticle');
Route::get('/admin/articles/{article}/show', 'AdminController@showArticle');
Route::get('/admin/articles/{article}/moderate', 'AdminController@moderate');
Route::get('/admin/articles/{article}/moderated', 'AdminController@moderated');
Route::get('/admin/article/{article}/addTag/{tagIdOrName}', 'AdminController@addTagToArticle');
Route::get('/admin/article/{article}/removeTag/{tag}', 'AdminController@removeTagFromArticle');
Route::get('/admin/notify', 'AdminController@notify');
Route::get('/admin/tags', 'AdminController@tags');
Route::post('/admin/createTag', 'AdminController@postCreateTag');
Route::get('/admin/user/{u}/subscribe', 'AdminController@subscribeUser');
Route::get('/admin/user/{u}/unsubscribe', 'AdminController@unsubscribeUser');
Route::get('/admin/user/{u}/activate', 'AdminController@activateUser');


Route::group(['prefix' => 'q'], function () {
    Route::get ('/', 'QuestionController@index');
    Route::get ('create', 'QuestionController@create');
    Route::post('create', 'QuestionController@postCreate');

    Route::get ('read/{question}', 'QuestionController@show');
    Route::get ('tag/{tag}', 'QuestionController@showTag');
    Route::get ('tags', 'QuestionController@tags');
    Route::get ('{question}/edit', 'QuestionController@edit');
    Route::post('{question}/createComment', 'QuestionController@postCreateComment');
    Route::post('{question}/edit', 'QuestionController@postEdit');
});

Route::post('/notifyAdmin', 'PageController@notifyAdmin');
Route::get('/testNotify', 'PageController@testNotify');

Route::resource('video', 'VideoController');