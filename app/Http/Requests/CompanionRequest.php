<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CompanionRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'about_me' => 'required|min:8',
            'about_trip' => 'required',
            'contact' => 'required',
        ];
    }
}
