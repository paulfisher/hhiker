<?php

namespace App\Http\Requests;

use App\Models\BlogReadCriteria;
use App\Http\Requests\Request;

class BlogReadCriteriaRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'viewMode' => 'in:'
                . BlogReadCriteria::VIEW_MODE_TABLE
                . ',' .
                BlogReadCriteria::VIEW_MODE_LIST
                . ',' .
                BlogReadCriteria::VIEW_MODE_STRIP,
            'onlyPopular' => 'boolean',
            'sortBy' => 'in:channel_post_weight,published_at,null',
            'order' => 'in:asc,desc,null',
        ];
    }
}
