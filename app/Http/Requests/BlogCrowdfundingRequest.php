<?php

namespace App\Http\Requests;

use App\BlogReadCriteria;
use App\Http\Requests\Request;

class BlogCrowdfundingRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'postcard_min_pledge' => 'required|integer',
            'yandex_money_account' => 'required',
        ];
    }
}
