<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CreateOrderRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'blog_id' => 'required',
            'seller_user_id' => 'required',
            'buyer_user_id' => 'required',
            'buyer_country' => 'required',
            'buyer_region' => 'required',
            'buyer_city' => 'required',
            'buyer_street' => 'required',
            'buyer_building' => 'required',
            'buyer_room' => 'required',
            'buyer_zip_code' => 'required',
            'buyer_email' => 'required',
        ];
    }
}
