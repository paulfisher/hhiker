<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;

class ActivatedUserMiddleware
{

    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($this->auth->guest() || !$this->auth->user()->is_active) {
            if ($request->ajax()) {
                return response('Unauthorized.', 401);
            } else {
                if ($this->auth->user()) {
                    return redirect()->to('error/inactive');

                } else {
                    return redirect()->to('auth/login');
                }
            }
        }

        return $next($request);
    }
}
