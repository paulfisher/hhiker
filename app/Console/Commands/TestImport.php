<?php

namespace App\Console\Commands;

use App\BlogChannel;
use App\Services\Channels\InstagramImportService;
use App\Services\Channels\VkGroup537ChannelImportService;
use App\Services\Channels\YoutubeChannelImportService;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\App;

class TestImport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'channels:test';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        /** @var VkGroup537ChannelImportService $importService */
        $importService = App::make('VkGroup537ChannelImportService');
        $importService->import(BlogChannel::findOrFail(13));
    }
}
