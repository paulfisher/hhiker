<?php

namespace App\Console\Commands;

use App\Models\Blog;
use App\Models\BlogChannel;
use App\Models\Channel;
use App\Services\Channels\ChannelImportService;
use Exception;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class ImportChannelsPosts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'channels:import {--import_type=} {--blog=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import posts from channels.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @param ChannelImportService $channelImportService
     * @return mixed
     */
    public function handle(ChannelImportService $channelImportService)
    {
        try {
            $blogId = intval($this->option('blog'), 10);
            foreach (BlogChannel::all() as $channel) {
                if ($blogId && $channel->blog_id != $blogId) {
                    continue;
                }

                try {
                    /** @var BlogChannel $channel */
                    $channelImportService->update($channel);
                } catch (Exception $e) {
                    try {
                        Mail::raw("Импорт данных из соц сетей завершился с ошибкой. Канал " . $channel->url . "\n\n" . $e->getMessage() . "\n\n" . $e->getTraceAsString(), function ($message) {
                            $message->from('noreply@hhiker.ru', 'HHiker');
                            $message->to('pavel.rybako@gmail.com')->subject('Ошибка импорта на HHiker.ru');
                        });
                    } catch (Exception $e) {}
                }
            }

            foreach (Blog::all() as $blog) {
                $blog->update([
                    'rating' => DB::table('blog_channel')->where('blog_id', $blog->id)->max('avg_weight')
                ]);
            }
        } catch (Exception $e) {
            try {
                Mail::raw("Импорт данных из соц сетей завершился с ошибков " . $e->getMessage() . "\n<br/>" . $e->getTraceAsString(), function ($message) {
                    $message->from('noreply@hhiker.ru', 'HHiker');
                    $message->to('pavel.rybako@gmail.com')->subject('Ошибка импорта на HHiker.ru');
                });
            } catch (Exception $e) {}
        }

        try {
            Mail::raw('Импорт данных из соц сетей завершен', function ($message) {
                $message->from('noreply@hhiker.ru', 'HHiker');
                $message->to('pavel.rybako@gmail.com')->subject('Импорт данных из блогов на HHiker.ru');
            });
        } catch (Exception $e) {}
    }
}
