;(function() {

    var clientId = 5053760;

    function auth(redirectTo) {

        var redirectParam = redirectTo
            ? encodeURIComponent('http://' + document.location.host + '/vkAuth?redirectOnAuth=' + encodeURIComponent(redirectTo))
            : 'http://' + document.location.host + '/vkAuth';
        var authUrl = "https://oauth.vk.com/authorize?client_id=" + clientId
            + "&redirect_uri=" + redirectParam
            + "&scope=friends,email"
            + "&response_type=code"
            + "&v=5.37";
        /*window.open(authUrl, "_blank");
        window.focus();*/
        document.location.href = authUrl;
    }

    window.register = function(redirectTo) {
        if (!redirectTo) {
            auth();
        } else if (redirectTo == 'this') {
            auth(document.location.href);
        } else {
            auth(redirectTo);
        }
    };

})();