@extends('app')

@section('content')


<div class="row">
    <div class="col-md-12">
        <p>
            {{$user->name}}, твой аккаунт не активирован.<br/>
            <b>
                {!! link_to_action('Auth\AuthController@getActivationMail', 'повторить отправку письма с ссылкой на активацию') !!}
            </b>
        </p>
    </div>
</div>

@stop