@extends('app')

@section('title') - {!! $blog->name !!} @endsection

@section('meta_description')Самостоятельные путешествия, Блог {!! $blog->name !!} @endsection
@section('meta_keywords')Блог,путешествия,{!! $blog->name !!} @endsection

@section('content')

    <ol class="breadcrumb">
        <li><a href="{{url('/')}}" title="HHiker.ru">Главная</a></li>
        <li>{!! link_to_action('BlogController@index', 'Блоги') !!}</li>
        <li>{!! link_to_action('BlogController@show', $blog->name, $blog->id) !!}</li>
        <li class="active">Лента</li>
    </ol>


    <div class="row">
        <div class="col-lg-4">
            <a title="Таблицей" href="?viewMode={{\App\Models\BlogReadCriteria::VIEW_MODE_TABLE}}" class="btn btn-default btn-xs @if ($blogReadCriteria->viewMode == \App\Models\BlogReadCriteria::VIEW_MODE_TABLE) active @endif">
                <span class="glyphicon glyphicon-th"></span>
            </a>
            <a title="Одной лента" href="?viewMode={{\App\Models\BlogReadCriteria::VIEW_MODE_STRIP}}" class="btn btn-default btn-xs @if ($blogReadCriteria->viewMode == \App\Models\BlogReadCriteria::VIEW_MODE_STRIP) active @endif">
                <span class="glyphicon glyphicon-th-list"></span>
            </a>
        </div>
        <div class="col-lg-4">
            <a href="?onlyPopular={{!$blogReadCriteria->onlyPopular}}" class="btn btn-default btn-xs @if ($blogReadCriteria->onlyPopular) active @endif">Показать только популярные</a>
        </div>
        <div class="col-lg-4">
            <a href="?sortBy=channel_post_weight&order={{$blogReadCriteria->order == 'desc' ? 'asc' : 'desc'}}" class="btn btn-default btn-xs @if ($blogReadCriteria->sortBy == 'channel_post_weight') active @endif">
                @if ($blogReadCriteria->sortBy == 'channel_post_weight')
                    @if ($blogReadCriteria->order == 'asc')
                        <span class="glyphicon glyphicon-arrow-up"></span>
                    @else
                        <span class="glyphicon glyphicon-arrow-down"></span>
                    @endif
                @endif
                По популярности
            </a>
            <a href="?sortBy=published_at&order={{$blogReadCriteria->order == 'desc' ? 'asc' : 'desc'}}" class="btn btn-default btn-xs @if ($blogReadCriteria->sortBy == 'published_at') active @endif">
                @if ($blogReadCriteria->sortBy == 'published_at')
                    @if ($blogReadCriteria->order == 'asc')
                        <span class="glyphicon glyphicon-arrow-up"></span>
                    @else
                        <span class="glyphicon glyphicon-arrow-down"></span>
                    @endif
                @endif
                Хронологический порядок
            </a>
        </div>
    </div>
    <div class="row" style="margin-top: 16px;">
        <div class="col-md-12">
            @if ($blogReadCriteria->viewMode == \App\Models\BlogReadCriteria::VIEW_MODE_TABLE)
                @include('article.table')
            @elseif ($blogReadCriteria->viewMode == \App\Models\BlogReadCriteria::VIEW_MODE_LIST)
                @include('article.list')
            @elseif ($blogReadCriteria->viewMode == \App\Models\BlogReadCriteria::VIEW_MODE_STRIP)
                @include('article.strip')
            @endif
        </div>
    </div>

    <script>
        window.go = function(url) {
            window.open(url);
        }
    </script>

@stop