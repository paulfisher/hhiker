@extends('app')

@section('title') - {!! $blog->name !!} @endsection

@section('meta_description')Самостоятельные путешествия, Блог {!! $blog->name !!} @endsection
@section('meta_keywords')Блог,путешествия,{!! $blog->name !!} @endsection

@section('content')

    <ol class="breadcrumb">
        <li><a href="{{url('/')}}" title="HHiker.ru">Главная</a></li>
        <li>{!! link_to_action('BlogController@index', 'Блоги') !!}</li>
        <li class="active">{!! $blog->name !!}</li>
    </ol>

    <div class="row">
        <div class="col-md-12">
            <div class="page-header">
                <h4>Поддержать {{$blog->name}}</h4>
                <p>Выбери способ поддержки:</p>
            </div>
        </div>
    </div>



    <div class="row">
        <div class="col-md-12">
            <a href="#" class="btn btn-success btn-lg">1. Поддержать деньгами</a><br/>
            <small>Поддержите путешественника деньгами</small>
        </div>
    </div>
    <hr/>
    <div class="row">
        <div class="col-md-12">
            <a href="#" class="btn btn-success btn-lg">2. Поддержать словом</a><br/>
            <small>Поддержать путешественника сообщением. Он получит его мгновеннно на свой телефон и email.</small>
        </div>
    </div>
    <hr/>
    <div class="row">
        <div class="col-md-12">
            <a href="#" class="btn btn-success btn-lg">3. Поддержать деньгами и получить в подарок открытку</a><br/>
            <small>Поддержите путешественника деньгами и получите открытку, отправленную лично им</small>
        </div>
    </div>
    <hr/>

@stop