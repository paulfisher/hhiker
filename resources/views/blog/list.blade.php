

<div class="row" style="margin-top: 20px;">
    <div class="col-sm-12">
        @foreach ($blogs as $blog)
            <div class="blog-widget">
                <div class="blog-image">
                    <a href="{{url('blog', $blog->id) }}" title="{{$blog->name}}">
                @if ($blog->img)
                    <img src="{!! $blog->img !!}" />
                @else
                    <img src="/images/internal/share-emotions.jpg" />
                @endif
                    </a>
                </div>
                <div class="caption">
                    <span class="pull-left blog-name">
                        {!! link_to_action('BlogController@show', $blog->name, $blog->id) !!}
                        @if ($blog->channels()->exists())
                            @if ($blog->channels()->where('type', \App\Models\ChannelType::VK_GROUP)->exists())
                                <small class="label label-default" title="У блога есть vk-паблик">vk</small>
                            @endif
                            @if ($blog->channels()->where('type', \App\Models\ChannelType::YOUTUBE)->exists())
                                <small class="label label-default" title="У блога есть youtube-канал">youtube</small>
                            @endif
                            @if ($blog->channels()->where('type', \App\Models\ChannelType::INSTAGRAM)->exists())
                                <small class="label label-default" title="У блога есть instagram">instagram</small>
                            @endif
                        @endif
                    </span>
                </div>
                <div class="stats">
                    <span class="pull-right">
                        @include('partials.back_btn', ['blog' => $blog])
                    </span>

                    @if ($blog->rating > 0)
                        <span class="pull-left stat text-muted" title="Как считается рейтинг? секрет :)"><span class="glyphicon glyphicon-star"></span> <span class="value">{{number_format($blog->rating, 0, '.', ' ')}}</span></span>
                    @endif
                    <small class="pull-left stat text-muted" title="подписчиков: {{$blog->subscribers()}}"><span class="glyphicon glyphicon-eye-open"></span> {{number_format($blog->subscribers(), 0, '.', ' ')}}</small>
                    <small class="pull-left stat text-muted" title="публикаций: {{$blog->articles()->count()}}"><span class="glyphicon glyphicon-pencil"></span> {{number_format($blog->articles()->count(), 0, '.', ' ')}}</small>



                </div>
                <div class="description">
                    {!! strip_tags($blog->about) !!}
                </div>
            </div>
        @endforeach
    </div>
</div>
