@extends('app')

@section('content')


    @include('blog.partials.edit_blog_breadcrumbs', ['blog' => $blog])
    <div class="row">
        <div class="col-md-12">
            @include('blog.partials.edit_blog_navs', ['blog' => $blog])
            <div class="well well-lg" style="min-height: 600px;">
                @if ($blog->is_accept_funding)
                    <div>
                        <strong>Ты крут! Твое путешествие получает поддержку от пользователей</strong>
                        <a href="{{action('BlogController@toggleCrowdfunding', $blog->id)}}" class="pull-right text-muted">
                            <small>отключить краудфандинг</small>
                        </a>
                    </div>

                    {!! Form::open(['method' => 'POST', 'action' => ['BlogController@saveCrowdfunding', $blog->id]]) !!}

                    <div class="row" style="margin-top: 20px;">
                        <div class="col-md-6">
                            Твои читатели могут поддержать тебя любой суммой.<br/>Укажи, при какой сумме ты готов отправить читателю открытку:
                        </div>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="number" class="form-control" name="postcard_min_pledge" value="{{$blog->postcard_min_pledge ?: \App\Order::POSTCARD_PRICE}}"/>
                                <span class="input-group-addon">руб.</span>
                            </div>
                        </div>
                    </div>
                    <div class="row" style="margin-top: 20px;">
                        <div class="col-md-6">
                            Деньги будут приходить на твой яндекс кошелек. Укажи номер кошелька
                        </div>
                        <div class="col-md-6">
                            <div class="input-group" style="width: 100%;">
                                <input type="number" class="form-control" name="yandex_money_account" value="{{$blog->user->yandex_money_account}}" placeholder="Номер кошелька Яндекс.Деньги"/>
                            </div>
                        </div>
                    </div>
                    <div class="row" style="margin-top: 20px;">
                        <div class="col-md-2">
                            <button class="btn btn-success" type="submit">
                                Сохранить
                            </button>
                        </div>
                    </div>
                    {!! Form::close() !!}
                @else
                    <div class="row" style="margin-top: 20px;">
                        <div class="col-md-4">
                            <a href="{{action('BlogController@toggleCrowdfunding', $blog->id)}}" class="btn btn-success">
                                включить краудфандинг
                            </a>
                        </div>
                    </div>
                @endif

            </div>
        </div>
    </div>

@stop