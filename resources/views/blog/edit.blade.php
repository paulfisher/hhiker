@extends('app')

@section('content')


    @include('blog.partials.edit_blog_breadcrumbs', ['blog' => $blog])
    <div class="row">
        <div class="col-md-12">
            @include('blog.partials.edit_blog_navs', ['blog' => $blog])

            <div class="well well-lg">
                {!! Form::model($blog, ['method' => 'PATCH', 'files' => true, 'action' => ['BlogController@update', $blog->id]]) !!}
                @include('blog.form', ['submitButtonText' => 'Обновить'])
                {!! Form::close() !!}
            </div>
        </div>
    </div>

@stop