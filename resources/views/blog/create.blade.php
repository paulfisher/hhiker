@extends('app')

@section('content')


    <ol class="breadcrumb">
        <li><a href="{{url('/')}}" title="HHiker.ru">Главная</a></li>
        <li>{!! link_to_action('BlogController@index', 'Блоги') !!}</li>
        <li class="active">Добавить блог</li>
    </ol>

    <div class="page-header">
        <h1>Мой travel-блог</h1>
    </div>

    {!! Form::model($blog = new \App\Models\Blog(), ['url' => 'blog', 'files' => true]) !!}
        @include('blog.form', ['submitButtonText' => 'Сохранить'])
    {!! Form::close() !!}

@stop