<div class="row" style="margin-bottom: 16px;">
    <div class="col-md-3">
        @if ($blog->img)
            <img src="{!! $blog->img !!}" class="pull-left img-responsive"/>
        @endif
    </div>
    <div class="col-md-9">
        <h4>
            {!! link_to_action('BlogController@show', $blog->name, $blog->id) !!}
        </h4>
        <p class="text-muted">
            <span>Автор: {!! link_to_action('UserController@show', $blog->user->name, [$blog->user->id]) !!}</span>
        </p>
        <p class="text-muted">
            <span>Подписчиков: {{number_format($blog->subscribers(), 0, '.', ' ')}}</span>&nbsp;&nbsp;
            <span>Публикаций: {{number_format($blog->articles()->count(), 0, '.', ' ')}}</span>
        </p>
        @if (Auth::check() && Auth::user()->id == $blog->user->id)
            {!! link_to_action('BlogController@edit', 'Редактировать', [$blog->id], ['class' => 'btn btn-default pull-right']) !!}
        @endif

        <span class="pull-left">
            @include('partials.back_btn', ['blog' => $blog])
        </span>
    </div>

</div>