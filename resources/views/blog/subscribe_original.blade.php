@if ($blogChannel->channel->type == \App\Models\ChannelType::VK_GROUP)
    <script type="text/javascript" src="//vk.com/js/api/openapi.js?117"></script>
        <div id="vk_subscribe"></div>
        <script type="text/javascript">
            VK.Widgets.Subscribe("vk_subscribe", {mode: 1}, {{$blogChannel->channel_blog_key}});
        </script>
@elseif ($blogChannel->channel->type == \App\Models\ChannelType::YOUTUBE)

    <script src="https://apis.google.com/js/platform.js"></script>

    <script>
        function onYtEvent(payload) {
            if (payload.eventType == 'subscribe') {
                $.post('/notifyAdmin', {
                    'message' : 'User has subscribed on blogChannelId: {{$blogChannel->id}}',
                    '_token' : "{{csrf_token()}}"
                });
            } else if (payload.eventType == 'unsubscribe') {
                $.post('/notifyAdmin', {
                    'message' : 'User has UNsubscribed on blogChannelId: {{$blogChannel->id}}',
                    '_token' : "{{csrf_token()}}"
                });
            }
            if (window.console) { // for debugging only
                window.console.log('YT event: ', payload);
            }
        }
    </script>

    <div class="g-ytsubscribe" data-channelid="{{$blogChannel->channel_blog_key}}" data-layout="full" data-count="hidden" data-onytevent="onYtEvent"></div>

@elseif ($blogChannel->channel->type == \App\Models\ChannelType::INSTAGRAM)

    <style>.ig-b- { display: inline-block; }
        .ig-b- img { visibility: hidden; }
        .ig-b-:hover { background-position: 0 -60px; } .ig-b-:active { background-position: 0 -120px; }
        .ig-b-v-24 { width: 137px; height: 24px; background: url(//badges.instagram.com/static/images/ig-badge-view-sprite-24.png) no-repeat 0 0; }
        @media only screen and (-webkit-min-device-pixel-ratio: 2), only screen and (min--moz-device-pixel-ratio: 2), only screen and (-o-min-device-pixel-ratio: 2 / 1), only screen and (min-device-pixel-ratio: 2), only screen and (min-resolution: 192dpi), only screen and (min-resolution: 2dppx) {
            .ig-b-v-24 { background-image: url(//badges.instagram.com/static/images/ig-badge-view-sprite-24@2x.png); background-size: 160px 178px; } }</style>
    <a href="http://instagram.com/{{$blogChannel->channel_blog_key}}?ref=badge" class="ig-b- ig-b-v-24"><img src="//badges.instagram.com/static/images/ig-badge-view-24.png" alt="Instagram" /></a>

@else
    <noindex>
        <nofollow>
            {!! link_to($blogChannel->url, $blogChannel->url, ['target' => '_blank', 'rel' => 'nofollow']) !!}
        </nofollow>
    </noindex>
@endif