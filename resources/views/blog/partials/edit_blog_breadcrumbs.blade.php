<ol class="breadcrumb">
    <li><a href="{{url('/')}}" title="HHiker.ru">Главная</a></li>
    <li>{!! link_to_action('BlogController@index', 'Путешествия') !!}</li>
    <li>{!! link_to_action('BlogController@show', $blog->name, $blog->id) !!}</li>
    <li class="active">Редактирование</li>
</ol>