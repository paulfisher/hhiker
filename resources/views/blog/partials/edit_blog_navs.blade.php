<ul class="nav nav-tabs">

    <li role="presentation" @if (Request::is('blog/*/edit')) class="active" @endif>
        {!! link_to_action('BlogController@edit', 'Описание', $blog->id, ['class' => '']) !!}
    </li>
    <li role="presentation" @if (Request::is('blog/*/edit/crowdfunding')) class="active" @endif>
        {!! link_to_action('BlogController@crowdfunding', 'Краудфандинг', $blog->id, ['class' => '']) !!}
    </li>
    <li role="presentation" @if (Request::is('blog/*/channel/index')) class="active" @endif>
        {!! link_to_action('BlogChannelController@index', 'Настройка импорта блога', $blog->id, ['class' => '']) !!}
    </li>

    @if (isset($blogChannel))
        <li role="presentation" class="active">
            <a href="#">
                @if ($blogChannel->id > 0)
                    {{$blogChannel->channel->name}}
                @else
                    Добавить внешний блог о путешествии
                @endif
            </a>
        </li>
    @endif
</ul>