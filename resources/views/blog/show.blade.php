@extends('app')

@section('title') {!! $blog->name !!} @endsection

@section('meta_description') {!! $blog->name !!} @endsection
@section('meta_keywords') {!! $blog->name !!} @endsection

@section('content')

    <ol class="breadcrumb">
        <li><a href="{{url('/')}}" title="HHiker.ru">Главная</a></li>
        <li>{!! link_to_action('BlogController@index', 'Путешественники') !!}</li>
        <li class="active">{!! $blog->name !!}</li>
    </ol>

    <div class="row">
        <div class="col-md-12">
            <div class="well well-lg">
                @include('blog.header_info', ['blog' => $blog])
                <hr/>
                {!! $blog->about !!}
                <hr/>
                @if ($blog->blogChannels()->exists())
                    <h4>Подпишись на {{$blog->name}}</h4>
                    <div class="row" style="margin-top: 24px;">
                        @foreach ($blog->blogChannels as $channel)
                            <div class="col-md-4">
                                @include('blog.subscribe_original', ['blogChannel' => $channel])
                            </div>
                        @endforeach
                    </div>
                @endif
            </div>
        </div>
    </div>


    <script>
        window.go = function(url) {
            window.open(url);
        }
    </script>

@stop