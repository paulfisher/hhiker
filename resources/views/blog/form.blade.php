<style>
    .form-group label {
        color: #3a3f58;
        font-size: 14px;
    }
</style>

<div class="form-group">
    {!! Form::label('img', 'Главное изображение:') !!}
    @if ($blog->img)
        <div>
            <img src="{!! $blog->img !!}" width="200"/>
        </div>
    @endif
    {!! Form::file('img', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group @if ($errors->getBag('default')->has('name')) has-error @endif">
    {!! Form::label('name', 'Название:') !!}
    {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Например "Орел и решка"']) !!}
    @if ($errors->getBag('default')->has('name'))
        <small class="text-danger">Дайти путешествию название</small>
    @endif
</div>

<div class="form-group @if ($errors->getBag('default')->has('about')) has-error @endif">
    {!! Form::label('about', 'О путешествии:') !!}
    {!! Form::textarea('about', null, ['class' => 'form-control', 'id' => 'about', 'placeholder' => 'Куда ездим ит.д.']) !!}
    @if ($errors->getBag('default')->has('about'))
        <small class="text-danger">Расскажите о своем путешествии</small>
    @endif
</div>

<div class="form-group" style="margin-top: 50px;">
    {!! Form::submit($submitButtonText, ['class' => 'btn btn-success']) !!}
</div>

@section('footer')
    <script src="/js/lib/ckeditor/ckeditor.js"></script>
    <script>
        (function() {
            CKEDITOR.replace('about');
        })();
    </script>
@endsection