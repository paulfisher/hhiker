@extends('app')

@section('content')


<div class="row">
    <div class="col-md-12">

        <div class="row">
            <div class="col-md-12">
                <p>
                    <strong>Ваши данные</strong>
                    &rarr;
                    Подтверждение
                </p>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="well well-lg" style="overflow: hidden;">
                    <div class="row">
                        <div class="col-md-12">
                            @include('orders.back_postcard_header', ['blog' => $blog])
                        </div>
                    </div>
                <hr/>
                    <div class="row">
                        <div class="col-md-6 col-md-push-3">
                            <p>Заполните свои данные, чтобы открытка дошла по адресу</p>
                            @include('orders.create', ['blog' => $blog, 'order' => $order])
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

@stop