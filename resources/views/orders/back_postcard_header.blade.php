@if ($blog->img)
    <img src="{!! $blog->img !!}" class="pull-left img-responsive" style="max-width: 140px;margin-right: 12px;"/>
@endif
Для поддержки проекта {!! link_to_action('BlogController@show', $blog->name, $blog->id, ['target' => '_blank']) !!} закажите отправку вам открытки.
{!! link_to_action('UserController@show', $blog->user->name, $blog->user->id, ['target' => '_blank']) !!} отправит вам ее лично.
Вы получите все уведомления о движении открытки на ваш email.
Стоимость заказа - <strong style="text-decoration: underline;" class="text-muted" data-placement="bottom" data-toggle="tooltip" title="На эти деньги {{$blog->user->name}} сможет купить себе еды и порадовать вас очередным отчетом о путешествии.">{{\App\Order::POSTCARD_PRICE}} рублей</strong>.
Оплата происходит с помощью сервиса Яндекс.Деньги