


<table class="table table-striped table-hover">
    <thead>
        <tr>
            <th>
                #
            </th>
            <th>
                Проект
            </th>
            <th>
                Статус
            </th>
            <th>

            </th>
        </tr>
    </thead>
    @foreach ($orders as $order)
        <tr>
            <td>
                {{$order->id}}
            </td>
            <td>
                {{$order->blog->name}}
            </td>
            <td>
                {{$order->statusName()}}
            </td>
            <td>
                {!! link_to_action('OrderController@show', 'детали', $order->id) !!}
            </td>
        </tr>

    @endforeach
</table>