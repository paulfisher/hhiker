
    {!! Form::model($order, ['url' => 'order/create', 'class' => 'order-form']) !!}
    @include('orders.form', ['submitButtonText' => 'Перейти к подтверждению'])
    {!! Form::close() !!}