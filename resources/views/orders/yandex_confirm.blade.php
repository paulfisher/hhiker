<form method="POST" action="https://money.yandex.ru/quickpay/confirm.xml" class="form-horizontal payment-form">
    <input type="hidden" name="receiver" value="{{\App\Models\Order::YA_MONEY_ACC}}">
    <input type="hidden" name="formcomment" value="{{$order->seller->name}}">
    <input type="hidden" name="short-dest" value="Оплата открытки от путешественника {{$order->seller->name}}">
    <input type="hidden" name="label" value="{{$order->id}}">
    <input type="hidden" name="quickpay-form" value="shop">
    <input type="hidden" name="targets" value="Поддержка travel-проекта {{$order->blog->name}}">
    <input type="hidden" name="sum" value="{{$order->rub_value}}" data-type="number" >
    <input type="hidden" name="comment" value="Поддержка travel-проекта {{$order->blog->name}}." >{{-- - это сообщение получателю, можно благодарить--}}
    <input type="hidden" name="need-fio" value="false">
    <input type="hidden" name="need-email" value="false" >
    <input type="hidden" name="need-phone" value="false">
    <input type="hidden" name="need-address" value="false">
    <input type="hidden" name="successURL" value="{{action('YandexController@payResult', ['o' => $order->id])}}"/>

    <div class="radio">
        <label>
            <input type="radio" name="paymentType" value="PC" required="required"/>
            Яндекс.Деньгами
        </label>
    </div>

    <div class="radio">
        <label>
            <input type="radio" name="paymentType" value="AC" required="required"/>
            Банковской картой
        </label>
    </div>

    <div class="form-group" style="margin-top: 12px;">
        <div class="col-sm-12">
            <input type="submit" name="submit-button" value="Оплатить" class="btn btn-success" style="margin-right: 20px;"/>
            {!! link_to_action('BlogController@back', 'Вернуться, чтобы изменить данные', ['blog' => $order->blog_id]) !!}
        </div>
    </div>
    <div class="form-group" style="margin-top: 24px;">
        <div class="col-sm-12 text-muted">
            <small>
                Платеж происходит с помощью сервиса <img src="/images/internal/logo-yandex-money.png" style="height: 20px;" alt="Яндекс.Деньги"/>
            </small>
        </div>
    </div>
</form>