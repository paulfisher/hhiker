@extends('app')


@section('content')

    <style>
        .order-info-row {
            margin-top: 14px;
        }
    </style>

    <ol class="breadcrumb">
        <li><a href="{{url('/')}}" title="HHiker.ru">Главная</a></li>
        <li>{!! link_to_action('UserController@show', $me->name, $me->id) !!}</li>
        <li>{!! link_to_action('OrderController@index', 'Заказы') !!}</li>
        <li class="active">Открытка от  {{$order->blog->name}}</li>
    </ol>

    <div class="row">
        <div class="col-md-12">

            @if ($isCb and $order->payment_status != \App\Models\PaymentStatus::SUCCESS_PAYED)
                <div class="alert alert-success" role="alert" style="font-size: 15px;">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <p>
                        <strong>
                            Оплата прошла успешно.

                        </strong>
                    </p>
                    <br/>
                    Поздравляю, вы сделали этот мир чуточку лучше! {{$order->seller->name}} отправит вам открытку при первой возможности и вы получите уведомление на свой email.
                    <br/><br/>
                    Спасибо! <span class="glyphicon glyphicon-heart"></span>
                </div>
            @endif
        </div>
    </div>




    <div class="row">
        <div class="col-md-12">
            <p>Заказ открытки от {{$order->blog->name}}</p>

            @if ($order->forMe())
                @if ($order->payment_status == \App\Models\PaymentStatus::NOT_PAYED)
                    <span class="label label-info">Еще не оплачено</span>
                @elseif ($order->payment_status == \App\Models\PaymentStatus::PROCESSING)
                    <span class="label label-info">В обработке</span>
                @elseif ($order->payment_status == \App\Models\PaymentStatus::CANCELLED)
                    <span class="label label-default">Отмена</span>
                @elseif ($order->payment_status == \App\Models\PaymentStatus::SUCCESS_PAYED)
                    <span class="label label-success">Успешно оплачено</span>
                @elseif ($order->payment_status == \App\Models\PaymentStatus::FAIL)
                    <span class="label label-warning">Оплата не удалась</span>
                @endif
            @endif

            @if ($order->status == \App\Models\OrderStatus::CREATED)
                <span class="label label-info">Новая</span>
            @elseif ($order->status == \App\Models\OrderStatus::FAILED)
                <span class="label label-warning">Ошибка</span>
            @elseif ($order->status == \App\Models\OrderStatus::SUCCESS_COME)
                <span class="label label-success">Пришла получателю</span>
            @elseif ($order->status == \App\Models\OrderStatus::SENT)
                <span class="label label-primary">Отправлена</span>
            @elseif ($order->status == \App\Models\OrderStatus::CONFIRMED)
                <span class="label label-primary">Оплата подтверждена</span>
            @endif

        </div>
    </div>

    <div class="row order-info-row">
        <div class="col-md-2">Отправитель</div>
        <div class="col-md-10">
            {{$order->seller->name}} {{$order->blog->name}}
        </div>
    </div>

    @if ($order->seller_message)
    <div class="row order-info-row">
        <div class="col-md-2">Комментарий отправителя</div>
        <div class="col-md-10">
            {{$order->seller_message}}
        </div>
    </div>
    @endif

    <div class="row order-info-row">
        <div class="col-md-2">Получатель</div>
        <div class="col-md-10">
            {!! link_to_action('UserController@show', $order->buyer->name, $order->buyer->id, ['target' => '_blank']) !!}
            <br/>
            {{$order->buyer_email}}
            <br/>
            {{$order->buyer_mobile_phone_number}}
        </div>
    </div>

    <div class="row order-info-row">
        <div class="col-md-2">Комментарий получателя</div>
        <div class="col-md-10">
            {{$order->buyer_message}}
        </div>
    </div>

    <div class="row order-info-row">
        <div class="col-md-2">Адрес получателя</div>
        <div class="col-md-10">
            {{$order->address()}}
        </div>
    </div>



    <div class="row order-info-row">
        <div class="col-md-2">Платеж</div>
        <div class="col-md-10">
            {{$order->rub_value}} руб
            @if ($order->payment_status == \App\Models\PaymentStatus::NOT_PAYED && $order->buyer->id == $me->id)
                <p class="text-danger">
                    {!! link_to_action('UserController@show', $order->seller->name, $order->seller->id, ['target' => '_blank']) !!} не сможет отправить вам открытку, пока не будет оплачена посылка.
                </p>
                {!! link_to_action('OrderController@confirm', 'оплатить', $order->id, ['class' => 'btn btn-success btn-sm']) !!}
            @endif
        </div>
    </div>

    <div class="row" style="margin-top: 20px;">
        <div class="col-md-6">
            @if ($order->forMe() and $order->status == \App\Models\OrderStatus::CONFIRMED)
                <a href="javascript: void 0;" onclick="$('#confirm-sent').toggle();" class="btn btn-xs btn-success">Открытка отослана</a>
                <div id="confirm-sent" style="display: none;">
                    <p>
                        Добавьте ответ для {{$order->buyer->name}}
                    </p>
                    {!! Form::open(['url' => 'order/sent', 'class' => 'orm']) !!}
                    @include('orders.form_sent', ['submitButtonText' => 'Открытка отослана', 'order' => $order])
                    {!! Form::close() !!}
                </div>
            @elseif ($order->isMine() and $order->status == \App\Models\OrderStatus::SENT)
                <p class="text-muted">
                    Когда придет открытка, пожалуйста, нажмите кнопку 'Открытка пришла'. Мы должны знать, что полностью удовлетворили вас :)
                </p>
                {!! link_to_action('OrderController@come', 'Открытка пришла', $order->id, ['class' => 'btn btn-success', 'onclick' => 'return confirm("Пометить открытку, как пришедшую?");']) !!}
            @endif
        </div>
    </div>

@endsection