@extends('app')


@section('content')


    <div class="row">
        <div class="col-md-12">
            Происходит обработка платежа. Подождите немного...
        </div>
    </div>
    <script>
        var redirect = '{{action('OrderController@show', ['orderId' => $order->id, 'cb' => 'true'])}}';
        setTimeout(function() {
            document.location.href = redirect;
        }, 4500);
    </script>

@endsection