@extends('app')

@section('content')


    <style>
        .payment-form label {
            font-size: 14px;
        }
    </style>

    <div class="row">
        <div class="col-md-12">

            <div class="row">
                <div class="col-md-12">
                    <p>
                        {!! link_to_action('BlogController@back', 'Ваши данные', ['blog' => $order->blog_id]) !!} &rarr;
                        <strong>Подтверждение</strong>
                    </p>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12" style="overflow: hidden;">
                    <div class="well well-lg">
                        <div class="row">
                            <div class="col-md-12" style="overflow: hidden;">
                                @include('orders.back_postcard_header', ['blog' => $order->blog])
                            </div>
                        </div>

                        <hr/>

                        <div class="row">
                            <div class="col-md-6 col-md-push-3">
                                <p>
                                    <strong class="text-success">Вы очень близки к тому, чтобы поддержать проект.</strong>
                                    <br/>
                                    Выберите способ оплаты
                                </p>
                                @include('orders.yandex_confirm', ['order' => $order])
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop