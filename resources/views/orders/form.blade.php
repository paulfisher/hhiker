<style>
    .order-form .form-group label {
        color: #3a3f58;
        font-size: 14px;
        /*width: 120px;
        text-align: right;*/
    }
    .order-form .form-group input {
        width: 270px;
    }
    .order-form .form-group .text-warning {
        margin-left: 120px;
    }
</style>


{!! Form::hidden('order_id', $order->id) !!}
{!! Form::hidden('blog_id', $blog->id) !!}
{!! Form::hidden('seller_user_id', $blog->user_id) !!}
{!! Form::hidden('buyer_user_id', Auth::user()->id) !!}


<div class="row">
    <div class="col-md-6">
        <div class="form-group @if ($errors->getBag('default')->has('buyer_country')) has-warning @endif">
            {!! Form::label('buyer_country', 'Страна:') !!}
            {!! Form::text('buyer_country', null, ['class' => 'form-control', 'placeholder' => 'Например Казахстан']) !!}
            @if ($errors->getBag('default')->has('buyer_country')) <small class="text-warning">Укажите вашу страну</small> @endif
        </div>

        <div class="form-group @if ($errors->getBag('default')->has('buyer_region')) has-warning @endif">
            {!! Form::label('buyer_region', 'Регион:') !!}
            {!! Form::text('buyer_region', null, ['class' => 'form-control', 'placeholder' => 'Например Московская область']) !!}
            @if ($errors->getBag('default')->has('buyer_region')) <small class="text-warning">Укажите ваш регион</small> @endif
        </div>

        <div class="form-group @if ($errors->getBag('default')->has('buyer_city')) has-warning @endif">
            {!! Form::label('buyer_city', 'Город:') !!}
            {!! Form::text('buyer_city', null, ['class' => 'form-control', 'placeholder' => 'Например Казань']) !!}
            @if ($errors->getBag('default')->has('buyer_city')) <small class="text-warning">Укажите ваш город</small> @endif
        </div>

        <div class="form-group @if ($errors->getBag('default')->has('buyer_street')) has-warning @endif">
            {!! Form::label('buyer_street', 'Улица:') !!}
            {!! Form::text('buyer_street', null, ['class' => 'form-control', 'placeholder' => 'Например Карла Маркса']) !!}
            @if ($errors->getBag('default')->has('buyer_street')) <small class="text-warning">Укажите вашу улицу</small> @endif
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group @if ($errors->getBag('default')->has('buyer_building')) has-warning @endif">
            {!! Form::label('buyer_building', 'Дом №:') !!}
            {!! Form::text('buyer_building', null, ['class' => 'form-control', 'placeholder' => 'Например 108']) !!}
            @if ($errors->getBag('default')->has('buyer_building')) <small class="text-warning">Укажите ваш дом</small> @endif
        </div>

        <div class="form-group @if ($errors->getBag('default')->has('buyer_room')) has-warning @endif">
            {!! Form::label('buyer_room', 'Квартира:') !!}
            {!! Form::text('buyer_room', null, ['class' => 'form-control', 'placeholder' => 'Например 41']) !!}
            @if ($errors->getBag('default')->has('buyer_room')) <small class="text-warning">Укажите вашу квартиру</small> @endif
        </div>

        <div class="form-group @if ($errors->getBag('default')->has('buyer_zip_code')) has-warning @endif">
            {!! Form::label('buyer_zip_code', 'Почтовый индекс:') !!}
            {!! Form::text('buyer_zip_code', null, ['class' => 'form-control', 'placeholder' => 'Например 424000']) !!}
            @if ($errors->getBag('default')->has('buyer_zip_code')) <small class="text-warning">Укажите почтовый индекс</small> @endif
        </div>

        <div class="hidden form-group @if ($errors->getBag('default')->has('buyer_mobile_phone_number')) has-warning @endif">
            {!! Form::label('buyer_mobile_phone_number', 'Номер мобильного для уведомлений:') !!}
            {!! Form::text('buyer_mobile_phone_number', null, ['class' => 'form-control', 'placeholder' => 'Например 89600967896']) !!}
            @if ($errors->getBag('default')->has('buyer_mobile_phone_number')) <small class="text-warning">Укажите ваш номер телефона</small> @endif
        </div>


        <div class="form-group @if ($errors->getBag('default')->has('buyer_email')) has-warning @endif">
            {!! Form::label('buyer_email', 'Email:') !!}
            {!! Form::text('buyer_email', null, ['class' => 'form-control', 'placeholder' => 'Например pavel.rybako@gmail.com']) !!}
            @if ($errors->getBag('default')->has('buyer_email')) <small class="text-warning">Укажите email</small> @endif
        </div>
    </div>
</div>



<div class="row">
    <div class="col-md-12">
        <div class="form-group @if ($errors->getBag('default')->has('buyer_message')) has-warning @endif">
            {!! Form::textarea('buyer_message', null, ['class' => 'form-control', 'placeholder' => 'Добрые слова или напутствие путешественнику. Например "Спасибо вам за ваши приключения. Нам очень интересно следить за ними."']) !!}
            @if ($errors->getBag('default')->has('buyer_message')) <small class="text-warning">Сообщение путешественнику</small> @endif
        </div>

        <div class="form-group" style="margin-top: 50px;">
            {!! Form::submit($submitButtonText, ['class' => 'btn btn-success']) !!}
        </div>
    </div>
</div>
