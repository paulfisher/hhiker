{!! Form::hidden('order_id', $order->id) !!}


<div class="row">
    <div class="col-md-12">
        <div class="form-group @if ($errors->getBag('default')->has('seller_message')) has-warning @endif">
            {!! Form::textarea('seller_message', null, ['class' => 'form-control', 'placeholder' => 'Например "Спасибо за поддержку, я буду и дальше радовать вас своими приключениями"']) !!}
            @if ($errors->getBag('default')->has('seller_message')) <small class="text-warning">Сообщение путешественнику</small> @endif
        </div>

        <div class="form-group" style="margin-top: 50px;">
            {!! Form::submit($submitButtonText, ['class' => 'btn btn-primary']) !!}
        </div>
    </div>
</div>
