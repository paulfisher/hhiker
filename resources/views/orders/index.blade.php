



@extends('app')


@section('content')

    <ol class="breadcrumb">
        <li><a href="{{url('/')}}" title="HHiker.ru">Главная</a></li>
        <li>{!! link_to_action('UserController@show', $me->name, $me->id) !!}</li>
        <li class="active">Заказы</li>
    </ol>

    <div class="row">
        <div class="col-md-6 well">
            <strong>
                Мои заказы
            </strong>
            @include('orders.list', ['orders' => $me->buyOrders()->latest()->get()])
        </div>
        @if ($me->is_sale_postcard)
        <div class="col-md-6 well">
            <strong>Заказы мне</strong>
            @include('orders.list', ['orders' => $me->saleOrders()->latest()->get()])
        </div>
        @endif
    </div>


@endsection