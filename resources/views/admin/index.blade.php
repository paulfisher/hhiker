@extends('admin')

@section('content')


    <div class="row">
        <div class="col-md-12">
            <div class="well">
                <div class="list-group">
                    <div class="list-group-item">
                        Статей: {{$articles}}
                    </div>
                    <div class="list-group-item">
                        Пользователей: {{$users}}
                    </div>
                    <div class="list-group-item">
                        Вопросов: {{$questions}}
                    </div>
                    <div class="list-group-item">
                        Поисков попутчиков: {{$companions}}
                    </div>
                    <div class="list-group-item">
                        Вписок: {{$couches}}
                    </div>
                    <div class="list-group-item">
                        Тэгов: {{$tags}}
                    </div>
                    <div class="list-group-item">
                        Комментариев к записям: {{$articleComments}}
                    </div>
                    <div class="list-group-item">
                        Ответов на вопросы: {{$questionComments}}
                    </div>
                    <div class="list-group-item">
                        Комментариев на поиск попутчиков: {{$huntCompanionComments}}
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection