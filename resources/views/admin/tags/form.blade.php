<div class="form-group @if ($errors->getBag('default')->has('name')) has-error @endif">
    {!! Form::label('name', 'Тэг:') !!}
    {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'тэг', 'required' => 'required']) !!}
    @if ($errors->getBag('default')->has('name'))
        <small class="text-danger">Напишите название тэга</small>
    @endif
</div>

<div class="form-group">
    {!! Form::submit($submitButtonText, ['class' => 'btn btn-success']) !!}
</div>