
@extends('admin')

@section('content')


    {!! Form::model($tag = new \App\Models\Tag(), ['action' => ['AdminController@postCreateTag']]) !!}
        @include('admin.tags.form', ['submitButtonText' => 'Создать'])
    {!! Form::close() !!}

<div class="row">
    <div class="col-md-12">
        @foreach($tags as $tag)
            <div class="label label-default">
                {{$tag->name}}
            </div>&nbsp;
        @endforeach
    </div>
</div>

@endsection