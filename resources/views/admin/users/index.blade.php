
@extends('admin')

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="well">
            <table class="table table-hover">
                @foreach ($users as $user)
                    <tr>
                        <td>
                            {{$user->id}}
                        </td>
                        <td>
                            @if ($user->img)
                                <img src="{{$user->img}}" height="100"/>
                            @endif
                        </td>
                        <td>
                            {{$user->name}}
                        </td>
                        <td>
                            {{$user->email}}<br/>
                            @if ($user->vkAccount)
                                <a target="_blank" href="http://vk.com/id{{$user->vkAccount->uid}}">{{$user->vkAccount->first_name}} {{$user->vkAccount->last_name}}</a>
                            @endif
                        </td>
                        <td>
                            @foreach ($user->blogs as $blog)
                                <a target="_blank" href="/blog/{{$blog->id}}">{{$blog->name}} #{{$blog->id}}</a>
                            @endforeach
                        </td>
                        <td>
                            @if ($user->is_sale_postcard)
                                <span class="label label-success">открытки вкл</span>
                            @endif
                            @if ($user->is_receive_subscription)
                                {!! link_to_action('AdminController@unsubscribeUser', 'отписать от email-рассылки', $user->id, ['class' => 'btn btn-danger btn-sm']) !!}
                            @else
                                {!! link_to_action('AdminController@subscribeUser', 'подписать на email-рассылку', $user->id, ['class' => 'btn btn-success btn-sm']) !!}
                            @endif
                        </td>
                        <td>
                            @if ($user->is_active)
                                <b>активирован</b>
                            @else
                                {!! link_to_action('AdminController@activateUser', 'активировать', $user->id, ['class' => 'btn btn-success btn-sm']) !!}
                            @endif
                        </td>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>
</div>

@endsection