@extends('admin')

@section('content')

    <div class="row">
        <div class="col-lg-6">
            <strong>{!! $article->title !!}</strong>
            <p>
                {!! nl2br($article->body) !!}
            </p>
            @foreach ($article->images as $image)
                <a href="{{$image->photo_lg}}" target="_blank">
                    <img src="{!! $image->photo_md !!}" alt="{{$article->short}}" class="img-responsive" style="margin-bottom: 6px;"/>
                </a>
            @endforeach
            @foreach ($article->videos as $video)
                @include('partials.show_video', ['video' => $video])
            @endforeach
            @if ($article->blogChannel)
                <div class="row">
                    <div class="col-md-12">
                        @include('article.channel_link', ['article' => $article])
                    </div>
                </div>
            @endif
        </div>
        <div class="col-lg-6">
            <h3>
                Управление тэгами:
            </h3>
            {!! link_to_action('AdminController@moderated', 'дальше', $article->id, ['class' => 'btn btn-default btn-lg pull-right']) !!}
            @foreach ($article->tags as $articleTag)
                <span class="label label-success">
                    {{$articleTag->name}}
                </span>
                &nbsp;
                <small>
                    {!! link_to_action('AdminController@removeTagFromArticle', 'отменить', ['article' => $article->id, 'tag' => $articleTag->id]) !!}
                </small>
                <br/>
            @endforeach

            <br/>
            <h4>
                Добавить:
            </h4>
            <p>
                <input type="text" placeholder="по имени" id="tagName" class="form-control" autofocus="autofocus"/>
            </p>
            <div id="tagsToAdd">
                @foreach ($tags as $tag)
                    @if (!$article->tags()->lists('id')->contains($tag->id))
                        {!! link_to_action('AdminController@addTagToArticle', $tag->name, ['article' => $article->id, 'tagIdOrName' => $tag->id], ['class' => 'label label-primary label-lg']) !!}
                    @endif
                @endforeach
            </div>
            <br/>
            @if ($article->hidden)
                {!! link_to_action('AdminController@showArticle', 'показывать', $article->id) !!}
            @else
                {!! link_to_action('AdminController@hideArticle', 'скрывать', $article->id) !!}
            @endif
        </div>
    </div>


    <script>

        (function() {
            var tagNameInput = $('#tagName');

            tagNameInput.bind('keypress',function (event){
                if (event.keyCode === 13) {
                    addTag();
                }
            });

            function addTag() {
                var tagName = $.trim(tagNameInput.val());
                if (tagName.length > 0) {
                    document.location.href = '/admin/article/{{$article->id}}/addTag/' + tagName.toLowerCase();
                }
            }
        })();
    </script>

@endsection