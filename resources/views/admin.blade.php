<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>HHiker.ru @yield('title')</title>
    <link rel="stylesheet" href="/css/bootstrap.css">

    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css" />
    <link rel="stylesheet" href="/css/main.css" />
    <link rel="stylesheet" href="/css/blog.css" />
    <link rel="stylesheet" href="/css/article.css" />
    <link href='http://fonts.googleapis.com/css?family=Open+Sans&subset=latin,cyrillic' rel='stylesheet' type='text/css'>

    <meta name="description" content="@yield('meta_description')">
    <meta name="keywords" content="@yield('meta_keywords')">
    <script src="/js/lib/jquery-2.1.4.min.js"></script>
    <script src="/js/lib/bootstrap.min.js"></script>

    @yield('head')

</head>
<body>

    <nav class="navbar navbar-static-top @if (Request::is('/')) splash @endif">
        <div class="container">
            <div id="navbar" class="collapse navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li>{!! link_to_action('PageController@index', 'Домой') !!}</li>
                    <li>{!! link_to_action('AdminController@index', 'Админка') !!}</li>
                    <li>{!! link_to_action('AdminController@users', 'Пользователи') !!}</li>
                    <li>{!! link_to_action('AdminController@moderateArticles', 'Модерация статей') !!}</li>
                    <li>{!! link_to_action('AdminController@tags', 'Тэги') !!}</li>
                </ul>
            </div>
        </div>
    </nav>

    <div id="content">
        @include('flash::message')
        <div class="container">
            @yield('content')
        </div>
    </div>

    @yield('footer')

    @include('partials.footer')
    @include('partials.scripts_footer')

</body>
</html>