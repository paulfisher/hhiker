@extends('app')


@section('content')
    <div class="row">
        <div class="col-md-12">
            <h4>
                @if ($user->img)
                    <img src="{!! $user->img !!}" style="max-width:50px; margin-right:10px;" class="img-responsive pull-left"/>
                @endif
                {{$user->name}}
            </h4>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <div class="page-header">
                <h3>Блоги</h3>
            </div>
            @include('blog.list', ['blogs' => $user->blogs])
        </div>
        <div class="col-md-3">
            <div class="page-header">
                <h3>Вопросы</h3>
            </div>
            @include('question.list_sm', ['questions' => $user->questions])
        </div>

        <div class="col-md-4">
            <div class="page-header">
                <h3>Поиск попутчиков</h3>
            </div>
            @include('companion.list_sm', ['companions' => $user->companions])
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <div class="page-header">
                <h3>Вписки</h3>
            </div>
            @include('couch.list_sm', ['couches' => $user->couches])
        </div>
        <div class="col-md-4">
            <div class="page-header">
                <h3>Публикации</h3>
            </div>
            @include('article.list_sm', ['articles' => $user->articles])
            {!! link_to_action('ArticleController@create', '+ добавить', null, ['class' => 'btn btn-success']) !!}
        </div>
    </div>

    @if (Auth::check() and Auth::user()->id == $user->id)
    <div class="row" style="margin-top: 16px;">
        <div class="col-md-12">
            <div class="page-header">
                <h4>Настройки рассылки</h4>
            </div>
            <p>
                Наша email-рассылка ненавязчива и приходит максимум один раз в день. Советую подписаться, будет интересно
            </p>
            @include('user.subscription_settings')
        </div>
    </div>
    @endif

@endsection