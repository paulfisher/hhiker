
@foreach ($users as $user)
<div class="row user-item">
    <div class="col-sm-12">
        {!! link_to_action('UserController@show', $user->name, ['u' => $user->id]) !!}
    </div>
</div>
@endforeach
