<p>
    @if ($user->is_receive_subscription)
        Я получаю свежие новости о портале HHiker.ru :)
        <a href="{{action('UserController@unsubscribe')}}" class="btn btn-xs">отписаться</a>
    @else
        Я не получаю свежие новости о портале HHiker.ru :(
        <a href="{{action('UserController@subscribe')}}" class="btn btn-success btn-xs">Подписаться</a>
    @endif
</p>
