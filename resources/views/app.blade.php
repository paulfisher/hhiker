<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>@yield('title') :: сообщество путешественников hhiker.ru</title>
    <link rel="stylesheet" href="/css/bootstrap.css">

    <link rel="stylesheet" href="/css/lib/select2.min.css" />
    <link rel="stylesheet" href="/css/main.css" />
    <link rel="stylesheet" href="/css/blog.css" />
    <link rel="stylesheet" href="/css/article.css" />
    {{--<link href='http://fonts.googleapis.com/css?family=Open+Sans&subset=latin,cyrillic' rel='stylesheet' type='text/css'>--}}

    <meta name="description" content="@yield('meta_description')">
    <meta name="keywords" content="@yield('meta_keywords')">
    <script src="/js/lib/jquery-2.1.4.min.js"></script>
    <script src="/js/lib/bootstrap.min.js"></script>

    @yield('head')

    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-29199759-7', 'auto');
        ga('send', 'pageview');
    </script>

</head>
<body>

    @include('partials.nav')

    <div id="content">
        @include('flash::message')
        <div class="container">
            @yield('content')
        </div>
    </div>

    @yield('footer')

    @include('partials.footer')
    @include('partials.scripts_footer')

</body>
</html>