@extends('app')

@section('content')

    <ol class="breadcrumb">
        <li><a href="{{url('/')}}" title="HHiker.ru">Главная</a></li>
        <li>{!! link_to_action('CouchController@index', 'Вписки') !!}</li>
        <li>{!! link_to_action('CouchController@show', str_limit($couch->about_couch, 25), [$couch->id]) !!}</li>
        <li class="active">Редактирование</li>
    </ol>

    {!! Form::model($couch, ['method' => 'PATCH', 'action' => ['CouchController@update', $couch->id]]) !!}
        @include('couch.form', ['submitButtonText' => 'Обновить'])
    {!! Form::close() !!}

@stop