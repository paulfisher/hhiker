@extends('app')

@section('title') - вписки, хосты, каучсерф @endsection

@section('meta_description')вписки, хосты, каучсерф @endsection
@section('meta_keywords')вписки, хосты, каучсерф @endsection

@section('content')

    <style>
        .companion-text {
            margin-top: 42px;
        }
    </style>


    <ol class="breadcrumb">
        <li><a href="{{url('/')}}" title="HHiker.ru">Главная</a></li>
        <li>{!! link_to_action('CouchController@index', 'Вписки') !!}</li>
        <li class="active">{{str_limit($couch->about_couch, 25)}}</li>
    </ol>

    <div class="page-header">
        @if ($couch->user->img)
            <img src="{!! $couch->user->img !!}" style="max-width: 180px;"/>
        @endif
            <h3>
                {!! link_to_action('UserController@show', $couch->user->name, [$couch->user_id]) !!} впишет в городе {!! $couch->cityName !!}
                @if ($couch->isMine())
                    {!! link_to_action('CouchController@edit', 'Редактировать', [$couch->id], ['class' => 'btn btn-default pull-right']) !!}
                @endif
            </h3>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="well">


                <div class="row">
                    <div class="col-md-2">
                        <div class="page-header">
                            <h5>Условия вписки</h5>
                        </div>
                    </div>
                    <div class="col-md-10">
                        <p class="companion-text">
                            {!! $couch->about_couch !!}
                        </p>
                    </div>
                </div>
                <hr/>

                <div class="row">
                    <div class="col-md-2">
                        <div class="page-header">
                            <h5>Мои контакты</h5>
                        </div>
                    </div>
                    <div class="col-md-10">
                        <p class="companion-text">
                            @if (Auth::check())
                                <noindex>
                                    <span onclick="this.innerText = $(this).data('contacts');" data-contacts="{!! $couch->contact !!}">показать</span>
                                </noindex>
                            @else
                                Чтобы увидеть контакты, {!! link_to_action('Auth\AuthController@getLogin', 'войдите') !!} в свой аккаунт
                                или {!! link_to_action('Auth\AuthController@getRegister', 'зарегистрируйтесь') !!}
                            @endif
                        </p>
                    </div>
                </div>
                <hr/>

                @if ($couch->couchsurf_account)
                    <div class="row">
                        <div class="col-md-2">
                            <div class="page-header">
                                <h5>CouchSurfing аккаунт</h5>
                            </div>
                        </div>
                        <div class="col-md-10">
                            <p class="companion-text">

                                <noindex>
                                    <a href="javascript: void 0;" onclick="go('{!! $couch->couchsurf_account !!}')" target="_blank">{!! $couch->couchsurf_account !!}</a>
                                </noindex>
                            </p>
                        </div>
                    </div>
                    <script>
                        window.go = function(url) {
                            window.open(url);
                        }
                    </script>
                @endif


            </div>
        </div>
    </div>


@stop