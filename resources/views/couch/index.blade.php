@extends('app')

@section('title') Вписки для путешественников @endsection

@section('meta_description') Поиск попутчиков, вписки @endsection
@section('meta_keywords') Вписки,попутчики,путешествия,автостоп @endsection


@section('content')

    <ol class="breadcrumb">
        <li><a href="{{url('/')}}" title="HHiker.ru">Главная</a></li>
        @if ($selectedCity)
            <li>{!! link_to_action('CouchController@index', 'вписки') !!}</li>
            <li class="active">{{$selectedCity->name}}</li>
        @else
            <li class="active">Вписки</li>
        @endif
    </ol>

    <div class="row">
        <div class="col-md-10">
            <div class="page-header">
                <h3>Вписки</h3>
            </div>
        </div>
        <div class="col-md-2" style="padding-top:40px;">
            @if (Auth::check())
                {!! link_to_action('CouchController@create', '+ Добавить вписку', [], ['class' => 'btn btn-default pull-right']) !!}
            @endif
        </div>
    </div>

    @if (!Auth::check())
        <div class="row">
            <div class="col-md-12">
                <div class="well">
                    Чтобы добавить свое жилье, {!! link_to_action('Auth\AuthController@getLogin', 'войдите') !!} в свой аккаунт или {!! link_to_action('Auth\AuthController@getRegister', 'зарегистрируйтесь') !!}
                    <br/><br/>
                    Быстрый способ:
                    <button class="btn btn-warning" onclick="register('couch/create');return false;">
                        <span class="glyphicon glyphicon-flash"></span>
                        Войти через vk и перейти к добавлению своей вписки
                    </button>
                </div>
            </div>
        </div>
    @endif

    <div class="row">
        <div class="col-md-12">
            @foreach ($cities as $city)
                @if ($selectedCity && $selectedCity->id == $city->id)

                    {!! link_to('/couch?city=' . $city->id, $city->name, ['class' => 'btn btn-xs btn-default disabled']) !!}
                @else
                    {!! link_to('/couch?city=' . $city->id, $city->name, ['class' => 'btn btn-xs btn-default']) !!}
                @endif
            @endforeach
        </div>
    </div>

    <div class="row" style="margin-top:12px;">
        <div class="col-md-10">
            <div class="well">
                @include('couch.list', ['couches' => $couches])
            </div>
        </div>
    </div>

@stop