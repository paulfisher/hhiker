<style>
    .form-group label {
        color: #3a3f58;
        font-size: 14px;
    }
    .tt-menu {
        background: #FFFFFF;
    }
    .tt-dataset {
        padding-left: 4px;
    }
    .tt-suggestion {
        cursor: pointer;
        padding: 4px;
    }
</style>

<div class="form-group @if ($errors->getBag('default')->has('city_id')) has-error @endif">
    {!! Form::label('cityName', 'Город:') !!}
    {!! Form::text('cityName', null, ['class' => 'form-control', 'placeholder' => 'Город', 'id' => 'city']) !!}
    @if ($errors->getBag('default')->has('city_id'))
        <small class="text-danger">Укажите ваш город</small>
    @endif
    {!! Form::hidden('city_id', null, ['id' => 'city_id']) !!}
</div>

<div class="form-group @if ($errors->getBag('default')->has('about_couch')) has-error @endif">
    {!! Form::label('about_couch', 'О вписке:') !!}
    {!! Form::textarea('about_couch', null, ['class' => 'form-control', 'placeholder' => 'Расскажите о жилье, которым хотите поделиться. Перечислите допустимые и нежелательные условия вписки, правила.']) !!}
    @if ($errors->getBag('default')->has('about_couch'))
        <small class="text-danger">Расскажите о своем доме</small>
    @endif
</div>

<div class="form-group @if ($errors->getBag('default')->has('contact')) has-error @endif">
    {!! Form::label('contact', 'Связаться со мной:') !!}
    {!! Form::text('contact', null, ['class' => 'form-control', 'placeholder' => 'Подойдет любой способ связи. Skype, телефон, страничка vk.com, ваш фейсбук ит.д.']) !!}
    <div class="text-muted">Эти данные увидит только потенциальный гость. Поисковыми системами не будет индексироваться.</div>
    @if ($errors->getBag('default')->has('contact'))
        <small class="text-danger">Пожалуйста, укажите свои контактные данные</small>
    @endif
</div>

<div class="form-group @if ($errors->getBag('default')->has('couchsurf_account')) has-error @endif">
    {!! Form::label('couchsurf_account', 'Аккаунт на CouchSurfing:') !!}
    {!! Form::text('couchsurf_account', null, ['class' => 'form-control', 'placeholder' => 'Например https://www.couchsurfing.com/people/pavelrybako']) !!}
    <div class="text-muted">Эти данные увидит только потенциальный гость. Поисковыми системами не будет индексироваться.</div>
    @if ($errors->getBag('default')->has('couchsurf_account'))
        <small class="text-danger">Укажите правильный url, вместе с http://</small>
    @endif
</div>

<div class="form-group" style="margin-top: 50px;">
    {!! Form::submit($submitButtonText, ['class' => 'btn btn-primary']) !!}
</div>

<script src="/js/lib/typeahead/typeahead.bundle.js"></script>
<script>
    $(function() {

        var cityName = $('#city');
        var cityId = $('#city_id');

        var searchCity = new Bloodhound({
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
            prefetch: {
                url: '/city/popular'
            },
            remote: {
                url: '/city/search?r=%QUERY',
                wildcard: '%QUERY'
            }
        });

        cityName.typeahead(null, {
            name: 'cities',
            minLength: 4,
            display: 'name',
            limit: 10,
            highlight: true,
            source: searchCity
        });

        cityName.bind('typeahead:select', function(ev, city) {
            cityId.val(city.id);
        });

    });
</script>