@extends('app')

@section('content')


    <ol class="breadcrumb">
        <li><a href="{{url('/')}}" title="HHiker.ru">Главная</a></li>
        <li>{!! link_to_action('CouchController@index', 'Вписки') !!}</li>
        <li class="active">Впишу путника</li>
    </ol>

    <div class="page-header">
        <h1>Впишу путника</h1>
    </div>

    {!! Form::model($couch = new \App\Models\Couch(), ['url' => 'couch']) !!}
        @include('couch.form', ['submitButtonText' => 'Сохранить'])
    {!! Form::close() !!}

@stop