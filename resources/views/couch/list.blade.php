
@foreach ($couches as $couch)
    <div class="row" style="margin-top: 20px;">
        <div class="col-sm-12">
            @if ($couch->user->img)
                <img src="{!! $couch->user->img !!}" style="max-width:100px; margin-right:10px;" class="pull-left"/>
            @endif
            <b>{!! $couch->cityName !!}</b>
            <p>
                {!! link_to_action('CouchController@show', str_limit($couch->about_couch, 60), [$couch->id]) !!}
            </p>
        </div>
    </div>
    <hr/>
@endforeach
