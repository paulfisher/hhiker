
@foreach ($couches as $couch)
    <div class="row">
        <div class="col-sm-12">
            {!! link_to_action('CouchController@show', str_limit($couch->about_couch, 25), [$couch->id]) !!}
        </div>
    </div>
@endforeach
