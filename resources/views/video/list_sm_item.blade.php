@if ($video->source_type == \App\Models\VideoSourceType::YOUTUBE)
    <iframe width="100%" height="315"
            src="http://www.youtube.com/embed/{!! $video->key !!}?rel=0&showinfo=0"
            frameborder="0"
            allowfullscreen>
    </iframe>
@elseif($video->source_type == \App\Models\VideoSourceType::VK)
    <noindex>
        <a href="{!! $video->article->blogChannel->url !!}?w=wall{!! $video->article->blogChannel->channel_blog_key  !!}_{!! $video->article->channel_post_key !!}" target="_blank" rel="nofollow">
            <img src="{{$video->photo_lg}}" class="img-responsive"/>
        </a>
    </noindex>
@elseif($video->source_type == \App\Models\VideoSourceType::VIMEO)
    <iframe src="https://player.vimeo.com/video/{{$video->key}}?color=909fb0&title=0&byline=0&portrait=0&badge=0"
            width="100%"
            height="315"
            frameborder="0"
            webkitallowfullscreen
            mozallowfullscreen
            allowfullscreen>
    </iframe>
@endif