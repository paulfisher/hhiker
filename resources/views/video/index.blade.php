@extends('app')

@section('title') Видео @endsection

@section('meta_description') Путешествия и приключения @endsection
@section('meta_keywords') Путешествия,автостоп,попутчики,travel-блоги,кругосветка,кругосветное @endsection


@section('content')

    {!! link_to_action('VideoController@create', '+ добавить видео', null, ['class' => 'btn btn-success']) !!}

    <div class="row">
        <div class="col-md-12">
            @include('video.list_sm')
        </div>
    </div>

@endsection