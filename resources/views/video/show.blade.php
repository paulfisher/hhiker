@extends('app')

@section('title') Видео @endsection

@section('meta_description') Путешествия и приключения @endsection
@section('meta_keywords') Путешествия,автостоп,попутчики,travel-блоги,кругосветка,кругосветное @endsection


@section('content')
    <div class="row">
        <div class="col-md-12">
            @include('partials.show_video')
        </div>
    </div>

@endsection