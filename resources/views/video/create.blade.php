@extends('app')

@section('content')
    <h1>Новое видео</h1>



    {!! Form::model($video = new \App\Models\Video, ['url' => 'video', 'files' => true]) !!}
    @include('video.form', ['submitButtonText' => 'Сохранить'])
    {!! Form::close() !!}


    @include('errors.list')

@stop