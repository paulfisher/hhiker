<style>
    .splash {
        /*background-color: #141d27;*/
        background-image: url(/images/internal/splash-road-2.jpg);
        background-repeat: no-repeat;
        background-size: 100% auto;
        background-position: 0 0px;
        -o-background-size: 100% auto;
        -webkit-background-size: 100% auto;
        /*color: #FFFFFF;*/
        text-align: center;
        /*padding: 115px 0;*/
        height: 500px;
    }
    .splash h1 {
        /*color: #FFFFFF;*/
        font-size: 50px;
    }
    .splash #motivation {
        font-size: 20px;
    }
    @media (max-width: 1200px) {
        .splash {
            background-image: none;
            height: auto;
        }
    }

</style>

<nav class="navbar navbar-static-top @if (Request::is('/')) splash @endif">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand navbar-hhiker-logo-container" href="/" title="HHiker.ru путешествия и приключения">
                <img src="/images/internal/logo-hhiker.svg" alt="HHiker logo">
            </a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
            <ul class="nav navbar-nav navbar-right">
                <li>{!! link_to_action('QuestionController@index', 'QA') !!}</li>
                <li>{!! link_to_action('HuntCompanionController@index', 'Попутчики') !!}</li>
                <li>{!! link_to_action('BlogController@index', 'Путешественники') !!}</li>
                <li>{!! link_to_action('CouchController@index', 'Вписки') !!}</li>
                <li>{!! link_to_action('PageController@about', 'О сайте') !!}</li>
                @if (Auth::check())
                    <li>{!! link_to_action('UserController@show', Auth::user()->name, ['u' => Auth::user()->id]) !!}</li>
                    <li>{!! link_to('auth/logout', 'Выйти') !!}</li>
                @else
                    <li>{!! link_to('auth/register', 'Регистрация') !!}</li>
                    <li>{!! link_to('auth/login', 'ВОЙТИ') !!}</li>
                @endif
                {{--<li>
                    <a href="/about" title="еще">
                        <span class="glyphicon glyphicon-menu-hamburger"></span>
                    </a>
                </li>--}}
            </ul>
            <form class="navbar-form navbar-right" role="search" action="{{action('PageController@search')}}">
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Поиск.." required="required" name="q" value="{{isset($query) ? $query : ''}}">
                </div>
            </form>
        </div>


        @if (Request::is('/'))
        <div class="row" style="padding-top: 70px;">
            <div class="col-lg-12">
                <h1>
                    В добрый путь, странник!
                </h1>
                <p id="motivation">
                    Сообщество самостоятельных путешественников
                </p>
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        @if (Auth::check())
                            {!! link_to('about', 'Начать путешествие', ['class' => 'btn btn-default']) !!}
                        @else
                            {!! link_to('auth/login', 'Начать путешествие', ['class' => 'btn btn-default']) !!}
                        @endif
                    </div>
                </div>
            </div>
        </div>
        @endif

    </div>

</nav>