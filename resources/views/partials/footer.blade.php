<script>
    $(function () {
        $("[data-toggle='tooltip']").tooltip();
    });
</script>
<footer class="footer">
    <div class="footer-menu">
        <div class="container">
            <div class="row" style="min-height: 200px;">
                <div class="col-md-2">
                    <a href="/" title="HHiker.ru - путешествия и приключения"><small>HHiker.ru</small></a>
                    <br/>
                    &copy;HHiker, 2015
                </div>
                <div class="col-md-4">
                    <small>
                        {!! link_to('/about', 'О сайте') !!}<br/>
                        {!! link_to_action('HuntCompanionController@index', 'Поиск попутчиков', [], ['title' => 'Поиск попутчиков в путешествие']) !!}<br/>
                        {!! link_to_action('CouchController@index', 'Каучсёрфинг', [], ['title' => 'Поиск жилья и вписок']) !!}<br/>
                        {!! link_to_action('BlogController@index', 'Блоги', [], ['title' => 'Блоги']) !!}<br/>
                        {!! link_to_action('QuestionController@index', 'Вопросы', [], ['title' => 'Вопросы']) !!}<br/>
                        <a href="https://vk.com/pavel.rybakov" target="_blank">Контакты</a>
                    </small>
                </div>

                <div class="col-md-6">
                    <iframe class="pull-right" src="https://smartprogress.do/goal/embed/136903/?user_id=79591" frameborder="0" width="450" height="150"></iframe>
                </div>
            </div>
        </div>
    </div>
</footer>