@if ($blog->is_accept_funding)
    <a href="{{action('BlogController@back', $blog->id)}}" class="btn @if (isset($btnClass)) {{$btnClass}} @else btn-success @endif btn-sm">
        <span class="glyphicon glyphicon-thumbs-up"></span> Поддержать проект
    </a>
@endif