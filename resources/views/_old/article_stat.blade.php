<div class="article-item-statistics">
    <span class="glyphicon glyphicon-heart-empty text-muted" title="Количество лайков: {{$article->channel_post_likes}}"></span>
    <span class="value" title="Количество лайков: {{$article->channel_post_likes}}">{{number_format($article->channel_post_likes, 0, '.', ' ')}}</span>

    <span class="glyphicon glyphicon-pencil text-muted" title="Количество комментариев: {{$article->channel_post_comments}}"></span>
    <span class="value" title="Количество комментариев: {{$article->channel_post_comments}}">{{number_format($article->channel_post_comments, 0, '.', ' ')}}</span>

    @if ($article->blogChannel->channel->type == \App\Models\ChannelType::VK_GROUP)
        <span class="glyphicon glyphicon-share-alt text-muted" title="Количество репостов: {{$article->channel_post_reposts}}"></span>
        <span class="value" title="Количество репостов: {{$article->channel_post_reposts}}">{{number_format($article->channel_post_reposts, 0, '.', ' ')}}</span>
    @endif

    <span class="glyphicon glyphicon-star text-muted" title="Рейтинг HHiker-а: {{$article->channel_post_weight}}"></span>
    <span class="value" title="Рейтинг HHiker-а: {{$article->channel_post_weight}}">{{number_format($article->channel_post_weight, 0, '.', ' ')}}</span>
</div>