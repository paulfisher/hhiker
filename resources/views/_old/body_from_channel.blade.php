@if ($article->geo_lat and $article->geo_long)
    <div class="row">
        <div class="col-md-12">
            @if ($article->geo_name)
                <span class="text-muted">{!! $article->geo_name !!}</span>
            @endif
            <iframe
                    width="100%"
                    height="300"
                    frameborder="0" style="border:0"
                    src="https://www.google.com/maps/embed/v1/place?key=AIzaSyAqxO9VL6DBTCshokFY5wx_IhCnEMhlYAc&q={!! $article->geo_lat !!},{!! $article->geo_long !!}" allowfullscreen>
            </iframe>
        </div>
    </div>
@endif

<div class="row">
    <div class="col-md-12">
        <div class="well well-lg">
            <div class="row">
                <?php $showLeftColumn = ($article->images()->exists() or $article->videos()->exists());?>
                @if ($showLeftColumn)
                        <div class="col-md-6">
                            @foreach ($article->images as $image)
                                <a href="{{$image->photo_lg}}" target="_blank">
                                    <img src="{!! $image->photo_md !!}" alt="{{$article->short}}" class="img-responsive" style="margin-bottom: 6px;"/>
                                </a>
                            @endforeach
                            @foreach ($article->videos as $video)
                                @include('partials.show_video', ['video' => $video])
                            @endforeach

                        </div>
                        <div class="col-md-6">
                @else
                        <div class="col-md-12">
                @endif

                    <article class="article-text" style="margin-bottom: 12px;">
                        <strong>
                            {{$article->title}}
                        </strong>
                        <br/>
                        <small class="text-muted">
                            {{$article->published_at->diffForHumans()}}
                        </small>
                        <br/><br/>
                        {!! nl2br($article->body) !!}
                    </article>

                            <span class="pull-left">
                    @include('article.article_stat', ['article' => $article])
                                </span>
                    <span class="pull-right">
                        @include('partials.back_btn', ['blog' => $article->blog, 'btnClass' => 'btn-danger'])
                    </span>

                </div>
            </div>

            @if ($article->blogChannel)
                <div class="row">
                    <div class="col-md-12">
                        @include('article.channel_link', ['article' => $article])
                    </div>
                </div>
            @endif
            <div class="row">
                <div class="col-md-12">
                    @foreach ($article->tags as $tag)
                        {!! link_to_action('ArticleController@readTag', $tag->name, $tag->id, ['class' => 'label label-warning']) !!}
                    @endforeach
                </div>
            </div>

            <div class="row" style="margin-top: 26px;">
                <div class="col-md-4 articles-table">
                    @if ($article->previous())
                        <p>
                            <small>{!! link_to_action('ArticleController@show', '&larr; '.strip_tags($article->previous()->short), $article->previous()->id) !!}...</small>
                        </p>
                    @endif
                </div>

                <div class="col-md-4 col-md-push-4 articles-table">
                    @if ($article->next())
                        <p class="pull-right">
                            <small>{!! link_to_action('ArticleController@show', strip_tags($article->next()->short) . '... &rarr;', $article->next()->id) !!}</small>
                        </p>
                    @endif
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    @include('partials.share')
                </div>
            </div>
        </div>
    </div>
</div>

    <div class="row">
        <div class="col-md-12">
            @include('article.comment.list', ['comments' => $article->comments])
        </div>
    </div>


    <div class="row">
        <div class="col-md-12">
            <div class="well">
                @if (Auth::check())
                    {!! Form::model($articleComment = new \App\Models\ArticleComment(), ['action' => ['ArticleController@postCreateComment', $article->id]]) !!}
                    @include('article.comment.form', ['submitButtonText' => 'Ответить'])
                    {!! Form::close() !!}
                @else
                    <p>
                        Чтобы Дать комментарий, {!! link_to_action('Auth\AuthController@getLogin', 'войдите') !!} в свой аккаунт или {!! link_to_action('Auth\AuthController@getRegister', 'зарегистрируйтесь') !!}
                        <br/><br/>
                        <button class="btn btn-warning" onclick="register('this');return false;">
                            <span class="glyphicon glyphicon-flash"></span>
                            Войти через vk и оставить комментарий
                        </button>
                    </p>
                @endif
            </div>
        </div>
    </div>
