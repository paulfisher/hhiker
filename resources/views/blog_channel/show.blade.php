@extends('app')

@section('title') - Блог {!! $blog->name !!} @endsection

@section('meta_description')Блог {!! $blog->name !!} @endsection
@section('meta_keywords')Блог,{!! $blog->name !!} @endsection

@section('content')

    <style>
        .companion-text {
            margin-top: 42px;
        }
    </style>


    <ol class="breadcrumb">
        <li><a href="{{url('/')}}" title="HHiker.ru">Главная</a></li>
        <li>{!! link_to_action('BlogController@index', 'Блоги') !!}</li>
        <li class="active">{!! $blog->name !!}</li>
    </ol>

    <div class="page-header">
        @if ($blog->img)
            <img src="{!! $blog->img !!}" style="max-width: 180px;"/>
        @endif
            <h3>
                {!! $blog->name !!}
                @if (Auth::check() && Auth::user()->id == $blog->user->id)
                    {!! link_to_action('BlogController@edit', 'Редактировать', [$blog->id], ['class' => 'btn btn-default pull-right']) !!}
                @endif
            </h3>

    </div>


    <div class="row">
        <div class="col-md-2">
            <div class="page-header">
                <h5>О блоге</h5>
            </div>
        </div>
        <div class="col-md-10">
            <p class="companion-text">
                {!! $blog->about !!}
            </p>
        </div>
    </div>


    <div class="row">
        <div class="col-md-2">
            <div class="page-header">
                <h5>Автор</h5>
            </div>
        </div>
        <div class="col-md-10">
            <p class="companion-text">
                {!! link_to_action('UserController@show', $blog->user->name, [$blog->user->id]) !!}
            </p>
        </div>
    </div>

    <script>
        window.go = function(url) {
            window.open(url);
        }
    </script>

@stop