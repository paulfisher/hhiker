@extends('app')

@section('content')


    @include('blog.partials.edit_blog_breadcrumbs', ['blog' => $blog])
    <div class="row">
        <div class="col-md-12">
            @include('blog.partials.edit_blog_navs', ['blog' => $blog, 'blogChannel' => $blogChannel])

            <div class="well well-lg">

                {!! Form::model($blogChannel, ['method' => 'PATCH', 'action' => ['BlogChannelController@update', $blogChannel->id]]) !!}
                    @include('blog_channel.form', ['submitButtonText' => 'Обновить'])
                {!! Form::close() !!}

            </div>

        </div>
    </div>

@stop