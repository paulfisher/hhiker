
@foreach ($blogs as $blog)
    <div class="row" style="margin-top: 20px;">
        <div class="col-sm-12">
            <div class="row">
                @if ($blog->img)
                    <div class="col-sm-2">
                        <a href="{{url('/blogs', $blog->id) }}" title="{{$blog->name}}">
                            <img src="{!! $blog->img !!}" style="max-width:150px; margin-right:10px;" class="pull-left"/>
                        </a>
                    </div>
                @endif

                <div class="col-sm-10">

                    <b>{!! link_to_action('BlogController@show', $blog->name, [$blog->id]) !!}</b>



                    @if ($blog->isMine())
                        {!! link_to_action('BlogController@edit', 'Редактировать', [$blog->id], ['class' => 'btn btn-default btn-xs pull-right']) !!}
                    @endif
                </div>
            </div>
        </div>
    </div>
@endforeach
