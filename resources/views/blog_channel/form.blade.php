<style>
    .form-group label {
        color: #3a3f58;
        font-size: 14px;
    }
</style>


<div class="form-group @if ($errors->getBag('default')->has('url')) has-error @endif">
    {!! Form::label('url', 'URL источника:') !!}
    {!! Form::text('url', null, ['class' => 'form-control', 'placeholder' => 'http://vk.com/mycooltripblog']) !!}
    @if ($errors->getBag('default')->has('url'))
        <small class="text-danger">Укажите ссылку на блог</small>
    @endif
</div>

<div class="form-group @if ($errors->getBag('default')->has('channel_id')) has-error @endif">
    {!! Form::label('channel_id', 'Тип источника:') !!}
    {!! Form::select('channel_id', App\Models\Channel::lists('name', 'id'), null, ['class' => 'form-control']) !!}
    @if ($errors->getBag('default')->has('channel_id'))
        <small class="text-danger">Укажите канал</small>
    @endif
</div>

<div class="form-group" style="margin-top: 50px;">
    {!! Form::submit($submitButtonText, ['class' => 'btn btn-success']) !!}
</div>

@section('footer')
@endsection