@extends('app')

@section('meta_description') Travel-блоги @endsection
@section('meta_keywords') Путешественники,блоги,travel-блоги @endsection


@section('content')

    @include('blog.partials.edit_blog_breadcrumbs', ['blog' => $blog])
    <div class="row">
        <div class="col-md-12">
            @include('blog.partials.edit_blog_navs', ['blog' => $blog])

            <div class="well well-lg">

                <table class="table table-responsive table-hover">
                    <thead>
                    <tr>
                        <th>Название</th>
                        <th>общее количество записей</th>
                        <th>общее количество подписчиков</th>
                        <th>среднее количество лайков</th>
                        <th>среднее количество комментариев</th>
                        <th>среднее количество репостов</th>
                        <th>рейтинг hhiker-а</th>
                        <th>&nbsp;</th>
                    </tr>


                    </thead>
                    <tbody>
                    @foreach ($blog->blogChannels as $channel)
                        <tr>
                            <td>{{$channel->channel->name}} {{$channel->url}}</td>
                            <td>{{$channel->articles()->count()}}</td>
                            <td>{{$channel->subscribers_count}}</td>
                            <td>{{$channel->avg_likes}}</td>
                            <td>{{$channel->avg_comments}}</td>
                            <td>{{$channel->avg_reposts}}</td>
                            <td>{{$channel->avg_weight}}</td>
                            <td>
                                <a href="{{action('BlogChannelController@edit', $channel->id)}}">редактировать</a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                {!! link_to_action('BlogChannelController@create', 'Добавить', $blog->id, ['class' => 'btn btn-success btn-xs']) !!}

            </div>
        </div>
    </div>
@stop