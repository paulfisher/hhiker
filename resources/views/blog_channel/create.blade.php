@extends('app')

@section('content')




    @include('blog.partials.edit_blog_breadcrumbs', ['blog' => $blog])
    <div class="row">
        <div class="col-md-12">
            @include('blog.partials.edit_blog_navs', ['blog' => $blog, 'blogChannel' => new \App\Models\BlogChannel()])

            <div class="well well-lg">
                {!! Form::model($channel = new \App\Models\Channel()) !!}
                    @include('blog_channel.form', ['submitButtonText' => 'Сохранить'])
                {!! Form::close() !!}
            </div>
        </div>
    </div>




@stop