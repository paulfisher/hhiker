@extends('app')

@section('content')

    <ol class="breadcrumb">
        <li><a href="{{url('/')}}" title="HHiker.ru">Главная</a></li>
        <li>{!! link_to_action('QuestionController@index', 'Вопросы') !!}</li>
        <li>{!! link_to_action('QuestionController@show', $question->title, $question->id) !!}</li>
        <li class="active">Редактирование</li>
    </ol>

    {!! Form::model($question, ['method' => 'POST', 'action' => ['QuestionController@postEdit', $question->id]]) !!}
        @include('question.form', ['submitButtonText' => 'Обновить'])
    {!! Form::close() !!}

@stop