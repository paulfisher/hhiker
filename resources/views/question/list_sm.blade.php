<div class="list-group">
    @foreach ($questions as $question)
        <div class="list-group-item">
            <div class="row">
                <div class="col-md-8">
                    <div class="pull-left">
                        <a href="{{action('UserController@show', $question->user_id)}}">
                            @include('user.avatar_sm', ['user' => $question->user])
                        </a>
                    </div>
                    <span class="question-list-title">
                        {!! link_to_action('QuestionController@show', $question->title, $question->id) !!}
                    </span>
                    @include('question.tags', ['tags' => $question->tags])
                    <p>
                        {!! link_to_action('UserController@show', $question->user->name, $question->user->id) !!}
                        <span class="question-list-time">
                            {{$question->created_at->diffForHumans()}}
                        </span>
                    </p>
                </div>
                <div class="col-md-4">
                    <div class="pull-left">
                        <p class="question-list-stat">
                            ответов: {{$question->comments()->count()}}
                        </p>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
</div>
