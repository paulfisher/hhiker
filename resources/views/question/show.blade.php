@extends('app')

@section('title') {{$question->title}} @endsection

@section('meta_description') {{$question->title}} @endsection
@section('meta_keywords') {{$question->title}} @endsection

@section('content')


    <ol class="breadcrumb">
        <li><a href="{{url('/')}}" title="HHiker.ru">Главная</a></li>
        <li>{!! link_to_action('QuestionController@index', 'Вопросы') !!}</li>
        <li>{!! link_to_action('QuestionController@tags', 'Тэги') !!}</li>
        <li class="active">{{$question->title}}</li>
    </ol>

    @if (Auth::check() && Auth::user()->id == $question->user_id)
        {!! link_to_action('QuestionController@edit', 'Редактировать', [$question->id], ['class' => 'btn btn-default pull-right']) !!}
    @endif


    <div class="row">
        <div class="col-md-12">
            <div class="well">
                <div class="row">
                    <div class="col-md-12">
                        <div class="pull-left">
                            @include('user.avatar_sm', ['user' => $question->user])
                        </div>
                        <div class="pull-left">
                            {!! link_to_action('UserController@show', $question->user->name, [$question->user_id]) !!}
                            <p class="question-time">
                                {{$question->created_at->diffForHumans()}}
                            </p>
                        </div>
                    </div>
                </div>
                <hr/>
                <p>
                    @include('question.tags', ['tags' => $question->tags])
                </p>
                <p class="question-title">
                    {{$question->title}}
                </p>
                <p class="question-body">
                    {{$question->body}}
                </p>
                <hr/>
                @if ($question->isMine()) <p class="text-muted">Расскажите о вашем вопросе друзьям</p> @endif
                @include('partials.share')
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            @include('question.comment.list', ['comments' => $question->comments])
        </div>
    </div>


    <div class="row">
        <div class="col-md-12">
            <div class="well">
                @if (Auth::check())
                    {!! Form::model($questionComment = new \App\Models\QuestionComment(), ['action' => ['QuestionController@postCreateComment', $question->id]]) !!}
                    @include('question.comment_form', ['submitButtonText' => 'Ответить'])
                    {!! Form::close() !!}
                @else
                    <p>
                        Чтобы ответить на вопрос, {!! link_to_action('Auth\AuthController@getLogin', 'войдите') !!} в свой аккаунт или {!! link_to_action('Auth\AuthController@getRegister', 'зарегистрируйтесь') !!}
                        <br/><br/>
                        <button class="btn btn-warning" onclick="register('this');return false;">
                            <span class="glyphicon glyphicon-flash"></span>
                            Войти через vk и ответить на вопрос
                        </button>
                    </p>
                @endif
            </div>
        </div>
    </div>

@stop