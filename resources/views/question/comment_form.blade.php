{!! Form::hidden('question_comment_id', null) !!}

<div class="form-group @if ($errors->getBag('default')->has('body')) has-error @endif">
    {!! Form::label('body', (isset($labelText) ? $labelText : 'Ваш ответ') . ':') !!}
    {!! Form::textarea('body', null, ['class' => 'form-control', 'placeholder' => 'Ваш ответ', 'required' => 'required']) !!}
    @if ($errors->getBag('default')->has('body'))
        <small class="text-danger">Напишите ответ</small>
    @endif
</div>

<div class="form-group">
    {!! Form::submit($submitButtonText, ['class' => 'btn btn-success']) !!}
</div>