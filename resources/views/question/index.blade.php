@extends('app')

@section('title') Вопросы @endsection

@section('meta_description') Вопросы о самостоятельных путешествиях @endsection
@section('meta_keywords') Путешественники,блоги,travel-блоги @endsection


@section('content')

    <ol class="breadcrumb">
        <li><a href="{{url('/')}}" title="HHiker.ru">Главная</a></li>
        <li class="active">Вопросы</li>
    </ol>

    <div class="row">
        <div class="col-md-12">
            <input type="text" class="form-control form-horizontal" placeholder="Найти вопрос, ответ или пользователя"/>
        </div>
    </div>


    <div class="row" style="margin-top:12px;">
        <div class="col-md-12">
            @if (Auth::check())
                {!! link_to_action('QuestionController@create', '+ Задать вопрос', [], ['class' => 'btn btn-success']) !!}
            @else
                <div class="well">
                    <p>
                        Чтобы задать вопрос, {!! link_to_action('Auth\AuthController@getLogin', 'войдите') !!} в свой аккаунт или {!! link_to_action('Auth\AuthController@getRegister', 'зарегистрируйтесь') !!}
                        <br/><br/>
                        Быстрый способ:
                        <button class="btn btn-warning" onclick="register('q/create');return false;">
                            <span class="glyphicon glyphicon-flash"></span>
                            Войти через vk и сразу задать вопрос
                        </button>
                    </p>
                </div>
            @endif
        </div>
    </div>


    <div class="row" style="margin-top:24px;">
        <div class="col-sm-12">
            @include('question.list_sm')
        </div>
    </div>
@stop