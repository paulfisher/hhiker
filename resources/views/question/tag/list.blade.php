

<div class="row">
    <div class="col-sm-12">
        @foreach ($tags as $tag)
            <p>
                {!! link_to_action('QuestionController@showTag', $tag->name, $tag->id) !!}
            </p>
        @endforeach
    </div>
</div>
