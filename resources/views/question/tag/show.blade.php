@extends('app')

@section('title') - попутчики в путешествие @endsection

@section('meta_description')Вопросы о самостоятельных путешествиях @endsection
@section('meta_keywords')Самостоятельные путешествия @endsection

@section('content')


    <ol class="breadcrumb">
        <li><a href="{{url('/')}}" title="HHiker.ru">Главная</a></li>
        <li>{!! link_to_action('QuestionController@index', 'Вопросы') !!}</li>
        <li>{!! link_to_action('QuestionController@tags', 'Тэги') !!}</li>
        <li class="active">{{$tag->name}}</li>
    </ol>

    <div class="row">
        <div class="col-md-12">
            <p>
                @include('question.list_sm', ['questions' => $tag->questions])
            </p>
        </div>
    </div>


@stop