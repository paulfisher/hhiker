<style>
    .form-group label {
        color: #3a3f58;
        font-size: 14px;
    }
</style>



<div class="form-group @if ($errors->getBag('default')->has('title')) has-error @endif">
    {!! Form::label('title', 'Суть вопроса:') !!}
    <p class="text-muted">Сформулируйте вопрос так, чтобы сразу было понятно, о чём речь.</p>
    {!! Form::text('title', null, ['class' => 'form-control', 'placeholder' => 'Заголовок вопроса']) !!}
    @if ($errors->getBag('default')->has('title'))
        <small class="text-danger">Укажите заголовок вопроса</small>
    @endif
</div>

<p>Теги вопроса</p>
<p class="text-muted">Укажите от 1 до 5 тегов — предметных областей, к которым вопрос относится.</p>
<div class="form-group">
    {!! Form::label('tag_list', 'Тэги:') !!}
    {!! Form::select('tag_list[]', App\Models\Tag::lists('name','id'), null, ['id' => 'tag_list', 'class' => 'form-control', 'multiple' => true]) !!}
</div>


<div class="form-group @if ($errors->getBag('default')->has('body')) has-error @endif">
    {!! Form::label('body', 'Детали вопроса:') !!}
    <p class="text-muted">Опишите в подробностях свой вопрос, чтобы получить более точный ответ.</p>
    {!! Form::textarea('body', null, ['class' => 'form-control', 'placeholder' => 'Текст вопроса']) !!}
    @if ($errors->getBag('default')->has('body'))
        <small class="text-danger">Ваш вопрос</small>
    @endif
</div>

<div class="form-group" style="margin-top: 50px;">
    {!! Form::submit($submitButtonText, ['class' => 'btn btn-success']) !!}
</div>



@section('footer')
    <script>
        $(document).ready(function() {
            $('#tag_list').select2({
                placeholder: 'Выберите тэг'
            });
        });
    </script>
@endsection