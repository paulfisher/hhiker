@extends('app')

@section('content')


    <ol class="breadcrumb">
        <li><a href="{{url('/')}}" title="HHiker.ru">Главная</a></li>
        <li>{!! link_to_action('QuestionController@index', 'Вопросы') !!}</li>
        <li class="active">Задать вопрос</li>
    </ol>

    <div class="page-header">
        <h1>Новый вопрос</h1>
    </div>

    {!! Form::model($question = new \App\Models\Question(), ['action' => 'QuestionController@postCreate']) !!}
    @include('question.form', ['submitButtonText' => 'Опубликовать'])
    {!! Form::close() !!}

@stop