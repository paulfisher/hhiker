@foreach($tags as $tag)
    <a class="label label-default" href="{{action('QuestionController@showTag', $tag->id)}}" title="{{$tag->name}}">{{$tag->name}}</a>
    &nbsp;
@endforeach