Привет, {{$order->buyer->name}}<br/><br/>

Я рад сообщить, что твоя открытка от {{$order->seller->name}} ({{$order->blog->name}}) уже в пути.<br/>

@if ($order->seller_message)
    Также {{$order->seller->name}} прикрепил(а) сообщение:<br/>
    <p style="font-size: 14px;">
        {{$order->seller_message}}
    </p><br/>
@endif

Пожалуйста, не забудь пометить открытку как <strong>пришедшую</strong> в разделе "{!! link_to_action('OrderController@index', 'заказы') !!}" твоего профиля на hhiker.ru, когда открытка придет тебе в руки<br/>

@include('emails.footer')