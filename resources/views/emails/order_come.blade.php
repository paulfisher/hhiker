Привет, {{$order->seller->name}}<br/><br/>

Я рад сообщить, что твоя открытка для {{$order->buyer->name}} успешно дошла до адресата.<br/>

@include('emails.footer')