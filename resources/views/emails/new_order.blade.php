Привет, {{$order->seller->name}}<br/><br/>

Я рад сообщить, что твой проект "{{$order->blog->name}}" поддержал(а) <strong>{{$order->buyer->name}}</strong>.<br/>
Он(а) заказал(а) доставку открытки на свой адрес:<br/>
{{$order->address()}}<br/>

@if ($order->buyer_message)
    Также {{$order->buyer->name}} прикрепил(а) сообщение:<br/>
    <p style="font-size: 14px;">
        {{$order->buyer_message}}
    </p><br/>
@endif

@if ($order->status == \App\Models\OrderStatus::CONFIRMED)
Посылка уже оплачена, можно приступать к отправке.<br/>
В скором времени я переведу деньги на твою карту.<br/>
@endif

{{$order->seller->name}}, ты можешь увидеть все детали в разделе "{!! link_to_action('OrderController@index', 'заказы') !!}" твоего профиля на hhiker.ru

@include('emails.footer')