@extends('app')

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="well">
                @if (Auth::check())
                    Привет, {{$user->name}}. Ты великолепен! Твой аккаунт активен, можешь начинать пользоваться сайтом...
                    <script>
                        setTimeout(function() {
                            document.location.href = '/';
                        }, 3000);
                    </script>
                @else
                    Аккаунт активирован. Теперь ты можешь {!! link_to_action('Auth\AuthController@getLogin', 'войти') !!} в свой аккаунт...
                    <script>
                        setTimeout(function() {
                            document.location.href = 'auth/login';
                        }, 3000);
                    </script>
                @endif
            </div>
        </div>
    </div>


@endsection