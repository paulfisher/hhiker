@extends('app')

@section('content')

    <div class="row">
        <div class="col-md-12">
            <p>
                Письмо со ссылкой на активацию отправлено. Проверь почту {{$user->email}}, пожалуйста.<br/><br/>
                {!! link_to_action('PageController@index', 'Домой') !!}
            </p>
        </div>
    </div>


@endsection