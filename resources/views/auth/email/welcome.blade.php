Привет, {{$user->name}}. Рад нашему знакомству!
<br/><br/>
Меня зовут Павел. Я - разработчик портала для путешественников HHiker.ru. Основная идея сайта - вдохновлять людей на путешествия и приключения.<br/>
Я заметил, что ты зарегистрировался на HHiker.ru - это великолепно! Мне хотелось бы узнать, как у тебя продвигаются дела и узнать, чем я могу помочь?
<br/><br/>
Вот несколько разделов, которые могут быть полезны для тебя:<br/>
- Если ты еще готовишься к поездке, будет полезным почитать тех людей, кто уже находится в путешествии. Для этого есть раздел "{!! link_to_action('BlogController@index', 'Путешественники') !!}".<br/>
- Если ты хочешь отправиться в путешествие с новыми друзьями, раздел "{!! link_to_action('HuntCompanionController@index', 'Поиск попутчиков') !!}" - для тебя.<br/>
- Также добро пожаловать в раздел "{!! link_to_action('QuestionController@index', 'Вопросы') !!}" - здесь ты можешь задать любой вопрос. Наши эксперты в путешествиях с радостью помогут тебе.
<br/><br/>
Разработка сайта не прекращается, скоро будет много нового. Также я всегда рад услышать ваше мнение о сайте в личке ВК или FB :)

@include('emails.footer')