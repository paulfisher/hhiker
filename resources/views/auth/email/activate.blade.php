Привет, {{$user->name}}
<br/><br/>
Пожалуйста, активируй свой аккаунт с помощью ссылки ниже
<br/>
<b style="font-size: 20px;">
    {!! link_to_action('Auth\AuthController@activateUser', 'Активировать аккаунт', $user->activation_hash) !!}
</b>
@include('emails.footer')