@extends('app')

@section('content')

<div class="col-md-6 col-md-offset-3">
    <div class="well">
        <h1>Вход</h1>

        @if (session('needAuthMessage'))
            <div class="alert alert-success" style="font-size: 18px;">
                <span class="glyphicon glyphicon-hand-right" style="font-size: 26px; margin-right: 10px;"></span>
                {{ session('needAuthMessage') }}
            </div>
        @endif

        <div class="form">
            @if (count($errors) > 0)
                <div class="alert alert-warning">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <form method="POST" action="{!! url('auth/login') !!}">
                {!! csrf_field() !!}

                <div class="form-group">
                    {!! Form::text('email', old('email'), ['class' => 'form-control', 'placeholder' => 'Email']) !!}
                </div>

                <div class="form-group">
                    {!! Form::password('password', ['class' => 'form-control', 'placeholder' => 'Пароль']) !!}
                </div>

                <div class="form-group">
                    <label><input type="checkbox" name="remember">&nbsp;Запомнить меня</label>
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-default" style="margin-right: 10px;">Войти</button>
                    {!! link_to('auth/register', 'Создать аккаунт') !!}
                    <button class="btn btn-success pull-right" onclick="register();return false;" title="Войти, используя свой аккаунт ВКонтакте">
                        <span class="glyphicon glyphicon-flash"></span>
                        Войти через VK
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>


@endsection