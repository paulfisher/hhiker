@extends('app')

@section('content')

<div class="col-md-6 col-md-offset-3">

    <div class="well">
        <h1>Я искатель приключений</h1>

        @if (session('needAuthMessage'))
            <div class="alert alert-success" style="font-size: 18px;">
                {{ session('needAuthMessage') }}
            </div>
        @endif

        <div class="form">
            @if (count($errors) > 0)
                <div class="alert alert-warning">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <form method="POST" action="{!! url('/auth/register') !!}">
                {!! csrf_field() !!}

                {!! Form::hidden('vkId', '') !!}
                <div class="form-group">
                    {!! Form::text('name', old('name'), ['class' => 'form-control', 'placeholder' => 'Имя']) !!}
                </div>

                <div class="form-group">
                    {!! Form::text('email', old('email'), ['class' => 'form-control', 'placeholder' => 'Email']) !!}
                </div>

                <div class="form-group">
                    {!! Form::password('password', ['class' => 'form-control', 'placeholder' => 'Пароль']) !!}
                </div>

                <div class="form-group">
                    {!! Form::password('password_confirmation', ['class' => 'form-control', 'placeholder' => 'Повторите пароль']) !!}
                </div>

                <a class="form-group">
                    <button type="submit" class="btn btn-default" style="margin-right: 10px;">Готово</button>
                    {!! link_to('auth/login', 'Уже есть аккаунт. Войти') !!}
                    <button class="btn btn-danger pull-right" onclick="register();return false;" title="Войти, используя свой аккаунт ВКонтакте">
                        <span class="glyphicon glyphicon-flash"></span>
                        Войти через VK
                    </button>
        </div>
        </form>
    </div>
    </div>

</div>
@stop
