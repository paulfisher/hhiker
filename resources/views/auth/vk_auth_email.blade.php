<p>Чтобы войти, нам нужно знать ваш Email адрес</p>

{!! Form::open(['method' => 'POST', 'action' => 'Auth\AuthController@vkAuth']) !!}

    {!! csrf_field() !!}

    {!! Form::email('email', '', ['class' => 'form-control', 'placeholder' => 'ваш Email']) !!}

    <button type="submit" class="btn btn-default">Готово</button>

{!! Form::close() !!}