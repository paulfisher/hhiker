<div class="list-group">
    @foreach ($companions as $companion)
        <div class="list-group-item">
            <div class="row">
                <div class="col-md-8">
                    <div class="pull-left">
                        <a href="{{action('UserController@show', $companion->user_id)}}">
                            @include('user.avatar_sm', ['user' => $companion->user])
                        </a>
                    </div>
                    <span class="companion-list-title">
                        {!! link_to_action('HuntCompanionController@show', str_limit($companion->about_trip, 32), [$companion->id]) !!}
                    </span>
                    @include('companion.tags', ['tags' => $companion->tags])
                    <p>
                        {!! link_to_action('UserController@show', $companion->user->name, $companion->user->id) !!}
                        <span class="companion-list-time">
                            {{$companion->created_at->diffForHumans()}}
                        </span>
                    </p>
                </div>
                <div class="col-md-4">
                    <div class="pull-left">
                        <p>
                            комментариев: {{$companion->comments()->count()}}
                        </p>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
</div>
