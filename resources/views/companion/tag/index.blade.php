@extends('app')

@section('title') - попутчики в путешествие @endsection

@section('meta_description')Вопросы о самостоятельных путешествиях @endsection
@section('meta_keywords')Самостоятельные путешествия @endsection

@section('content')


    <ol class="breadcrumb">
        <li><a href="{{url('/')}}" title="HHiker.ru">Главная</a></li>
        <li>{!! link_to_action('HuntCompanionController@index', 'Поиск попутчиков') !!}</li>
        <li class="active">Тэги</li>
    </ol>

    <div class="row">
        <div class="col-md-12">
            <p>
                @include('companion.tag.list')
            </p>
        </div>
    </div>


@stop