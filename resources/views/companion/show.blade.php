@extends('app')

@section('title') {{str_limit($huntCompanion->about_trip, 25)}} @endsection

@section('meta_description') {{$huntCompanion->about_trip}} @endsection
@section('meta_keywords') {{$huntCompanion->about_trip}} @endsection

@section('content')

    <style>
        .companion-text {
            margin-top: 42px;
        }
    </style>


    <ol class="breadcrumb">
        <li><a href="{{url('/')}}" title="HHiker.ru">Главная</a></li>
        <li>{!! link_to_action('HuntCompanionController@index', 'Поиск попутчиков') !!}</li>
        <li class="active">{{str_limit($huntCompanion->about_trip, 25)}}</li>
    </ol>

    <div class="page-header">
        <h3>
            {!! link_to_action('UserController@show', $huntCompanion->user->name, [$huntCompanion->user->id]) !!} ищет попутчика в путешествие
            @if (Auth::check() && Auth::user()->id == $huntCompanion->user->id)
                {!! link_to_action('HuntCompanionController@edit', 'Редактировать', [$huntCompanion->id], ['class' => 'btn btn-default pull-right']) !!}
            @endif
        </h3>
    </div>


    <div class="row">
        <div class="col-md-12">
            <div class="well">
                <div class="row">
                    <div class="col-md-12">
                        <div class="pull-left">
                            @include('user.avatar_sm', ['user' => $huntCompanion->user])
                        </div>
                        <div class="pull-left">
                            {!! link_to_action('UserController@show', $huntCompanion->user->name, $huntCompanion->user_id) !!}
                            <p class="question-time">
                                {{$huntCompanion->created_at->diffForHumans()}}
                            </p>
                        </div>
                    </div>
                </div>
                <hr/>

                <div class="row">
                    <div class="col-md-12">
                        @include('companion.tags', ['tags' => $huntCompanion->tags])
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <div class="page-header">
                            <h4>Обо мне</h4>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <p class="companion-text">
                            {!! $huntCompanion->about_me !!}
                        </p>
                    </div>
                </div>
                <hr/>

                <div class="row">
                    <div class="col-md-3">
                        <div class="page-header">
                            <h4>О путешествии</h4>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <p class="companion-text">
                            {!! $huntCompanion->about_trip !!}
                        </p>
                    </div>
                </div>
                <hr/>

                <div class="row">
                    <div class="col-md-3">
                        <div class="page-header">
                            <h4>Кого ищу в попутчики</h4>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <p class="companion-text">
                            {!! $huntCompanion->about_companion !!}
                        </p>
                    </div>
                </div>
                <hr/>

                <div class="row">
                    <div class="col-md-3">
                        <div class="page-header">
                            <h4>Когда хочу отправиться в путь</h4>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <p class="companion-text">
                            {!! $huntCompanion->when !!}
                        </p>
                    </div>
                </div>
                <hr/>

                <div class="row">
                    <div class="col-md-3">
                        <div class="page-header">
                            <h4>Мои контакты</h4>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <p class="companion-text">
                            @if (Auth::check())
                            <noindex>
                                <span onclick="this.innerText = $(this).data('contacts');" data-contacts="{!! $huntCompanion->contact !!}">показать</span>
                            </noindex>
                            @else
                                Чтобы увидеть контакты путешественника, {!! link_to_action('Auth\AuthController@getLogin', 'войдите') !!} в свой аккаунт или {!! link_to_action('Auth\AuthController@getRegister', 'зарегистрируйтесь') !!}
                                <br/><br/>
                                Быстрый способ:
                                <button class="btn btn-warning" onclick="register('this');return false;">
                                    <span class="glyphicon glyphicon-flash"></span>
                                    Войти через vk и посмотреть контакты
                                </button>
                            @endif
                        </p>
                    </div>
                </div>
                <hr/>
                <div class="row">
                    <div class="col-md-12">
                        @if ($huntCompanion->isMine()) <p class="text-muted">Расскажите о вашем путешествии друзьям</p> @endif
                        @include('partials.share')
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    @include('companion.comment.list', ['comments' => $huntCompanion->comments])
                </div>
            </div>


            <div class="row">
                <div class="col-md-12">
                    <div class="well">
                        @if (Auth::check())
                            {!! Form::model($huntCompanionComment = new \App\Models\HuntCompanionComment(), ['action' => ['HuntCompanionController@postCreateComment', $huntCompanion->id]]) !!}
                            @include('companion.comment_form', ['submitButtonText' => 'Ответить'])
                            {!! Form::close() !!}
                        @else
                            <p>
                                Чтобы написать комментарий, {!! link_to_action('Auth\AuthController@getLogin', 'войдите') !!} в свой аккаунт или {!! link_to_action('Auth\AuthController@getRegister', 'зарегистрируйтесь') !!}
                                <br/><br/>
                                <button class="btn btn-warning" onclick="register('this');return false;">
                                    <span class="glyphicon glyphicon-flash"></span>
                                    Войти через vk и написать комментарий
                                </button>
                            </p>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop