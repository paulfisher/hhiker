@extends('app')

@section('content')


    <ol class="breadcrumb">
        <li><a href="{{url('/')}}" title="HHiker.ru">Главная</a></li>
        <li>{!! link_to_action('HuntCompanionController@index', 'Поиск попутчиков') !!}</li>
        <li class="active">Ищу попутчика</li>
    </ol>

    <div class="page-header">
        <h1>Ищу попутчика</h1>
    </div>

    <div class="well">
        {!! Form::model($huntCompanion = new \App\Models\HuntCompanion(), ['url' => 'companion']) !!}
        @include('companion.form', ['submitButtonText' => 'Сохранить'])
        {!! Form::close() !!}
    </div>

@stop