<style>
    .form-group label {
        color: #3a3f58;
        font-size: 14px;
    }
</style>


<div class="form-group @if ($errors->getBag('default')->has('about_me')) has-error @endif">
    {!! Form::label('about_me', 'Обо мне:') !!}
    <p class="text-muted">Расскажите какой вы хороший, ваши привычки, что цените в людях ит.д.</p>
    {!! Form::textarea('about_me', null, ['class' => 'form-control', 'placeholder' => 'обо мне']) !!}
    @if ($errors->getBag('default')->has('about_me'))
        <small class="text-danger">Расскажите о себе</small>
    @endif
</div>
<hr/>

<div class="form-group @if ($errors->getBag('default')->has('about_trip')) has-error @endif">
    {!! Form::label('about_trip', 'О поездке:') !!}
    <p class="text-muted">Куда собираетесь, на каком транспорте, в чем цель поездки, где будете ночевать ит.д.</p>
    {!! Form::textarea('about_trip', null, ['class' => 'form-control', 'placeholder' => 'о поездка']) !!}
    @if ($errors->getBag('default')->has('about_trip'))
        <small class="text-danger">Расскажите о поездке</small>
    @endif
</div>
<hr/>

<div class="form-group">
    {!! Form::label('about_companion', 'Ищу такого попутчика:') !!}
    <p class="text-muted">Кого бы вы хотели видеть спутником в своем путешествии</p>
    {!! Form::textarea('about_companion', null, ['class' => 'form-control', 'placeholder' => 'описание попутчика']) !!}
</div>
<hr/>

<div class="form-group">
    {!! Form::label('when', 'Когда:') !!}
    <p class="text-muted">Укажите примерные временные рамки</p>
    {!! Form::text('when', null, ['class' => 'form-control', 'placeholder' => 'когда']) !!}
</div>
<hr/>

<p>Теги путешествия</p>
<p class="text-muted">Укажите от 1 до 5 тегов характерных для вашего путешествия.</p>
<div class="form-group">
    {!! Form::label('tag_list', 'Тэги:') !!}
    {!! Form::select('tag_list[]', App\Models\Tag::lists('name','id'), null, ['id' => 'tag_list', 'class' => 'form-control', 'multiple' => true]) !!}
</div>
<hr/>

<div class="form-group @if ($errors->getBag('default')->has('contact')) has-error @endif">
    {!! Form::label('contact', 'Связаться со мной:') !!}
    <p class="text-muted">Подойдет любой способ связи. Skype, телефон, страничка vk.com, ваш фейсбук ит.д.<br/>Эти данные увидит только потенциальный попутчик. Поисковыми системами не будет индексироваться.</p>
    {!! Form::text('contact', null, ['class' => 'form-control', 'placeholder' => 'мои контакты']) !!}
    @if ($errors->getBag('default')->has('contact'))
        <small class="text-danger">Пожалуйста, укажите свои контактные данные</small>
    @endif
</div>
<hr/>

<div class="form-group" style="margin-top: 50px;">
    {!! Form::submit($submitButtonText, ['class' => 'btn btn-primary']) !!}
</div>



@section('footer')
    <script>
        $(document).ready(function() {
            $('#tag_list').select2({
                placeholder: 'Выберите тэг'
            });
        });
    </script>
@endsection