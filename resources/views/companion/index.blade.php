@extends('app')

@section('title') Поиск попутчиков @endsection

@section('meta_description') Поиск попутчиков @endsection
@section('meta_keywords') Попутчики,путешествия,автостоп @endsection


@section('content')

    <ol class="breadcrumb">
        <li><a href="{{url('/')}}" title="HHiker.ru">Главная</a></li>
        <li class="active">Поиск попутчиков</li>
    </ol>

    <div class="row">
        <div class="col-md-12">
            <div class="page-header">
                <h3>Поиск попутчиков</h3>
            </div>
        </div>
    </div>

    <div class="row" style="margin-bottom: 16px;">
        <div class="col-md-12">
            @if (Auth::check())
                {!! link_to_action('HuntCompanionController@create', '+ Ищу попутчика', [], ['class' => 'btn btn-success']) !!}
            @endif
        </div>
    </div>

    @if (!Auth::check())
        <div class="row">
            <div class="col-md-12">
                <div class="well">
                    Чтобы найти попутчика, {!! link_to_action('Auth\AuthController@getLogin', 'войдите') !!} в свой аккаунт
                    или {!! link_to_action('Auth\AuthController@getRegister', 'зарегистрируйтесь') !!}
                    <br/><br/>
                    Быстрый способ:
                    <button class="btn btn-warning" onclick="register('companion/create');return false;">
                        <span class="glyphicon glyphicon-flash"></span>
                        Войти через vk и перейти к поиску попутчика
                    </button>
                </div>
            </div>
        </div>
    @endif

    <div class="row">
        <div class="col-md-12">
            @include('companion.list_sm', ['companions' => $companions])
        </div>
    </div>

@stop
