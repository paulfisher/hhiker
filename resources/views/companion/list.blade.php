
@foreach ($companions as $companion)
    <div class="row" style="margin-top: 20px;">
        <div class="col-sm-12">
            <div class="row">
                @if ($companion->user->img)
                    <div class="col-sm-2">
                        <img src="{!! $companion->user->img !!}" style="max-width:150px; margin-right:10px;" class="pull-left"/>
                    </div>
                @endif

                <div class="col-sm-10">
                    <b>{!! link_to_action('UserController@show', $companion->user->name, [$companion->user->id]) !!}</b>
                    <small class="text-muted">{!! $companion->when !!}</small>
                    <p>{!! str_limit($companion->about_trip, 60) !!}</p>

                    {!! link_to_action('HuntCompanionController@show', 'Смотреть', [$companion->id]) !!}

                    @if (Auth::check() && Auth::user()->id == $companion->user->id)
                        {!! link_to_action('HuntCompanionController@edit', 'Редактировать', [$companion->id], ['class' => 'btn btn-default btn-xs pull-right']) !!}
                    @endif
                </div>
            </div>
        </div>
    </div>
@endforeach
