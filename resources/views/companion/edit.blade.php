@extends('app')

@section('content')

    <ol class="breadcrumb">
        <li><a href="{{url('/')}}" title="HHiker.ru">Главная</a></li>
        <li>{!! link_to_action('HuntCompanionController@index', 'Поиск попутчиков') !!}</li>
        <li>{!! link_to_action('HuntCompanionController@show', str_limit($huntCompanion->about_trip, 25), [$huntCompanion->id]) !!}</li>
        <li class="active">Редактирование</li>
    </ol>

    <div class="well">
        {!! Form::model($huntCompanion, ['method' => 'PATCH', 'action' => ['HuntCompanionController@update', $huntCompanion->id]]) !!}
        @include('companion.form', ['submitButtonText' => 'Обновить'])
        {!! Form::close() !!}
    </div>

@stop