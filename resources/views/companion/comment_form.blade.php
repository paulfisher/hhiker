{!! Form::hidden('hunt_companion_comment_id', null) !!}

<div class="form-group @if ($errors->getBag('default')->has('body')) has-error @endif">
    {!! Form::label('body', (isset($labelText) ? $labelText : 'Ваш комментарий') . ':') !!}
    {!! Form::textarea('body', null, ['class' => 'form-control', 'placeholder' => 'комментарий', 'required' => 'required']) !!}
    @if ($errors->getBag('default')->has('body'))
        <small class="text-danger">Напишите комментарий</small>
    @endif
</div>

<div class="form-group">
    {!! Form::submit($submitButtonText, ['class' => 'btn btn-success']) !!}
</div>