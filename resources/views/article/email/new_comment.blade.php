Привет, {{$article->user->name}}
<br/>
<br/>
Хорошие новости!
<br/>
{{$comment->user->name}} дал комментарий на твою запись <b>{!! link_to_action('ArticleController@show', $article->title, $article->id) !!}</b>:
<br/>
<br/>
<p>{{$comment->body}}</p>
<br/>
<b>
    {!! link_to_action('ArticleController@show', 'Посмотреть комментарий', $article->id) !!}
</b>
@include('emails.footer')
