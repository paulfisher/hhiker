@extends('app')

@section('meta_description')Путешествия и приключения @endsection
@section('meta_keywords')Путешествия,автостоп,попутчики,travel-блог @endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="page-header">
                <h1>Свежие приключения</h1>
            </div>
            @include('article.table', ['articles' => $articles])
        </div>
    </div>
@stop