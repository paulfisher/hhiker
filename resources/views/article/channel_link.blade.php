<noindex>
    <nofollow>
@if ($article->blogChannel->channel->type == \App\Models\ChannelType::VK_GROUP)
    <a href="{!! $article->blogChannel->url !!}?w=wall{!! $article->blogChannel->channel_blog_key  !!}_{!! $article->channel_post_key !!}" target="_blank" rel="nofollow">
        <small class="text-muted">посмотреть оригинал</small>
    </a>
@elseif($article->blogChannel->channel->type == \App\Models\ChannelType::YOUTUBE)
    <a href="https://www.youtube.com/watch?v={!! $article->channel_post_key !!}" target="_blank" rel="nofollow">
        <small class="text-muted">смотреть на youtube</small>
    </a>
@endif
    </nofollow>
</noindex>