@extends('app')

@section('meta_description') Путешествия и приключения @endsection
@section('meta_keywords') {{$tag->name}} @endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="page-header">
                <h1>{{$tag->name}}</h1>
            </div>
            @include('article.table', ['articles' => $tag->articles])
        </div>
    </div>
@stop