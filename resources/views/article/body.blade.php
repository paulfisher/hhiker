<div class="row">
    <div class="col-md-12">
        <article class="article-text">
            {!! $article->body !!}
        </article>
        @include('partials.share')
    </div>
</div>