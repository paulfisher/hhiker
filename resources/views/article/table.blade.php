
<div class="row articles-table">
    <div class="col-sm-12">
@foreach ($articles as $article)
    @include('article.list_item', ['article' => $article])
@endforeach
    </div>
</div>
