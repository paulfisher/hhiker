<article class="article-item">
    <div class="article-item-main_img">
        @if ($article->main_img)
            <a href="{{url('/article', $article->slugOrId()) }}" title="{{$article->title}}">
                <img src="{!! $article->main_img !!}" alt="{{$article->title}}"/>
            </a>
        @endif
    </div>

    <div class="article-item-link">
        @include('article.tags', ['tags' => $article->tags])
        <a href="{{url('/article', $article->slugOrId()) }}" title="{{$article->title}}">
            {{$article->title}}
        </a>
    </div>

    <div class="article-item-statistics">
        <span class="time">
            {{$article->created_at->diffForHumans()}}
        </span>
    </div>
</article>