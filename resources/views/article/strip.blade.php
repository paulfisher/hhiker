
<div class="row articles-strip">
    <div class="col-lg-12">

        <div class="well well-lg">
        @foreach ($articles as $article)
            <div class="row article-item">
                <div class="col-lg-4">
                    @foreach ($article->images as $image)
                        <div class="article-image-item">
                            @if ($image->photo_lg)
                            <a href="{!! $image->photo_lg !!}" target="_blank">
                            @endif
                                <img src="{!! $image->photo_md !!}" class="img-responsive"/>
                            @if ($image->photo_lg)
                            </a>
                            @endif
                        </div>
                    @endforeach
                    @foreach ($article->videos as $video)
                        <a href="{{url('/article', $article->slugOrId()) }}">показать видео</a>
                    @endforeach
                </div>
                <div class="col-lg-8">
                        <div class="article-item-link">
                            <a href="{{url('/article', $article->slugOrId()) }}" title="{{$article->title}}">
                                {{$article->title}}
                            </a>
                            <br/>
                            <span class="time">
                                {{$article->published_at}}
                            </span>
                        </div>
                        <div class="article-item_text">
                            {!! nl2br($article->body) !!}
                        </div>
                    @if ($article->blogChannel)
                        <small>
                            @include('article.channel_link', ['article' => $article])
                        </small>
                    @endif
                    <span class="pull-left">
                        @include('article.article_stat', ['article' => $article])
                    </span>
                    <span class="pull-right">
                        @include('partials.back_btn', ['blog' => $article->blog])
                    </span>
                </div>
            </div>
            <hr/>
        @endforeach
        </div>

    </div>
</div>