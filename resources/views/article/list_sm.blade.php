<div class="list-group">
    @foreach ($articles as $article)
        <div class="list-group-item">
            <div class="row">
                <div class="col-md-8">
                    <div class="pull-left">
                        <a href="{{action('UserController@show', $article->user_id)}}">
                            @include('user.avatar_sm', ['user' => $article->user])
                        </a>
                    </div>
                    <span class="article-list-title">
                        {!! link_to_action('ArticleController@show', str_limit($article->short, 32), $article->id) !!}
                    </span>
                    <p>
                        {!! link_to_action('UserController@show', $article->user->name, $article->user->id) !!}
                        <span class="companion-list-time">
                            {{$article->created_at->diffForHumans()}}
                        </span>
                    </p>
                </div>
                <div class="col-md-4">
                    <div class="pull-left">
                        <p>
                            комментариев: {{$article->comments()->count()}}
                        </p>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
</div>
