@extends('app')

@section('title') {{$article->title}} @endsection

@section('meta_description') {{$article->title}} @endsection
@section('meta_keywords') {{$article->title}} @endsection

@section('content')

    <div class="row">
        <div class="col-md-12">
            <ol class="breadcrumb">
                <li><a href="{{url('/')}}" title="HHiker.ru">Главная</a></li>
                <li>{!! link_to('article', 'Публикации') !!}</li>
                @if ($article->blog_id > 0)
                <li>{!! link_to_action('BlogController@show', $article->blog->name, [$article->blog_id]) !!}</li>
                @endif
                <li class="active">{{$article->title}}</li>
            </ol>

            @if (Auth::check() && $article->user->id == Auth::user()->id)
                <a href="{{route('article.edit', $article->id)}}" class="pull-right btn btn-xs btn-primary">редактировать</a>
            @endif
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="page-header">
                <h2>{{$article->title}}</h2>
            </div>
        </div>
    </div>
    @include('article.body')

    <hr/>

    @if ($article->blog()->exists())
        <div class="well well-lg">
            @include('blog.header_info', ['blog' => $article->blog])
        </div>
    @endif

    <hr/>


    <div id="disqus_thread"></div>
    <script type="text/javascript">
        /* * * CONFIGURATION VARIABLES * * */
        var disqus_shortname = 'hhiker';

        /* * * DON'T EDIT BELOW THIS LINE * * */
        (function() {
            var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
            dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
            (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
        })();
    </script>
    <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript" rel="nofollow">comments powered by Disqus.</a></noscript>
@stop