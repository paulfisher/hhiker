@foreach($tags as $tag)
    <a class="label label-default" href="{{action('ArticleController@readTag', $tag->id)}}" title="{{$tag->name}}">{{$tag->name}}</a>
    &nbsp;
@endforeach