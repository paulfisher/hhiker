<div class="list-group">
    @foreach ($comments as $comment)
        <div class="list-group-item article-comment-container">
            <div class="row">
                <div class="col-md-12">
                    <div class="pull-left">
                        @include('user.avatar_sm', ['user' => $comment->user])
                    </div>
                    <div class="pull-left">
                        <span class="question-comment-author">
                            {{$comment->user->name}}
                        </span>
                        <span class="question-comment-time">
                            {{$comment->created_at->diffForHumans()}}
                        </span>
                        @if ($comment->parentComment)
                            <span class="question-comment-answer-to">
                                ответ на <a data-trigger="hover" data-toggle="popover" title="{{$comment->parentComment->user->name}}" href="javascript: void 0;" data-content="{{$comment->parentComment->body}}">комментарий</a> от: {{$comment->parentComment->user->name}}
                            </span>
                        @endif
                        <p class="question-comment-body">
                            {{$comment->body}}
                        </p>
                        @if (Auth::check())
                        <a href="javascript: void 0;" onclick="$(this).closest('.article-comment-container').find('.answer-form').toggle().find('textarea').focus();">ответить</a>
                        @endif
                    </div>
                </div>
            </div>
            @if (Auth::check())
            <div class="row answer-form" style="display: none;">
                <div class="col-md-12">
                    <hr/>
                    {!! Form::model($articleComment = new \App\Models\ArticleComment(['article_comment_id' => $comment->id]), ['action' => ['ArticleController@postCreateComment', $comment->article_id]]) !!}
                    @include('article.comment.form', ['submitButtonText' => 'Ответить', 'labelText' => 'Ваш ответ для ' . $comment->user->name])
                    {!! Form::close() !!}
                </div>
            </div>
            @endif
        </div>
    @endforeach
</div>