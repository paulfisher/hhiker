{!! Form::open(['url' => 'image/upload', 'files' => true]) !!}
    {!! Form::file('image', null, ['class' => 'form-control']) !!}
    {!! Form::hidden('callbackId', $callbackId) !!}
    {!! Form::submit("upload", ['class' => 'btn btn-primary form-control']) !!}
{!! Form::close() !!}

@section('footer')
<script>
</script>
@endsection