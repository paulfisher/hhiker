@extends('app')

@section('meta_description') Путешествия и приключения @endsection
@section('meta_keywords')
    @foreach ($tags as $tag)
        {{$tag->name}},
    @endforeach
@endsection



@section('content')
    <style>
        .tag-btn {
            margin-bottom: 5px;
        }
    </style>
    <div class="row" style="margin-top: 32px;">
        <div class="col-lg-12">
            <div class="page-header">
                <h2>Выбери интересные тебе темы и приступай к путешествию</h2>
            </div>
            <a href="{{action('ArticleController@saveInterests')}}" class="btn btn-lg btn-success" style="display: none;margin-bottom: 12px;" id="go-travel">
                <span class="glyphicon glyphicon-ok-sign"></span> поехали
            </a>
            <br/>
            @foreach ($tags as $tag)
                {!! link_to_action('ArticleController@readTag', $tag->name, $tag->id, ['class' => 'btn btn-lg btn-primary tag-btn', 'tag-id' => $tag->id]) !!}
            @endforeach
        </div>
    </div>
@endsection

@section('footer')
    <script>
        $(function() {
            var goTravel = $('#go-travel');
            $('.tag-btn').click(function() {
                $(this).toggleClass('active');
                if ($('.tag-btn.active').length > 0) {
                    goTravel.show();
                } else {
                    goTravel.hide();
                }
                return false;
            });
            goTravel.click(function() {
                var tags = [];
                $('.tag-btn.active').each(function() {
                    tags.push($(this).attr('tag-id'));
                });
                document.location.href = '{{action('ArticleController@saveInterests')}}?tags=' + tags.join(',');
                return false;
            });
        });
    </script>
@endsection