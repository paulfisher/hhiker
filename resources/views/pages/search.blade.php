@extends('app')

@section('meta_description') Путешествия и приключения @endsection
@section('meta_keywords') Путешествия,автостоп,попутчики,travel-блог,{{$query}} @endsection

@section('head')
    <link rel="stylesheet" href="/css/index.css" />
@stop

@section('content')
    <div class="row" style="margin-top: 32px;">
        <div class="col-lg-6 col-lg-push-3 text-center">
            <form class="" role="search" action="{{action('PageController@search')}}">
                <div class="form-group">
                    <input type="text" autofocus class="form-control" placeholder="Поиск.." required="required" name="q" value="{{$query}}">
                </div>
            </form>
        </div>
    </div>
    @if (count($blogs) > 0)
    <div class="row">
        <div class="col-md-12">
            <div class="page-header">
                <h4>Блоги</h4>
            </div>
            @include('blog.list')
        </div>
    </div>
    @endif

    @if (count($articles) > 0)
    <div class="row">
        <div class="col-md-12">
            <div class="page-header">
                <h4>Записи</h4>
            </div>
            @include('article.table')
        </div>
    </div>
    @endif

    @if (count($articles) == 0 and count($blogs) == 0 and mb_strlen($query) > 3)
    <div class="row">
        <div class="col-lg-6 col-lg-push-3">
            К сожалению, по запросу "{{$query}}" не найдено записей
        </div>
    </div>
    @endif

@stop