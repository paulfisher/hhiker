@extends('app')

@section('content')


<div class="row">
    <div class="col-md-12">
        <div class="page-header">
            <h3>О сайте HHiker.ru</h3>
        </div>
        <div class="well">
            <p>Привет, друг. Ты попал на сайт для самостоятельных путешественников :)</p>
            <p>
                Здесь можно:
                <ul>
                    <li>{!! link_to_action('HuntCompanionController@index', 'Найти попутчика в путешествие') !!}</li>
                    <li>{!! link_to_action('QuestionController@index', 'Задать вопрос') !!}</li>
                    <li>{!! link_to_action('CouchController@index', 'Найти вписку') !!}</li>
                    <li>{!! link_to_action('BlogController@index', 'Читать и смотреть блоги и влоги путешественников') !!}</li>
                </ul>
            </p>
        </div>
    </div>
</div>

@stop