@extends('app')

@section('title') Главная @endsection

@section('meta_description') Путешествия и приключения @endsection
@section('meta_keywords') Путешествия,автостоп,попутчики,travel-блоги,кругосветка,кругосветное @endsection

@section('head')
    <link rel="stylesheet" href="/css/index.css" />
@stop

@section('content')
    <div class="row">
        <div class="col-md-6">
            <div class="page-header">
                <h3>{!! link_to_action('HuntCompanionController@index', 'Поиск попутчиков') !!}</h3>
            </div>
            @include('companion.list_sm')
            @if (Auth::check())
                {!! link_to_action('HuntCompanionController@create', '+ Найти попутчика', [], ['class' => 'btn btn-success']) !!}
            @else
                {!! link_to_action('HuntCompanionController@index', '+ Найти попутчика', [], ['class' => 'btn btn-success']) !!}
            @endif
        </div>
        <div class="col-md-6">
            <div class="page-header">
                <h3>{!! link_to_action('QuestionController@index', 'Вопросы') !!}</h3>
            </div>
            @include('question.list_sm')
            @if (Auth::check())
                {!! link_to_action('QuestionController@create', '+ Задать вопрос', [], ['class' => 'btn btn-success']) !!}
            @else
                {!! link_to_action('QuestionController@index', '+ Задать вопрос', [], ['class' => 'btn btn-success']) !!}
            @endif
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="page-header">
                <h3>
                    {!! link_to_action('BlogController@index', 'Путешественники') !!}
                </h3>
            </div>
            @include('blog.list')
            @if (Auth::check())
                {!! link_to_action('BlogController@create', '+ Добавить свой блог', [], ['class' => 'btn btn-success']) !!}
            @else
                {!! link_to_action('BlogController@index', '+ Добавить свой блог', [], ['class' => 'btn btn-success']) !!}
            @endif
        </div>
    </div>
@stop