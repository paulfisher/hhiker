@extends('app')

@section('content')


<div class="row">
    <div class="col-md-12">
        <div class="page-header">
            <h1>HHiker.ru</h1>
        </div>
        <div class="well">
            Теперь путешественники могут получать финансовую поддержку от своих читателей.
            Механизм банальный, отправка отрыток. Читатель заказывает открытку, оплачивает и путешественник получает уведомление.
            Деньги перечисляются на карту путешественнику.
            <br/>
            В планах развитие функционала. Например настройка цены открытки для каждого путешественника. Также есть идея разделить цену на два тарифа, для подписчиков/остальных(проверяться будет автоматически). Таким образом мотивировать людей подписываться на паблики/каналы.
            <br/>
            <strong>
                Функционал заказа открыток включается по запросу. Требуется <a href="https://vk.com/pavel.rybakov" target="_blank">написать мне</a>.
            </strong>
        </div>
    </div>
</div>

@stop